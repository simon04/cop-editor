# Copeditor instructions
Here you can find instructions to launch **Copeditor** application in development mode and instructions to generate .war file for deployment.

## Development

**Copeditor** requires that you have NodeJS and Yarn installed on your computer. After installing all dependencies with 
   
    yarn install
    
you can start the application with command:

    yarn start

To run the backend part of **Copeditor** simply run the following command:

    mvn spring-boot:run -Dspring-boot.run.profiles=dev,swagger,no-liquibase
    
You can easily define a launcher or shortcut in your preferred IDE.    
Specifying the correct profiles is very important, as it defines how the application is configured, i.e. database connection among other parameters. There are currently two profiles: **dev** and **prod**. You can see and edit configuration for each environment in the .yaml files located under

    src/main/resources/config/application-dev.yml
    src/main/resources/config/application-prod.yml

## Deployment

To generate a .war file ready for deployment simply run the following command:

     mvn -DskipTests=true package -P prod,no-liquibase
     
This will generate a war file in the target folder of the project.
    

