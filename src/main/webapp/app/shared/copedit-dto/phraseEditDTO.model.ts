import { Moment } from 'moment';
import { DomainDTO } from './domainDTO.model';
import { PhraseLanguageDTO } from 'app/shared/copedit-dto/phraseLanguageDTO.model';
import { VersionDTO } from 'app/shared/copedit-dto/versionDTO.model';
import { SentenceLanguageDTO } from 'app/shared/copedit-dto/SentenceLanguageDTO.model';

export interface IPhraseOptionEditDTO {
    domain?: DomainDTO;
    version?: VersionDTO;
    datenew?: Moment;
    datelast?: Moment;
    phrasesDE?: PhraseLanguageDTO;
    phrasesIT?: PhraseLanguageDTO;
    phrasesEN?: PhraseLanguageDTO;
    phrasesFR?: PhraseLanguageDTO;
    sentenceList?: SentenceLanguageDTO[];
}

export class PhraseOptionEditDTO implements IPhraseOptionEditDTO {
    constructor(
        public domain?: DomainDTO,
        public version?: VersionDTO,
        public datenew?: Moment,
        public datelast?: Moment,
        public phrasesDE?: PhraseLanguageDTO,
        public phrasesIT?: PhraseLanguageDTO,
        public phrasesEN?: PhraseLanguageDTO,
        public phrasesFR?: PhraseLanguageDTO,
        public sentenceList?: SentenceLanguageDTO[]
    ) {}
}
