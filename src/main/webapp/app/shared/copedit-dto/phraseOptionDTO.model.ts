import { Moment } from 'moment';

export interface IPhraseOptionDTO {
    id?: number;
    name?: string;
    language?: string;
    header?: string;
    version?: string;
    basisVersion?: string;
    remark?: string;
    deleted?: boolean;
    domainId?: number;
    datenew?: Moment;
    datelast?: Moment;
    moduleNo?: number;
    itemNo?: number;
}

export class PhraseOptionDTO implements IPhraseOptionDTO {
    constructor(
        public id?: number,
        public name?: string,
        public language?: string,
        public header?: string,
        public version?: string,
        public basisVersion?: string,
        public remark?: string,
        public deleted?: boolean,
        public domainId?: number,
        public datenew?: Moment,
        public datelast?: Moment,
        public moduleNo?: number,
        public itemNo?: number
    ) {}
}
