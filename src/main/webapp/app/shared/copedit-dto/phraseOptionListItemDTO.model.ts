import { Moment } from 'moment';

export interface IPhraseOptionListItemDTO {
    id?: number;
    name?: string;
    header?: string;
    datelast?: Moment;
    nrOfSentences?: number;
}

export class PhraseOptionListItemDTO implements IPhraseOptionListItemDTO {
    constructor(public id?: number, public name?: string, public header?: string, public datelast?: Moment, public nrOfSentences?: number) { }
}
