import { Moment } from 'moment';
import { PhraseOptionDTO } from 'app/shared/copedit-dto/phraseOptionDTO.model';

export interface ISentenceLanguageDTO {
    id?: number;
    name?: string;
    header?: string;
    language?: String;
    remark?: String;
    updatedDate?: Moment;
    createdDate?: Moment;
    structure?: string;
    phraseOptions?: Array<PhraseOptionDTO>;
}

export class SentenceLanguageDTO implements ISentenceLanguageDTO {
    constructor(
        public id?: number,
        public name?: string,
        public header?: string,
        public language?: String,
        public remark?: String,
        public updatedDate?: Moment,
        public createdDate?: Moment,
        public structure?: string,
        public phraseOptions?: Array<PhraseOptionDTO>

    ) { }
}
