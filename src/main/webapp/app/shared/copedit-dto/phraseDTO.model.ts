import { Moment } from 'moment';

export interface IPhraseDTO {
    id?: number;
    value?: string;
    spaceBefore?: Boolean;
    spaceAfter?: Boolean;
    removePunctuationBefore?: Boolean;
    itemPartNo?: number;
    itemNo?: number;
    incorrect?: Boolean;
    datenew?: Moment;
    datelast?: Moment;
}

export class PhraseDTO implements IPhraseDTO {
    constructor(
        public id?: number,
        public value?: string,
        public spaceBefore?: Boolean,
        public spaceAfter?: Boolean,
        public removePunctuationBefore?: Boolean,
        public itemPartNo?: number,
        public itemNo?: number,
        public incorrect?: Boolean,
        public datenew?: Moment,
        public datelast?: Moment
    ) {
        this.spaceAfter = false;
        this.spaceBefore = false;
        this.incorrect = false;
        this.removePunctuationBefore = false;
    }
}
