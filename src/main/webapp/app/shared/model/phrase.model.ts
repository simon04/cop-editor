import { Moment } from 'moment';

export interface IPhrase {
    id?: number;
    value?: string;
    spaceBefore?: boolean;
    spaceAfter?: boolean;
    removePunctuationBefore?: boolean;
    itemNo?: number;
    itemPartNo?: number;
    incorrect?: boolean;
    phraseOptionItemId?: number;
    datelast?: Moment;
    datenew?: Moment;
}

export class Phrase implements IPhrase {
    constructor(
        public id?: number,
        public value?: string,
        public spaceBefore?: boolean,
        public spaceAfter?: boolean,
        public removePunctuationBefore?: boolean,
        public itemNo?: number,
        public itemPartNo?: number,
        public incorrect?: boolean,
        public phraseOptionItemId?: number,
        public datelast?: Moment,
        public datenew?: Moment
    ) {
        this.spaceBefore = false;
        this.spaceAfter = false;
        this.removePunctuationBefore = false;
        this.incorrect = false;
    }
}
