import { Moment } from 'moment';

export interface IDomain {
    id?: number;
    name?: string;
    datenew?: Moment;
    datelast?: Moment;
}

export class Domain implements IDomain {
    constructor(public id?: number, public name?: string, public datenew?: Moment, public datelast?: Moment) {}
}
