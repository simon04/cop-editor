import { Moment } from 'moment';

export const enum LanguageEnum {
    DE = 'DE',
    EN = 'EN',
    IT = 'IT',
    FR = 'FR'
}

export interface ISentenceLang {
    id?: number;
    name?: string;
    header?: string;
    language?: LanguageEnum;
    updatedDate?: Moment;
    structure?: string;
    remark?: string;
}

export class SentenceLang implements ISentenceLang {
    constructor(
        public id?: number,
        public name?: string,
        public header?: string,
        public language?: LanguageEnum,
        public updatedDate?: Moment,
        public structure?: string,
        public remark?: string
    ) {}
}
