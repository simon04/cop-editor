import { Moment } from 'moment';
import { IPhraseOption } from 'app/shared/model//phrase-option.model';

export const enum LanguageEnum {
    DE = 'DE',
    EN = 'EN',
    IT = 'IT',
    FR = 'FR'
}

export interface ISentenceLanguage {
    id?: number;
    name?: string;
    header?: string;
    language?: LanguageEnum;
    updatedDate?: Moment;
    structure?: string;
    remark?: string;
    phraseOptions?: IPhraseOption[];
}

export class SentenceLanguage implements ISentenceLanguage {
    constructor(
        public id?: number,
        public name?: string,
        public header?: string,
        public language?: LanguageEnum,
        public updatedDate?: Moment,
        public structure?: string,
        public remark?: string,
        public phraseOptions?: IPhraseOption[]
    ) {}
}
