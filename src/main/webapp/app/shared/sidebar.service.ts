import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SidebarService {
    private subject = new Subject<any>();

    showSidebar(show: boolean) {
        this.subject.next(show);
    }

    clearMessage() {
        this.subject.next();
    }

    getSidebarShowStatus(): Observable<any> {
        return this.subject.asObservable();
    }
}
