import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { AppSharedModule } from 'app/shared';
import { AppCoreModule } from 'app/core';
import { AppAppRoutingModule } from './app-routing.module';
import { AppHomeModule } from './home/home.module';
import { AppAccountModule } from './account/account.module';
import { PanelMenuModule } from 'primeng/panelmenu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SidebarModule } from 'primeng/sidebar';
import { TooltipModule } from 'primeng/tooltip';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { AppDomainModule } from 'app/entities/domain/domain.module';
import { AppSentenceModule } from 'app/entities/sentence/sentence.module';
import { CopeditPhraseModule } from 'app/forms/copedit-phrases/copedit-phrases.module';
import { CopeditSentencesModule } from 'app/forms/copedit-sentences/copedit-sentences.module';
import { ButtonModule } from 'primeng/primeng';
import { CopeditImportModule } from 'app/forms/copedit-import/copedit-import.module';
import { CopeditExportModule } from 'app/forms/copedit-export/copedit-export.module';
import { CopeditRecodeDomainModule } from './forms/copedit-recode-domain/copedit-recode-domain.module';

@NgModule({
    imports: [
        BrowserModule,
        AppAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        AppSharedModule,
        AppCoreModule,
        AppHomeModule,
        AppAccountModule,
        PanelMenuModule,
        BrowserAnimationsModule,
        AppDomainModule,
        AppSentenceModule,
        CopeditSentencesModule,
        CopeditPhraseModule,
        FontAwesomeModule,
        SidebarModule,
        TooltipModule,
        ButtonModule,
        CopeditImportModule,
        CopeditExportModule,
        CopeditRecodeDomainModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        }
    ],
    bootstrap: [JhiMainComponent]
})
export class AppAppModule {}
