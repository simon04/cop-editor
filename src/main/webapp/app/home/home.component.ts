import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LoginModalService, Principal, Account } from 'app/core';
import { LocalStorageService, LocalStorage } from '../../../../../node_modules/ngx-webstorage';
import { DomainDTO } from '../shared/copedit-dto/domainDTO.model';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { CopeditLookupService } from 'app/forms/copedit-shared/copedit-lookup.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss'],
    providers: [LocalStorageService, CopeditLookupService]
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    @LocalStorage('currentDomain')
    public currentDomain: DomainDTO;
    domains: Array<DomainDTO>;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private localstorageService: LocalStorageService,
        private route: ActivatedRoute,
        private copEditLookupService: CopeditLookupService
    ) { }

    ngOnInit() {
        /* set default domain to PRODUCTION */
        if (this.currentDomain === undefined) {
            this.currentDomain = { id: 1, name: 'PRODUCTION'/*, datenew: null, datelast: null */ };
        }
        this.principal.identity().then(account => {
            this.account = account;
            this.copEditLookupService.getDomains().subscribe(res => {
                this.domains = res.body;
            });
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.principal.identity().then(account => {
                this.account = account;
                this.copEditLookupService.getDomains().subscribe(res => {
                    this.domains = res.body;
                });
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
