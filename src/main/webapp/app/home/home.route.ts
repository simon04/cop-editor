import { Route } from '@angular/router';

import { HomeComponent } from './';
import { DomainssResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';

export const HOME_ROUTE: Route = {
    path: '',
    component: HomeComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
