import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/primeng';
import { CardModule } from 'primeng/card';
import { DropdownModule } from 'primeng/dropdown';

import { AppSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { DomainssResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';

@NgModule({
    imports: [AppSharedModule, RouterModule.forChild([HOME_ROUTE]), ButtonModule, CardModule, DropdownModule],
    declarations: [HomeComponent],
    providers: [DomainssResolver],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppHomeModule { }
