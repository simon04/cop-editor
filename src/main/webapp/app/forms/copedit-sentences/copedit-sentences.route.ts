import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { Sentence } from 'app/shared/model/sentence.model';
import { ISentence } from 'app/shared/model/sentence.model';
import { SentenceService } from 'app/entities/sentence/sentence.service';
import { VersionsResolver, DomainssResolver, PhrasesResolver, CopeditSentenceResolver, CopeditNewSentenceResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { SentencesListComponent } from 'app/forms/copedit-sentences/copedit-sentences-list/copedit-sentences-list.component';
import { CopeditSentenceEditComponent } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentence-edit.component';
import { PhraseOptionsDEResolver, PhraseOptionsITResolver, PhraseOptionsENResolver, PhraseOptionsFRResolver } from 'app/forms/copedit-shared/copedit-phrase-options.service';

@Injectable({ providedIn: 'root' })
export class SentenceResolve implements Resolve<ISentence> {
    constructor(private service: SentenceService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((sentence: HttpResponse<Sentence>) => sentence.body);
        }
        return Observable.of(new Sentence());
    }
}

export const copeditSentenceRoute: Routes = [
    {
        path: 'sentencesList',
        component: SentencesListComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams,
            versions: VersionsResolver,
            domains: DomainssResolver,
            phraseOptions: PhrasesResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.sentence.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentenceEdit/:name',
        component: CopeditSentenceEditComponent,
        resolve: {
            sentence: CopeditSentenceResolver,
            phraseOptionsDE: PhraseOptionsDEResolver,
            phraseOptionsIT: PhraseOptionsITResolver,
            phraseOptionsEN: PhraseOptionsENResolver,
            phraseOptionsFR: PhraseOptionsFRResolver,
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentence.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'newSentence',
        component: CopeditSentenceEditComponent,
        resolve: {
            sentence: CopeditNewSentenceResolver,
            phraseOptionsDE: PhraseOptionsDEResolver,
            phraseOptionsIT: PhraseOptionsITResolver,
            phraseOptionsEN: PhraseOptionsENResolver,
            phraseOptionsFR: PhraseOptionsFRResolver,
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentence.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
