import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, ConfirmationService } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';
import { ISentence } from 'app/shared/model/sentence.model';
import { JhiParseLinks } from 'ng-jhipster';
import { Version } from 'app/shared/model/version.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Domain } from 'app/shared/model/domain.model';
import { PhraseOption } from 'app/shared/model/phrase-option.model';
import { CopeditSentencesService } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { MessageService } from '../../../../../../../node_modules/primeng/components/common/messageservice';
import { LocalStorageService, LocalStorage } from '../../../../../../../node_modules/ngx-webstorage';
import { DomainDTO } from 'app/shared/copedit-dto/domainDTO.model';
import { SidebarService } from 'app/shared/sidebar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-sentences-list',
    templateUrl: 'copedit-sentences-list.component.html',
    providers: [MessageService, LocalStorageService]
})
export class SentencesListComponent implements OnInit {
    viewDeleted: Boolean;
    viewJoker: Boolean;
    breadcrumbs: MenuItem[];
    home: MenuItem;
    sentences: ISentence[];
    loading: Boolean;
    totalRecords: number;
    currentPageNr;
    first: any;
    versions: Array<Version>;
    domains: Array<Domain>;
    positions: Array<any>;
    phraseOptions: Array<PhraseOption>;
    multiSortMeta = [];
    @ViewChild('dt') dt;
    @LocalStorage('currentDomain') public currentDomain: DomainDTO;

    constructor(
        private sentencesService: CopeditSentencesService,
        private parseLinks: JhiParseLinks,
        private route: ActivatedRoute,
        private router: Router,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private localStorageService: LocalStorageService,
        private sidebarService: SidebarService,
        private translateService: TranslateService
    ) {}

    ngOnInit() {
        this.sidebarService.showSidebar(false);
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.first = 0;
        this.breadcrumbs = [{ label: 'Sentences' }];
        /* init filter on domain */
        this.dt.filters = {
            domain: { value: this.currentDomain.id, matchMode: 'equals' }
        };

        this.versions = this.route.snapshot.data.versions;
        this.domains = this.route.snapshot.data.domains;
        this.phraseOptions = this.route.snapshot.data.phraseOptions;
        this.positions = [
            { label: 1, value: 1 },
            { label: 2, value: 2 },
            { label: 3, value: 3 },
            { label: 4, value: 4 },
            { label: 5, value: 5 },
            { label: 6, value: 6 },
            { label: 7, value: 7 },
            { label: 8, value: 8 },
            { label: 9, value: 9 },
            { label: 10, value: 10 }
        ];

        this.loading = true;
    }

    loadNextPage($event) {
        /* compose search filters and pass to service */
        this.currentPageNr = $event.first / $event.rows;
        const filter = {
            page: this.currentPageNr,
            size: 20
        };
        /* add sorting */
        if ($event.multiSortMeta !== undefined) {
            $event.multiSortMeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }

        Object.keys($event.filters).forEach(element => {
            filter[element] = $event.filters[element].value;
        });
        this.loading = true;
        this.sentencesService.findAll(filter).subscribe(
            res => {
                (<any>this.sentences) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    filterData($event) {
        /* compose search filters and pass to service */
        const filter = {
            page: this.currentPageNr,
            size: 20
        };
        Object.keys($event.filters).forEach(element => {
            filter[element] = $event.filters[element].value;
        });
        /* add sorting */
        if ($event.multisortmeta !== undefined) {
            $event.multisortmeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }

        this.sentencesService.findAll(filter).subscribe(
            res => {
                (<any>this.sentences) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    newSentence() {
        this.router.navigate(['newSentence']);
    }

    editSentence(name) {
        this.router.navigate(['sentenceEdit', name]);
    }

    sortSentences($event) {
        this.loading = true;
        const filter = {
            page: this.currentPageNr,
            size: 20,
            domain: this.currentDomain.id
        };
        /* add sorting */
        if ($event.multisortmeta !== undefined) {
            $event.multisortmeta.forEach(field => {
                let dir;
                if (field.order === 1) {
                    dir = 'asc';
                } else if (field.order === -1) {
                    dir = 'desc';
                }
                filter['sort'] = [field.field + ',' + dir];
            });
        }
        if ($event.hasOwnProperty('filters')) {
            Object.keys($event.filters).forEach(element => {
                filter[element] = $event.filters[element].value;
            });
        }

        this.sentencesService.findAll(filter).subscribe(
            res => {
                (<any>this.sentences) = res.body;
                this.totalRecords = Number(res.headers.get('x-total-count'));
                this.loading = false;
            },
            (err: HttpErrorResponse) => {
                console.error(err);
            }
        );
    }

    confirmDelete(sentenceName: string) {
        this.confirmationService.confirm({
            message: this.translateService.instant('appApp.sentenceList.grid.messages.deleteConfirmationMessage', { name: sentenceName }),
            accept: () => {
                this.sentencesService.delete(sentenceName).subscribe(
                    res => {
                        this.filterData({ filters: this.dt.filters });
                        this.messageService.add({
                            severity: 'success',
                            detail: this.translateService.instant('appApp.sentenceList.grid.messages.deleteSuccessMessage', {
                                name: sentenceName
                            })
                        });
                    },
                    err => {
                        this.messageService.add({
                            severity: 'error',
                            detail: this.translateService.instant('appApp.sentenceList.grid.messages.deleteErrorMessage')
                        });
                        console.error(err);
                    }
                );
            }
        });
    }
}
