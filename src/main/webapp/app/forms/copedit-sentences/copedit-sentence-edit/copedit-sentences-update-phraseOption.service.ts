import { Injectable } from '@angular/core';
import { Subject } from '../../../../../../../node_modules/rxjs';

@Injectable()
export class UpdatePhraseOptionSharedService {

    private phraseOption = new Subject<any>();

    phraseOption$ = this.phraseOption.asObservable();

    // Service message commands
    publishData(data: any) {
        this.phraseOption.next(data);

    }
    constructor() { }
}
