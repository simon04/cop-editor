import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { PhraseOptionDTO } from 'app/shared/copedit-dto/phraseOptionDTO.model';
import { SentenceLanguageDTO } from 'app/shared/copedit-dto/sentenceLanguageDTO.model';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { faSitemap } from '@fortawesome/free-solid-svg-icons';
import { UpdatePhraseOptionSharedService } from 'app/forms/copedit-sentences/copedit-sentence-edit/copedit-sentences-update-phraseOption.service';
import { MessageService } from '../../../../../../../node_modules/primeng/components/common/messageservice';
import { Message } from '../../../../../../../node_modules/primeng/components/common/api';
import { structureRegex } from 'app/shared/constants/copedit.constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-copedit-phrase-option-editor',
    templateUrl: 'copedit-phrase-option-editor.component.html',
    providers: [MessageService]
})
export class CopeditPhraseOptionEditorComponent implements OnInit {
    @Input('phraseOptions') phraseOptions: Array<PhraseOptionDTO>;
    @Input('sentenceLanguage') sentenceLanguage: SentenceLanguageDTO;
    @Input('allLanguages') allLanguages: boolean;

    faEdit = faEdit;
    faPlusSquare = faPlusSquare;
    faSitemap = faSitemap;
    selectedPhraseOption: PhraseOptionDTO;
    options: any = {
        removeOnSpill: true
    };
    filteredPhraseOptions: Array<PhraseOptionDTO>;
    sentenceModule: any;

    constructor(
        private dragulaService: DragulaService,
        private updatePhraseOptionSharedService: UpdatePhraseOptionSharedService,
        private messageService: MessageService,
        private router: Router
    ) {}

    ngOnInit() {
        this.filteredPhraseOptions = this.phraseOptions;
        this.updatePhraseOptionSharedService.phraseOption$.subscribe(res => {
            const moduleToUpdate = this.sentenceLanguage.phraseOptions.find(x => x.name === res.prevName);
            if (moduleToUpdate !== undefined) {
                // find header by language
                const header = this.phraseOptions.find((x: any) => x.name === res.updatedName);
                if (header !== undefined) {
                    //  substitute
                    const index = this.sentenceLanguage.phraseOptions.findIndex(x => x.name === res.prevName);
                    const itemNo = this.sentenceLanguage.phraseOptions[index].itemNo;
                    const module = this.sentenceLanguage.phraseOptions[index].moduleNo;
                    this.sentenceLanguage.phraseOptions[index] = header;
                    this.sentenceLanguage.phraseOptions[index].itemNo = itemNo;
                    this.sentenceLanguage.phraseOptions[index].moduleNo = module;
                }
            }
        });

        this.dragulaService.drop.subscribe(value => {
            this.syncModules();
        });

        this.dragulaService.removeModel.subscribe(value => {
            this.syncModules();
        });

        // the order of phraseOptions is defined in  sentenceLanguage.structure
        /*  const orderStructure = [];
         this.sentenceLanguage.structure.split(',').forEach(element => {
             const y = +element.split('.')[0] - 1;
             const tmp = Object.assign({}, this.sentenceLanguage.phraseOptions[y]);
             // check language exception
             if (+element.split('.')[1] === 2) {
                 tmp.itemNo = 2;
             } else {
                 tmp.itemNo = 1;
             }
             orderStructure.push(tmp);
         });
         this.sentenceLanguage.phraseOptions = orderStructure; */
        // this.reorderStructure();
    }

    syncModules() {
        let s = '';
        this.sentenceLanguage.phraseOptions.forEach(element => {
            // check exception
            s = s + element.moduleNo + '.' + element.itemNo + ',';
        });
        this.sentenceLanguage.structure = s.substring(0, s.length - 1);
    }

    addPhraseOption() {
        if (this.selectedPhraseOption) {
            const newPos = this.sentenceLanguage.phraseOptions.length + 1;
            this.selectedPhraseOption.moduleNo = newPos;
            this.selectedPhraseOption.itemNo = 1;
            this.sentenceLanguage.phraseOptions.push(this.selectedPhraseOption);
            this.syncModules();
            this.selectedPhraseOption = undefined;
        }
    }

    gotoPhraseOption(idx) {
        this.router.navigate(['phraseEdit', this.sentenceLanguage.phraseOptions[idx].name]);
    }

    /**
     * If the option 'update all languages' is enabled, updates all languages searching for the phraseOption matching the
     * previous name and updating it with the one matching the new value for the given language.
     * ItemNo and moduleNo are maintained
     *
     * @private
     * @param {*} event new value
     * @param {*} prev previous phraseOption value
     * @param {*} idx index of the phraseOption
     * @memberof CopeditPhraseOptionEditorComponent
     */
    updatePhraseOptionInLanguages(event, prev) {
        if (event !== prev) {
            if (this.allLanguages) {
                this.updatePhraseOptionSharedService.publishData({
                    moduleNo: prev.moduleNo,
                    prevName: prev.name,
                    // tslint:disable-next-line:object-literal-shorthand
                    updatedName: event.name
                });
            }
        }
    }

    filterPhraseOptions($event) {
        this.filteredPhraseOptions = this.phraseOptions.filter(x => x.name.toLowerCase().includes($event.query));
    }

    /**
     * Validates structure field and updates array of phraseOption accordingly
     *
     * @memberof CopeditPhraseOptionEditorComponent
     */
    updatePhraseOptionsFromStructure() {
        this.messageService.clear();
        if (!this.isStructureValid()) {
            return;
        }
        const struct = this.sentenceLanguage.structure.split(',');
        // if valid, reorder array by structure
        const reorderedPhraseOptions = [];
        struct.forEach(x => {
            const element = x.split('.');
            reorderedPhraseOptions.push(this.sentenceLanguage.phraseOptions[Number(element[0]) - 1]);
        });
        this.sentenceLanguage.phraseOptions = reorderedPhraseOptions;
        this.messageService.add({ severity: 'success', detail: 'Phrase options reordered successfully' });
    }

    /**
     * Checks if structure is valid matching it to the phraseOptions array and testing it against regex and length
     *
     * @private
     * @returns {Boolean} true if structure is valid, else false
     * @memberof CopeditPhraseOptionEditorComponent
     */
    private isStructureValid(): Boolean {
        const struct = this.sentenceLanguage.structure.split(',');
        const errorMsg: Message[] = [];
        // check number of elements match
        if (struct.length !== this.sentenceLanguage.phraseOptions.length) {
            console.error('The number of elements does not match');
            this.messageService.add({ severity: 'error', detail: 'The number of elements does not match' });
            return false;
        }
        // check regex match
        if (!structureRegex.test(this.sentenceLanguage.structure)) {
            console.error('Invalid structure format');
            this.messageService.add({ severity: 'error', detail: 'Invalid structure format' });
            return false;
        }
        // check existence of every element
        let isValid: Boolean = true;

        struct.forEach(x => {
            const element: Array<string> = x.split('.');
            const moduleNO = Number(element[0]);
            const itemNO = Number(element[1]);
            const result: PhraseOptionDTO = this.sentenceLanguage.phraseOptions.find(
                el => el.itemNo === itemNO && el.moduleNo === moduleNO
            );
            if (result === undefined) {
                console.error('Element ' + x + ' does not exist');
                errorMsg.push({ severity: 'error', detail: 'Element ' + x + ' does not exist' });
                isValid = false;
            }
            // check duplicated/not present elements
            const nrOfOccurences: number = struct.filter(el => el === x).length;
            if (nrOfOccurences > 1) {
                console.error('Element ' + element + ' is repeated more than once');
                if (errorMsg.find(el => el.detail === 'Element ' + element + ' is repeated more than once') === undefined) {
                    errorMsg.push({ severity: 'error', detail: 'Element ' + element + ' is repeated more than once' });
                }
                isValid = false;
            }
        });
        if (errorMsg.length > 0) {
            this.messageService.addAll(errorMsg);
        }
        return isValid;
    }
}
