import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { CopeditPhrasesListComponent } from 'app/forms/copedit-phrases/copedit-phrases-list/copedit-phrases-list.component';
import { ISentence, Sentence } from 'app/shared/model/sentence.model';
import { SentenceService } from 'app/entities/sentence';
import { VersionsResolver, DomainssResolver, PhrasesResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { CopeditPhraseEditComponent } from 'app/forms/copedit-phrases/copedit-phrase-edit/copedit-phrase-edit.component';
import { PhraseResolver, NewPhraseResolver } from 'app/forms/copedit-phrases/copedit-phrase.service';

@Injectable({ providedIn: 'root' })
export class SentenceResolve implements Resolve<ISentence> {
    constructor(private service: SentenceService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((sentence: HttpResponse<Sentence>) => sentence.body);
        }
        return Observable.of(new Sentence());
    }
}

export const copeditPhrasesRoute: Routes = [
    {
        path: 'phrasesList',
        component: CopeditPhrasesListComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams,
            versions: VersionsResolver,
            domains: DomainssResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phraseEdit/:name',
        component: CopeditPhraseEditComponent,
        resolve: {
            phrase: PhraseResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentence.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'newPhrase',
        component: CopeditPhraseEditComponent,
        resolve: {
            phrase: NewPhraseResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentence.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
