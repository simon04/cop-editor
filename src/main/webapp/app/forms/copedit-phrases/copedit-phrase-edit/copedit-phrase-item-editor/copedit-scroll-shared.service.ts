import { Injectable } from '@angular/core';
import { Subject } from '../../../../../../../../node_modules/rxjs';

@Injectable()
export class CopeditScrollSharedService {

    private scrollEvent = new Subject<any>();

    scrollEvent$ = this.scrollEvent.asObservable();

    // Service message commands
    sharedScroll(data: any) {
        this.scrollEvent.next(data);

    }
    constructor() { }
}
