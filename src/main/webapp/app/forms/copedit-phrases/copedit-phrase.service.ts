import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Phrase } from 'app/shared/model/phrase.model';
import { PhraseOption } from 'app/shared/model/phrase-option.model';
import { LocalStorageService } from '../../../../../../node_modules/ngx-webstorage';
import { PhraseOptionListItemDTO } from 'app/shared/copedit-dto/phraseOptionListItemDTO.model';
import { PhraseOptionEditDTO } from 'app/shared/copedit-dto/phraseEditDTO.model';
import { IPhraseOptionEditDTO } from '../../shared/copedit-dto/phraseEditDTO.model';

type EntityResponseType = HttpResponse<IPhraseOptionEditDTO>;
type EntityArrayResponseType = HttpResponse<IPhraseOptionEditDTO[]>;

@Injectable({ providedIn: 'root' })
export class CopEditPhraseService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/phrases';
    private phraseOptionsURL = SERVER_API_URL + 'api/copedit/phraseOptions';

    constructor(private http: HttpClient, private localStorageService: LocalStorageService) {}

    create(sentence: IPhraseOptionEditDTO): Observable<EntityResponseType> {
        return this.http.post<IPhraseOptionEditDTO>(this.resourceUrl, sentence, { observe: 'response' });
    }

    update(sentence: IPhraseOptionEditDTO): Observable<EntityResponseType> {
        return this.http.put<IPhraseOptionEditDTO>(this.resourceUrl, sentence, { observe: 'response' });
    }

    getList(req?: any): Observable<HttpResponse<PhraseOption[]>> {
        const options = createRequestOption(req);
        return this.http.get<Phrase[]>(SERVER_API_URL + 'api/copedit/phraseOptionsList', { params: options, observe: 'response' });
    }

    findAll(req?: any): Observable<HttpResponse<PhraseOptionListItemDTO[]>> {
        const options = createRequestOption(req);
        return this.http.get<Phrase[]>(this.phraseOptionsURL, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        /* TODO */
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    /**
     * Find all phrases by name
     *
     * @param {string} name name of the phrase to find
     * @returns {Observable<HttpResponse<PhraseOptionEditDTO>>}
     * @memberof CopEditPhraseService
     */
    findPhraseByName(name: string): Observable<HttpResponse<PhraseOptionEditDTO>> {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.http.get<any>(`${this.resourceUrl}/${name}?domain=${currentDomain.id}`, { observe: 'response' });
    }

    /**
     * Gets a new empty phrase
     *
     * @returns {Observable<EntityResponseType>} an empty phrase
     * @memberof CopEditPhraseService
     */
    getNewPhrase(): Observable<EntityResponseType> {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.http.get<IPhraseOptionEditDTO>(`${this.resourceUrl}/newPhrase?domain=${currentDomain.id}`, { observe: 'response' });
    }
}

/* RESOLVERS */
@Injectable()
export class PhrasesResolver implements Resolve<PhraseOptionListItemDTO[]> {
    constructor(private copeditService: CopEditPhraseService) {}

    resolve() {
        return this.copeditService.findAll().map((x: HttpResponse<PhraseOptionListItemDTO[]>) => x.body);
    }
}

@Injectable()
export class PhraseResolver implements Resolve<PhraseOptionEditDTO> {
    constructor(private copEditPhraseService: CopEditPhraseService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.copEditPhraseService.findPhraseByName(route.paramMap.get('name')).map((x: HttpResponse<PhraseOptionEditDTO>) => x.body);
    }
}
@Injectable()
export class NewPhraseResolver implements Resolve<PhraseOptionEditDTO> {
    constructor(private copEditPhraseService: CopEditPhraseService) {}

    resolve() {
        return this.copEditPhraseService.getNewPhrase().map((x: HttpResponse<PhraseOptionEditDTO>) => x.body);
    }
}
