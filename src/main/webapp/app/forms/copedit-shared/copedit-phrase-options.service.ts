import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISentence } from 'app/shared/model/sentence.model';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Phrase } from 'app/shared/model/phrase.model';
import { PhraseOption } from 'app/shared/model/phrase-option.model';
import { PhraseOptionDTO } from '../../shared/copedit-dto/phraseOptionDTO.model';
import { LocalStorageService } from '../../../../../../node_modules/ngx-webstorage';
import { PhraseOptionListItemDTO } from 'app/shared/copedit-dto/phraseOptionListItemDTO.model';
import { PhraseOptionEditDTO } from 'app/shared/copedit-dto/phraseEditDTO.model';

type EntityResponseType = HttpResponse<ISentence>;
type EntityArrayResponseType = HttpResponse<ISentence[]>;

@Injectable({ providedIn: 'root' })
export class CopEditPhraseOptionsService {
    // private resourceUrl = SERVER_API_URL + 'api/copedit/phrases';
    private phraseOptionsURL = SERVER_API_URL + 'api/copedit/phrasesOptionsLang';

    constructor(private http: HttpClient) {}

    getList(req?: any): Observable<HttpResponse<PhraseOption[]>> {
        const options = createRequestOption(req);
        return this.http.get<Phrase[]>(SERVER_API_URL + 'api/copedit/phrasesList', { params: options, observe: 'response' });
    }

    findAll(req?: any): Observable<HttpResponse<PhraseOptionListItemDTO[]>> {
        const options = createRequestOption(req);
        return this.http.get<Phrase[]>(`${this.phraseOptionsURL}`, { params: options, observe: 'response' });
    }

    findPhraseOptionsByLang(lang: any): Observable<HttpResponse<PhraseOptionDTO[]>> {
        const options = createRequestOption(lang);
        return this.http.get<any>(`${this.phraseOptionsURL}`, { params: options, observe: 'response' });
    }
}

@Injectable()
export class PhraseOptionsDEResolver implements Resolve<PhraseOptionDTO[]> {
    constructor(private copEditPhraseOptionsService: CopEditPhraseOptionsService, private localStorageService: LocalStorageService) {}

    resolve() {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.copEditPhraseOptionsService
            .findPhraseOptionsByLang({ lang: 'de', domain: currentDomain.id })
            .map((x: HttpResponse<PhraseOptionDTO[]>) => x.body);
    }
}
@Injectable()
export class PhraseOptionsENResolver implements Resolve<PhraseOptionDTO[]> {
    constructor(private copEditPhraseOptionsService: CopEditPhraseOptionsService, private localStorageService: LocalStorageService) {}

    resolve() {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.copEditPhraseOptionsService
            .findPhraseOptionsByLang({ lang: 'en', domain: currentDomain.id })
            .map((x: HttpResponse<PhraseOptionDTO[]>) => x.body);
    }
}
@Injectable()
export class PhraseOptionsITResolver implements Resolve<PhraseOptionDTO[]> {
    constructor(private copEditPhraseOptionsService: CopEditPhraseOptionsService, private localStorageService: LocalStorageService) {}

    resolve() {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.copEditPhraseOptionsService
            .findPhraseOptionsByLang({ lang: 'it', domain: currentDomain.id })
            .map((x: HttpResponse<PhraseOptionDTO[]>) => x.body);
    }
}
@Injectable()
export class PhraseOptionsFRResolver implements Resolve<PhraseOptionDTO[]> {
    constructor(private copEditPhraseOptionsService: CopEditPhraseOptionsService, private localStorageService: LocalStorageService) {}

    resolve() {
        const currentDomain = this.localStorageService.retrieve('currentDomain');
        return this.copEditPhraseOptionsService
            .findPhraseOptionsByLang({ lang: 'fr', domain: currentDomain.id })
            .map((x: HttpResponse<PhraseOptionDTO[]>) => x.body);
    }
}
