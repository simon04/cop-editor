import { UserRouteAccessService } from 'app/core';
import { Routes } from '@angular/router';
import { DomainssResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { CopeditRecodeDomainComponent } from './copedit-recode-domain.component';

export const recodeDomainRoute: Routes = [
    {
        path: 'promoteDomain',
        component: CopeditRecodeDomainComponent,
        resolve: {
            domains: DomainssResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.promoteDomain.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
