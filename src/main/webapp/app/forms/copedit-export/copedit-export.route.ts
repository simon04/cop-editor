import { UserRouteAccessService } from 'app/core';
import { Routes } from '@angular/router';
import { CopeditExportComponent } from 'app/forms/copedit-export/copedit-export.component';
import { DomainssResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';

export const copeditExportRoute: Routes = [
    {
        path: 'export',
        component: CopeditExportComponent,
        resolve: {
            domains: DomainssResolver
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'appApp.export.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
