import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Domain } from 'app/shared/model/domain.model';
import { ActivatedRoute } from '@angular/router';
import { AuthServerProvider } from 'app/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { FileUpload } from 'primeng/primeng';
import * as SockJS from 'sockjs-client';
import * as Stomp from '@stomp/stompjs';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { SidebarService } from 'app/shared/sidebar.service';
import { CopeditImportService } from 'app/forms/copedit-import/copedit-import.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'jhi-copedit-import',
    templateUrl: 'copedit-import.component.html'
})
export class CopeditImportComponent implements OnInit, OnDestroy {
    domains: Array<Domain>;
    selectedDomain: Domain;
    uploadedFiles: Array<any> = [];
    @ViewChild('fileUpload') fileUpload: FileUpload;
    showProgressModal: Boolean = false;
    private stompClient;
    wsUrl: string;
    breadcrumbs: MenuItem[];
    home: MenuItem;
    socket: any;

    constructor(
        private route: ActivatedRoute,
        private authServerProvider: AuthServerProvider,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private sidebarService: SidebarService,
        private copeditImportService: CopeditImportService,
        private translateService: TranslateService
    ) {}

    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: [''] };
        this.breadcrumbs = [{ label: 'Import' }];
        this.domains = this.route.snapshot.data.domains;
        this.wsUrl = this.getWebsocketUrl(window.location.hostname);
        this.socket = new SockJS(this.wsUrl + '/import-websocket');
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect({}, frame => {
            console.log('Connected: ' + frame);
            this.stompClient.subscribe('/import/importStatus', greetin => {
                if (greetin.body === 'OK') {
                    this.showProgressModal = false;
                    this.messageService.add({
                        severity: 'success',
                        detail: this.translateService.instant('appApp.import.messages.importSuccessfulMessage')
                    });
                } else if (greetin.body === 'FATAL') {
                    this.showProgressModal = false;
                    this.messageService.add({
                        severity: 'error',
                        detail: this.translateService.instant('appApp.import.messages.importErrorMessage')
                    });
                } else {
                    this.showProgressModal = false;
                    this.messageService.add({
                        severity: 'error',
                        detail: this.translateService.instant('appApp.import.messages.importErrorMessage')
                    });
                }
            });
        });
        this.sidebarService.showSidebar(false);
    }

    ngOnDestroy(): void {
        this.stompClient.unsubscribe();
        this.socket.close();
    }

    /**
     * Adds authorization token to request
     *
     * @param {*} event
     * @memberof CopeditImportComponent
     */
    addAuthHeaders(event) {
        const token = this.authServerProvider.getToken();
        event.xhr.setRequestHeader('Authorization', `Bearer ${token}`);
        this.showProgressModal = true;
    }

    /**
     * Checks that the correct number of files is provided for the upload and that their names are correct
     *
     * @param {*} $event
     * @returns {Boolean}
     * @memberof CopeditImportComponent
     */
    checkFiles(): Boolean {
        if (this.fileUpload.files.length !== 4) {
            this.messageService.add({
                severity: 'error',
                detail: this.translateService.instant('appApp.import.messages.invalidNrFilesMessage')
            });
            return false;
        }
        const files = [];
        for (let i = 0; i < this.fileUpload.files.length; i++) {
            files.push(this.fileUpload.files[i]);
        }
        const de: Boolean = files.find(x => x.name === 'de.zip') !== undefined;
        const it: Boolean = files.find(x => x.name === 'it.zip') !== undefined;
        const en: Boolean = files.find(x => x.name === 'en.zip') !== undefined;
        const fr: Boolean = files.find(x => x.name === 'fr.zip') !== undefined;

        if ((de && it && en && fr) === false) {
            this.messageService.add({
                severity: 'error',
                detail: this.translateService.instant('appApp.import.messages.invalidFileNameMessage')
            });
            return false;
        }
        return true;
    }

    /**
     * Display error message on error during upload
     *
     * @param {*} $event browser event
     * @memberof CopeditImportComponent
     */
    onError($event) {
        this.showProgressModal = false;
        this.fileUpload.files = $event.files;
        this.messageService.add({ severity: 'error', detail: JSON.parse($event.xhr.response).message });
    }

    /**
     * Return the base URL of the import websocket based on the environment the app is running in
     *
     * @param {*} url hostname of the window
     * @returns base URL of the import websocket
     * @memberof CopeditImportComponent
     */
    getWebsocketUrl(url) {
        if (url === 'localhost') {
            return 'http://localhost:8081';
        } else if (url === 'supporto2.clesius.it') {
            return 'http://supporto2.clesius.it/copedit';
        } else {
            // TODO: change prod url
            return `http://${url}/copedit`;
        }
    }

    askConfirmation() {
        const filesAreValid: Boolean = this.checkFiles();
        if (filesAreValid) {
            this.confirmationService.confirm({
                header: this.translateService.instant('appApp.import.fileUpload.confirmationHeader'),
                message: this.translateService.instant('appApp.import.fileUpload.confirmationMessage'),
                icon: 'fa fa-warning',
                accept: () => {
                    this.showProgressModal = true;
                    this.copeditImportService.uploadFiles(this.fileUpload.files, this.selectedDomain.name).subscribe(
                        res => {
                            this.fileUpload.files = [];
                            this.selectedDomain = undefined;
                        },
                        err => {
                            this.showProgressModal = false;
                            console.log(err);
                        }
                    );
                }
            });
        }
    }
}
