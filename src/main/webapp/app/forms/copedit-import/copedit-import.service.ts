import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CopeditImportService {
    private resourceUrl = SERVER_API_URL + 'api/copedit/import';

    constructor(private http: HttpClient) {}

    uploadFiles(files: File[], domain: string) {
        const formData = new FormData();
        files.forEach(f => {
            formData.append('files[]', f);
        });
        const head = new Headers();
        head.append('Content-Type', 'multipart/form-data');
        head.append('Accept', 'application/json');
        return this.http.post<any>(this.resourceUrl + `?domain=${domain}`, formData, { observe: 'response' });
    }
}
