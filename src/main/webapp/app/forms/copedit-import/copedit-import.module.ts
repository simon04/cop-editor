import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DomainssResolver } from 'app/forms/copedit-sentences/copedit-sentences.service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { copeditImportRoute } from 'app/forms/copedit-import/copedit-import.route';
import { CopeditImportComponent } from 'app/forms/copedit-import/copedit-import.component';
import { FileUploadModule } from 'primeng/fileupload';
import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { DialogModule } from 'primeng/dialog';
import { ProgressBarModule } from 'primeng/progressbar';
import { CopeditImportService } from 'app/forms/copedit-import/copedit-import.service';

const ENTITY_STATES = [...copeditImportRoute];

@NgModule({
    imports: [
        AppSharedModule,
        FieldsetModule,
        CheckboxModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        BreadcrumbModule,
        RouterModule.forChild(ENTITY_STATES),
        MessagesModule,
        MessageModule,
        GrowlModule,
        ConfirmDialogModule,
        FileUploadModule,
        DialogModule,
        ProgressBarModule
    ],
    declarations: [CopeditImportComponent],
    providers: [DomainssResolver, AuthServerProvider, MessageService, CopeditImportService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CopeditImportModule {}
