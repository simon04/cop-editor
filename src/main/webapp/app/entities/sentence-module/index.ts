export * from './sentence-module.service';
export * from './sentence-module-update.component';
export * from './sentence-module-delete-dialog.component';
export * from './sentence-module-detail.component';
export * from './sentence-module.component';
export * from './sentence-module.route';
