import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { SentenceModule } from 'app/shared/model/sentence-module.model';
import { SentenceModuleService } from './sentence-module.service';
import { SentenceModuleComponent } from './sentence-module.component';
import { SentenceModuleDetailComponent } from './sentence-module-detail.component';
import { SentenceModuleUpdateComponent } from './sentence-module-update.component';
import { SentenceModuleDeletePopupComponent } from './sentence-module-delete-dialog.component';
import { ISentenceModule } from 'app/shared/model/sentence-module.model';

@Injectable({ providedIn: 'root' })
export class SentenceModuleResolve implements Resolve<ISentenceModule> {
    constructor(private service: SentenceModuleService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((sentenceModule: HttpResponse<SentenceModule>) => sentenceModule.body);
        }
        return Observable.of(new SentenceModule());
    }
}

export const sentenceModuleRoute: Routes = [
    {
        path: 'sentence-module',
        component: SentenceModuleComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceModule.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-module/:id/view',
        component: SentenceModuleDetailComponent,
        resolve: {
            sentenceModule: SentenceModuleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceModule.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-module/new',
        component: SentenceModuleUpdateComponent,
        resolve: {
            sentenceModule: SentenceModuleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceModule.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-module/:id/edit',
        component: SentenceModuleUpdateComponent,
        resolve: {
            sentenceModule: SentenceModuleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceModule.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sentenceModulePopupRoute: Routes = [
    {
        path: 'sentence-module/:id/delete',
        component: SentenceModuleDeletePopupComponent,
        resolve: {
            sentenceModule: SentenceModuleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceModule.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
