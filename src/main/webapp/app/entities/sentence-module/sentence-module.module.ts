import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared';
import {
    SentenceModuleComponent,
    SentenceModuleDetailComponent,
    SentenceModuleUpdateComponent,
    SentenceModuleDeletePopupComponent,
    SentenceModuleDeleteDialogComponent,
    sentenceModuleRoute,
    sentenceModulePopupRoute
} from './';

const ENTITY_STATES = [...sentenceModuleRoute, ...sentenceModulePopupRoute];

@NgModule({
    imports: [AppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SentenceModuleComponent,
        SentenceModuleDetailComponent,
        SentenceModuleUpdateComponent,
        SentenceModuleDeleteDialogComponent,
        SentenceModuleDeletePopupComponent
    ],
    entryComponents: [
        SentenceModuleComponent,
        SentenceModuleUpdateComponent,
        SentenceModuleDeleteDialogComponent,
        SentenceModuleDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppSentenceModuleModule {}
