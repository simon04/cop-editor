import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISentenceModule } from 'app/shared/model/sentence-module.model';
import { SentenceModuleService } from './sentence-module.service';

@Component({
    selector: 'jhi-sentence-module-delete-dialog',
    templateUrl: './sentence-module-delete-dialog.component.html'
})
export class SentenceModuleDeleteDialogComponent {
    sentenceModule: ISentenceModule;

    constructor(
        private sentenceModuleService: SentenceModuleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sentenceModuleService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sentenceModuleListModification',
                content: 'Deleted an sentenceModule'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sentence-module-delete-popup',
    template: ''
})
export class SentenceModuleDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sentenceModule }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SentenceModuleDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sentenceModule = sentenceModule;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
