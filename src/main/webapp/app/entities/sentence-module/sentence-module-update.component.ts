import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ISentenceModule } from 'app/shared/model/sentence-module.model';
import { SentenceModuleService } from './sentence-module.service';

@Component({
    selector: 'jhi-sentence-module-update',
    templateUrl: './sentence-module-update.component.html'
})
export class SentenceModuleUpdateComponent implements OnInit {
    private _sentenceModule: ISentenceModule;
    isSaving: boolean;
    datenew: string;
    datelast: string;

    constructor(private sentenceModuleService: SentenceModuleService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sentenceModule }) => {
            this.sentenceModule = sentenceModule;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.sentenceModule.datenew = moment(this.datenew, DATE_TIME_FORMAT);
        this.sentenceModule.datelast = moment(this.datelast, DATE_TIME_FORMAT);
        if (this.sentenceModule.id !== undefined) {
            this.subscribeToSaveResponse(this.sentenceModuleService.update(this.sentenceModule));
        } else {
            this.subscribeToSaveResponse(this.sentenceModuleService.create(this.sentenceModule));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ISentenceModule>>) {
        result.subscribe((res: HttpResponse<ISentenceModule>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get sentenceModule() {
        return this._sentenceModule;
    }

    set sentenceModule(sentenceModule: ISentenceModule) {
        this._sentenceModule = sentenceModule;
        this.datenew = moment(sentenceModule.datenew).format(DATE_TIME_FORMAT);
        this.datelast = moment(sentenceModule.datelast).format(DATE_TIME_FORMAT);
    }
}
