import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhraseOption } from 'app/shared/model/phrase-option.model';

@Component({
    selector: 'jhi-phrase-option-detail',
    templateUrl: './phrase-option-detail.component.html'
})
export class PhraseOptionDetailComponent implements OnInit {
    phraseOption: IPhraseOption;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ phraseOption }) => {
            this.phraseOption = phraseOption;
        });
    }

    previousState() {
        window.history.back();
    }
}
