import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPhraseOption } from 'app/shared/model/phrase-option.model';

type EntityResponseType = HttpResponse<IPhraseOption>;
type EntityArrayResponseType = HttpResponse<IPhraseOption[]>;

@Injectable({ providedIn: 'root' })
export class PhraseOptionService {
    private resourceUrl = SERVER_API_URL + 'api/phrase-options';

    constructor(private http: HttpClient) {}

    create(phraseOption: IPhraseOption): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(phraseOption);
        return this.http
            .post<IPhraseOption>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    update(phraseOption: IPhraseOption): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(phraseOption);
        return this.http
            .put<IPhraseOption>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IPhraseOption>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IPhraseOption[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(phraseOption: IPhraseOption): IPhraseOption {
        const copy: IPhraseOption = Object.assign({}, phraseOption, {
            datenew: phraseOption.datenew != null && phraseOption.datenew.isValid() ? phraseOption.datenew.toJSON() : null,
            datelast: phraseOption.datelast != null && phraseOption.datelast.isValid() ? phraseOption.datelast.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.datenew = res.body.datenew != null ? moment(res.body.datenew) : null;
        res.body.datelast = res.body.datelast != null ? moment(res.body.datelast) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((phraseOption: IPhraseOption) => {
            phraseOption.datenew = phraseOption.datenew != null ? moment(phraseOption.datenew) : null;
            phraseOption.datelast = phraseOption.datelast != null ? moment(phraseOption.datelast) : null;
        });
        return res;
    }
}
