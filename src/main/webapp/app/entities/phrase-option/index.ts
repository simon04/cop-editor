export * from './phrase-option.service';
export * from './phrase-option-update.component';
export * from './phrase-option-delete-dialog.component';
export * from './phrase-option-detail.component';
export * from './phrase-option.component';
export * from './phrase-option.route';
