import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { PhraseOption } from 'app/shared/model/phrase-option.model';
import { PhraseOptionService } from './phrase-option.service';
import { PhraseOptionComponent } from './phrase-option.component';
import { PhraseOptionDetailComponent } from './phrase-option-detail.component';
import { PhraseOptionUpdateComponent } from './phrase-option-update.component';
import { PhraseOptionDeletePopupComponent } from './phrase-option-delete-dialog.component';
import { IPhraseOption } from 'app/shared/model/phrase-option.model';

@Injectable({ providedIn: 'root' })
export class PhraseOptionResolve implements Resolve<IPhraseOption> {
    constructor(private service: PhraseOptionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((phraseOption: HttpResponse<PhraseOption>) => phraseOption.body);
        }
        return Observable.of(new PhraseOption());
    }
}

export const phraseOptionRoute: Routes = [
    {
        path: 'phrase-option',
        component: PhraseOptionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase-option/:id/view',
        component: PhraseOptionDetailComponent,
        resolve: {
            phraseOption: PhraseOptionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase-option/new',
        component: PhraseOptionUpdateComponent,
        resolve: {
            phraseOption: PhraseOptionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'phrase-option/:id/edit',
        component: PhraseOptionUpdateComponent,
        resolve: {
            phraseOption: PhraseOptionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const phraseOptionPopupRoute: Routes = [
    {
        path: 'phrase-option/:id/delete',
        component: PhraseOptionDeletePopupComponent,
        resolve: {
            phraseOption: PhraseOptionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.phraseOption.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
