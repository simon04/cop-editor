import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDomain } from 'app/shared/model/domain.model';

type EntityResponseType = HttpResponse<IDomain>;
type EntityArrayResponseType = HttpResponse<IDomain[]>;

@Injectable({ providedIn: 'root' })
export class DomainService {
    private resourceUrl = SERVER_API_URL + 'api/domains';

    constructor(private http: HttpClient) {}

    create(domain: IDomain): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(domain);
        return this.http
            .post<IDomain>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    update(domain: IDomain): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(domain);
        return this.http
            .put<IDomain>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IDomain>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IDomain[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(domain: IDomain): IDomain {
        const copy: IDomain = Object.assign({}, domain, {
            datenew: domain.datenew != null && domain.datenew.isValid() ? domain.datenew.toJSON() : null,
            datelast: domain.datelast != null && domain.datelast.isValid() ? domain.datelast.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.datenew = res.body.datenew != null ? moment(res.body.datenew) : null;
        res.body.datelast = res.body.datelast != null ? moment(res.body.datelast) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((domain: IDomain) => {
            domain.datenew = domain.datenew != null ? moment(domain.datenew) : null;
            domain.datelast = domain.datelast != null ? moment(domain.datelast) : null;
        });
        return res;
    }
}
