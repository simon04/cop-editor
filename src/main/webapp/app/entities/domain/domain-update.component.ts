import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDomain } from 'app/shared/model/domain.model';
import { DomainService } from './domain.service';

@Component({
    selector: 'jhi-domain-update',
    templateUrl: './domain-update.component.html'
})
export class DomainUpdateComponent implements OnInit {
    private _domain: IDomain;
    isSaving: boolean;
    datenew: string;
    datelast: string;

    constructor(private domainService: DomainService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ domain }) => {
            this.domain = domain;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.domain.datenew = moment(this.datenew, DATE_TIME_FORMAT);
        this.domain.datelast = moment(this.datelast, DATE_TIME_FORMAT);
        if (this.domain.id !== undefined) {
            this.subscribeToSaveResponse(this.domainService.update(this.domain));
        } else {
            this.subscribeToSaveResponse(this.domainService.create(this.domain));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDomain>>) {
        result.subscribe((res: HttpResponse<IDomain>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get domain() {
        return this._domain;
    }

    set domain(domain: IDomain) {
        this._domain = domain;
        this.datenew = moment(domain.datenew).format(DATE_TIME_FORMAT);
        this.datelast = moment(domain.datelast).format(DATE_TIME_FORMAT);
    }
}
