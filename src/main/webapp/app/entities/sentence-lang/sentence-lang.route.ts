import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { SentenceLang } from 'app/shared/model/sentence-lang.model';
import { SentenceLangService } from './sentence-lang.service';
import { SentenceLangComponent } from './sentence-lang.component';
import { SentenceLangDetailComponent } from './sentence-lang-detail.component';
import { SentenceLangUpdateComponent } from './sentence-lang-update.component';
import { SentenceLangDeletePopupComponent } from './sentence-lang-delete-dialog.component';
import { ISentenceLang } from 'app/shared/model/sentence-lang.model';

@Injectable({ providedIn: 'root' })
export class SentenceLangResolve implements Resolve<ISentenceLang> {
    constructor(private service: SentenceLangService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((sentenceLang: HttpResponse<SentenceLang>) => sentenceLang.body);
        }
        return Observable.of(new SentenceLang());
    }
}

export const sentenceLangRoute: Routes = [
    {
        path: 'sentence-lang',
        component: SentenceLangComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceLang.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-lang/:id/view',
        component: SentenceLangDetailComponent,
        resolve: {
            sentenceLang: SentenceLangResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceLang.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-lang/new',
        component: SentenceLangUpdateComponent,
        resolve: {
            sentenceLang: SentenceLangResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceLang.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'sentence-lang/:id/edit',
        component: SentenceLangUpdateComponent,
        resolve: {
            sentenceLang: SentenceLangResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceLang.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sentenceLangPopupRoute: Routes = [
    {
        path: 'sentence-lang/:id/delete',
        component: SentenceLangDeletePopupComponent,
        resolve: {
            sentenceLang: SentenceLangResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appApp.sentenceLang.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
