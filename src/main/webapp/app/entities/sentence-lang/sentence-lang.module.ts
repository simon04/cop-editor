import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppSharedModule } from 'app/shared';
import {
    SentenceLangComponent,
    SentenceLangDetailComponent,
    SentenceLangUpdateComponent,
    SentenceLangDeletePopupComponent,
    SentenceLangDeleteDialogComponent,
    sentenceLangRoute,
    sentenceLangPopupRoute
} from './';

const ENTITY_STATES = [...sentenceLangRoute, ...sentenceLangPopupRoute];

@NgModule({
    imports: [AppSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SentenceLangComponent,
        SentenceLangDetailComponent,
        SentenceLangUpdateComponent,
        SentenceLangDeleteDialogComponent,
        SentenceLangDeletePopupComponent
    ],
    entryComponents: [
        SentenceLangComponent,
        SentenceLangUpdateComponent,
        SentenceLangDeleteDialogComponent,
        SentenceLangDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppSentenceLangModule {}
