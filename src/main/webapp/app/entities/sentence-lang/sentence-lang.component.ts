import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISentenceLang } from 'app/shared/model/sentence-lang.model';
import { Principal } from 'app/core';
import { SentenceLangService } from './sentence-lang.service';

@Component({
    selector: 'jhi-sentence-lang',
    templateUrl: './sentence-lang.component.html'
})
export class SentenceLangComponent implements OnInit, OnDestroy {
    sentenceLangs: ISentenceLang[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private sentenceLangService: SentenceLangService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.sentenceLangService.query().subscribe(
            (res: HttpResponse<ISentenceLang[]>) => {
                this.sentenceLangs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSentenceLangs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISentenceLang) {
        return item.id;
    }

    registerChangeInSentenceLangs() {
        this.eventSubscriber = this.eventManager.subscribe('sentenceLangListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
