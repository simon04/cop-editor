package it.clesius.albina.copedit.repository;

import it.clesius.albina.copedit.domain.PhraseOption;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PhraseOption entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhraseOptionRepository extends JpaRepository<PhraseOption, Long> {

}
