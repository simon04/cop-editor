package it.clesius.albina.copedit.repository.custom;

import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionDTO;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionListItemDTO;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditPhraseOptionMapper;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class CopeditPhraseOptionRepository implements JpaRepository<PhraseOption, Long> {

    @Autowired
    EntityManager em;
    @Autowired
    CopeditPhraseOptionMapper copeditPhraseOptionMapper;


    @Override
    public List<PhraseOption> findAll() {
        Query q = em.createNativeQuery("select phrase_option_id, name from phrase_option WHERE language = 'de' ORDER BY name asc", "PhraseOptionMapper");
        return q.getResultList();
    }

    @Override
    public List<PhraseOption> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<PhraseOption> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<PhraseOption> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    @Transactional
    public void deleteById(Long phraseOptionId) {

        em.createNativeQuery(String.format("UPDATE PHRASE_OPTION SET deleted = 1 WHERE PHRASE_OPTION_ID = %d", phraseOptionId))
            .executeUpdate();
    }

    @Override
    public void delete(PhraseOption PhraseOption) {

    }

    @Override
    public void deleteAll(Iterable<? extends PhraseOption> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends PhraseOption> S save(S s) {
        return null;
    }

    @Override
    public <S extends PhraseOption> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<PhraseOption> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends PhraseOption> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<PhraseOption> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public PhraseOption getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends PhraseOption> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends PhraseOption> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends PhraseOption> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends PhraseOption> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends PhraseOption> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends PhraseOption> boolean exists(Example<S> example) {
        return false;
    }


    //solo quelle a primo livello
//    SELECT phrase_option.phrase_option_id, phrase_option.name, phrase_option.language, phrase_option.domain_id
    //   FROM phrase_option INNER JOIN sentence_module ON phrase_option.phrase_option_id = sentence_module.phrase_option_id
    //   WHERE (((phrase_option.language)="de") AND ((phrase_option.domain_id)=1));


    public List<PhraseOptionDTO> findAllByLanguage(String lang, Integer domain) {
        Query q = em.createNativeQuery("select * FROM phrase_option WHERE language = :language and domain_id = :domain ORDER BY name asc", PhraseOption.class)
            .setParameter("language", lang)
            .setParameter("domain", domain);
        List<PhraseOption> res = q.getResultList();
        List<PhraseOptionDTO> optionDTOS = copeditPhraseOptionMapper.phraseOptionListToDTO(res);
        return optionDTOS;
    }

   /* public List<PhraseOptionComboDTO> findAllByLanguageCombo(String lang) {
        Query q = em.createNativeQuery("select * FROM phrase_option WHERE language = :language ORDER BY name asc", PhraseOption.class)
            .setParameter("language", lang);
        List<PhraseOption> res = q.getResultList();
        List<PhraseOptionDTO> optionDTOS = copeditPhraseOptionMapper.phraseOptionListToDTO(res);
        List<PhraseOptionComboDTO> combos = new ArrayList<>();
        optionDTOS.forEach(x -> {
            PhraseOptionComboDTO comboItem = new PhraseOptionComboDTO(x.getName(), x);
            combos.add(comboItem);
        });
        return combos;
    }*/


    public Page<PhraseOptionListItemDTO> findAll(Pageable pageable, Boolean deleted, String search, Integer domainId, String versionId) {

        StringBuffer query = new StringBuffer();
        query.append("SELECT s.phrase_option_id, s.header, s.name, s.datelast, count(s.phrase_option_id) AS usedInSentence");
        query.append(" FROM (SELECT phrase_option_id AS phrase_option_id, header, name, datelast, row_number() OVER (");
        if (pageable.getSort().isUnsorted()) {
            query.append(" order by header");
        } else {
            query.append(" order by ");
            pageable.getSort().forEach(x -> {
                query.append(x.getProperty() + " " + x.getDirection());
            });
        }
        query.append(") AS rn from PHRASE_OPTION where LANGUAGE = 'de' ");
        if (BooleanUtils.isTrue(deleted)) {
            query.append(" and deleted = '1'");
        } else {
            query.append(" and deleted = '0'");
        }
        if (!StringUtils.isEmpty(search)) {
            query.append(" and UPPER(name) like '%" + search.toUpperCase() + "%'");
        }
        if (!StringUtils.isEmpty(versionId)) {
            query.append(" and version = '" + versionId + "'");
        }
        if (domainId != null) {
            query.append(" and domain_id = " + domainId);
        }
        query.append(" ) s\n" +
            " left join SENTENCE_MODULE  on s.phrase_option_id = SENTENCE_MODULE.PHRASE_OPTION_ID\n" +
            " where rn between :start and :end\n" +
            " GROUP BY s.phrase_option_id, s.header, s.name, s.datelast");

        if (!pageable.getSort().isUnsorted()) {
            query.append(" order by ");
            pageable.getSort().forEach(x -> {
                query.append(x.getProperty() + " " + x.getDirection());
            });
        }

        Query q = em.createNativeQuery(query.toString(), "PhraseOptionListDTOMapper")
            .setParameter("start", pageable.getPageNumber() * pageable.getPageSize())
            .setParameter("end", pageable.getPageNumber() * pageable.getPageSize() + pageable.getPageSize() - 1);
        List<PhraseOptionListItemDTO> sentences = q.getResultList();

        StringBuffer totalRowsQuery = new StringBuffer("select count(*) from PHRASE_OPTION where LANGUAGE = 'de'");
        if (BooleanUtils.isTrue(deleted)) {
            totalRowsQuery.append(" and deleted = '1'");
        } else {
            totalRowsQuery.append(" and deleted = '0'");
        }
        if (!StringUtils.isEmpty(search)) {
            totalRowsQuery.append(" and UPPER(name) like '%" + search.toUpperCase() + "%'");
        }
        if (!StringUtils.isEmpty(versionId)) {
            totalRowsQuery.append(" and version = '" + versionId + "'");
        }
        if (domainId != null) {
            totalRowsQuery.append(" and domain_id = '" + domainId + "'");
        }

        Query totalRows = em.createNativeQuery(totalRowsQuery.toString());
        Integer totRows = ((BigInteger) totalRows.getResultList().get(0)).intValue();
        // return new PageImpl(authors);
        return new Page<PhraseOptionListItemDTO>() {
            @Override
            public int getTotalPages() {
                return totRows / pageable.getPageSize();
            }

            @Override
            public long getTotalElements() {
                return totRows;
            }

            @Override
            public <U> Page<U> map(Function<? super PhraseOptionListItemDTO, ? extends U> function) {
                return null;
            }

            @Override
            public int getNumber() {
                return pageable.getPageNumber();
            }

            @Override
            public int getSize() {
                return sentences.size();
            }

            @Override
            public int getNumberOfElements() {
                return totRows;
            }

            @Override
            public List<PhraseOptionListItemDTO> getContent() {
                return sentences;
            }

            @Override
            public boolean hasContent() {
                return sentences.isEmpty();
            }

            @Override
            public Sort getSort() {
                return pageable.getSort();
            }

            @Override
            public boolean isFirst() {
                return pageable.getPageNumber() == 1;
            }

            @Override
            public boolean isLast() {
                return pageable.getPageNumber() == totRows;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<PhraseOptionListItemDTO> iterator() {
                return sentences.iterator();
            }
        };
    }


    @SqlResultSetMapping(
        name = "PhraseOptionMapper",
        classes = @ConstructorResult(
            targetClass = PhraseOption.class,
            columns = {
                @ColumnResult(name = "phrase_option_id", type = Long.class),
                @ColumnResult(name = "name", type = String.class)
            }))
    @Entity
    class PhraseOptionMapper {
        @Id
        int id;
    }


    @SqlResultSetMapping(
        name = "PhraseOptionListMapper",
        classes = @ConstructorResult(
            targetClass = PhraseOption.class,
            columns = {
                @ColumnResult(name = "phrase_option_id", type = Long.class),
                @ColumnResult(name = "header", type = String.class),
                @ColumnResult(name = "datelast", type = ZonedDateTime.class),
                @ColumnResult(name = "usedInSentence", type = Integer.class)
            }))
    @Entity
    class PhraseOptionListMapper {
        @Id
        int id;
    }

    @SqlResultSetMapping(
        name = "PhraseOptionListDTOMapper",
        classes = @ConstructorResult(
            targetClass = PhraseOptionListItemDTO.class,
            columns = {
                @ColumnResult(name = "phrase_option_id", type = Long.class),
                @ColumnResult(name = "header", type = String.class),
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "datelast", type = ZonedDateTime.class),
                @ColumnResult(name = "usedInSentence", type = Integer.class)
            }))
    @Entity
    class PhraseOptionListDTOMapper {
        @Id
        int id;
    }


}
