package it.clesius.albina.copedit.repository.custom;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.domain.SentenceModule;
import it.clesius.albina.copedit.domain.copedit.*;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditDomainMapper;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditPhraseOptionMapper;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditSentenceMapper;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditSentenceModuleMapper;
import it.clesius.albina.copedit.web.rest.copedit.json.indexes.Item;
import it.clesius.albina.copedit.web.rest.copedit.json.indexes.JsonIndexes;
import it.clesius.albina.copedit.web.rest.copedit.json.sentences.JsonSentence;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.persistence.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static it.clesius.albina.copedit.config.Constants.*;

@Service
public class CopeditSentenceRepository implements JpaRepository<Sentence, Long> {

    @Autowired
    EntityManager em;
    @Autowired
    CopeditSentenceMapper copeditSentenceMapper;
    @Autowired
    CopeditPhraseOptionMapper copeditPhraseOptionMapper;
    @Autowired
    CopeditSentenceModuleMapper copeditSentenceModuleMapper;
    @Autowired
    CopeditDomainRepository copeditDomainRepository;
    @Autowired
    CopeditDomainMapper copeditDomainMapper;
    @Autowired
    ApplicationProperties applicationProperties;

    @Override
    public List<Sentence> findAll() {
        Query q = em.createNativeQuery("select SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE FROM SENTENCE where LANGUAGE = 'de' ", "SentenceListMapper");
        List<Sentence> sentences = q.getResultList();
        return sentences;
    }

    @Override
    public List<Sentence> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Sentence> findAll(Pageable pageable) {

        StringBuffer query = new StringBuffer();
        query.append("select SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE");
        query.append(" FROM (SELECT SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE, row_number() OVER (order by name) rn from SENTENCE where LANGUAGE = 'de') ");
        query.append(" where rn between :start and :end");

        Query q = em.createNativeQuery("select SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE FROM (SELECT SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE, row_number() OVER (order by name) rn from SENTENCE where LANGUAGE = 'de') where rn between :start and :end", "SentenceListMapper")
            .setParameter("start", pageable.getPageNumber() * pageable.getPageSize())
            .setParameter("end", pageable.getPageNumber() * pageable.getPageSize() + pageable.getPageSize() - 1);
        List<Sentence> sentences = q.getResultList();

        Query totalRows = em.createNativeQuery("select count(*) from SENTENCE where LANGUAGE = 'de'");
        Integer totRows = ((BigDecimal) totalRows.getResultList().get(0)).intValue();
        // return new PageImpl(authors);
        return new Page<Sentence>() {
            @Override
            public int getTotalPages() {
                return totRows / pageable.getPageSize();
            }

            @Override
            public long getTotalElements() {
                return totRows;
            }

            @Override
            public <U> Page<U> map(Function<? super Sentence, ? extends U> function) {
                return null;
            }

            @Override
            public int getNumber() {
                return pageable.getPageNumber();
            }

            @Override
            public int getSize() {
                return sentences.size();
            }

            @Override
            public int getNumberOfElements() {
                return totRows;
            }

            @Override
            public List<Sentence> getContent() {
                return sentences;
            }

            @Override
            public boolean hasContent() {
                return sentences.isEmpty();
            }

            @Override
            public Sort getSort() {
                return pageable.getSort();
            }

            @Override
            public boolean isFirst() {
                return pageable.getPageNumber() == 1;
            }

            @Override
            public boolean isLast() {
                return pageable.getPageNumber() == totRows;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Sentence> iterator() {
                return sentences.iterator();
            }
        };
    }

    @Override
    public List<Sentence> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Sentence sentence) {

    }

    @Override
    public void deleteAll(Iterable<? extends Sentence> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Sentence> S save(S s) {
        return null;
    }

    @Override
    public <S extends Sentence> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Sentence> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Sentence> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Sentence> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Sentence getOne(Long aLong) {

        return null;
    }

    @Override
    public <S extends Sentence> Optional<S> findOne(Example<S> example) {

        return Optional.empty();
    }

    @Override
    public <S extends Sentence> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Sentence> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Sentence> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Sentence> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Sentence> boolean exists(Example<S> example) {
        return false;
    }

    public Page<Sentence> findAll(Pageable pageable, Boolean deleted, Boolean joker, String search, Integer phraseId, Integer position, Integer domainId, String versionId) {

        StringBuffer query = new StringBuffer();
        query.append("select SENTENCE_ID, NAME, HEADER, DELETED, JOKER_SENTENCE");
        query.append(" FROM (SELECT SENTENCE.SENTENCE_ID, SENTENCE.NAME, SENTENCE.HEADER, SENTENCE.DELETED, SENTENCE.JOKER_SENTENCE, row_number() OVER (");

        query.append(") as rn from SENTENCE where LANGUAGE = 'de' ");
        if (BooleanUtils.isTrue(deleted)) {
            query.append(" and deleted = '1'");
        } else {
            query.append(" and deleted = '0'");
        }
        if (BooleanUtils.isTrue(joker)) {
            query.append(" and joker_sentence = '1'");
        } else {
            query.append(" and joker_sentence = '0'");
        }
        if (!StringUtils.isEmpty(search)) {
            String domainname =  copeditDomainRepository.getOne(Long.valueOf(domainId)).getName();
            Set<String> ids = searchSentences(search,domainname );
            if (ids.size() > 0) {
                query.append(" and sentence_id in (" + String.join(", ", ids) + ")");
            } else {
                query.append(" and sentence_id in (-1)");
            }
        }
        if (!StringUtils.isEmpty(versionId)) {
            query.append(" and version = '" + versionId + "'");
        }
        if (domainId != null) {
            query.append(" and domain_id = " + domainId);
        }
        if (phraseId != null) {
            query.append(" AND EXISTS (SELECT * FROM SENTENCE_MODULE WHERE SENTENCE_MODULE.SENTENCE_ID = SENTENCE.SENTENCE_ID AND SENTENCE_MODULE.PHRASE_OPTION_ID = " + phraseId + " )");
        }
        if (position != null) {
            query.append(" AND EXISTS (SELECT * FROM SENTENCE_MODULE WHERE SENTENCE_MODULE.SENTENCE_ID = SENTENCE.SENTENCE_ID AND SENTENCE_MODULE.MODULE_NO = " + position + " )");
        }
        query.append(" ) as result where rn between :start and :end");

        if (pageable.getSort().isUnsorted()) {
            query.append(" order by name");
        } else {
            query.append(" order by ");
            pageable.getSort().forEach(x -> {
                query.append(x.getProperty() + " " + x.getDirection());
            });
        }

        Query q = em.createNativeQuery(query.toString(), "SentenceListMapper")
            .setParameter("start", pageable.getPageNumber() * pageable.getPageSize())
            .setParameter("end", pageable.getPageNumber() * pageable.getPageSize() + pageable.getPageSize() - 1);
        List<Sentence> sentences = q.getResultList();

        StringBuffer totalRowsQuery = new StringBuffer("select count(*) from SENTENCE where LANGUAGE = 'de'");
        if (BooleanUtils.isTrue(deleted)) {
            totalRowsQuery.append(" and deleted = '1'");
        } else {
            totalRowsQuery.append(" and deleted = '0'");
        }
        if (BooleanUtils.isTrue(joker)) {
            totalRowsQuery.append(" and joker_sentence = '1'");
        } else {
            totalRowsQuery.append(" and joker_sentence = '0'");
        }
        if (!StringUtils.isEmpty(search)) {
            totalRowsQuery.append(" and UPPER(name) like '%" + search.toUpperCase() + "%'");
        }
        if (!StringUtils.isEmpty(versionId)) {
            totalRowsQuery.append(" and version = '" + versionId + "'");
        }
        if (domainId != null) {
            totalRowsQuery.append(" and domain_id = '" + domainId + "'");
        }
        if (phraseId != null) {
            totalRowsQuery.append(" AND EXISTS (SELECT * FROM SENTENCE_MODULE WHERE SENTENCE_MODULE.SENTENCE_ID = SENTENCE.SENTENCE_ID AND SENTENCE_MODULE.PHRASE_OPTION_ID =" + phraseId + "  )");
        }
        if (position != null) {
            totalRowsQuery.append(" AND EXISTS (SELECT * FROM SENTENCE_MODULE WHERE SENTENCE_MODULE.SENTENCE_ID = SENTENCE.SENTENCE_ID AND SENTENCE_MODULE.MODULE_NO = " + position + "  )");
        }

        Query totalRows = em.createNativeQuery(totalRowsQuery.toString());
        //Integer totRows = ((BigDecimal) totalRows.getResultList().get(0)).intValue();

        Integer totRows = ((BigInteger) totalRows.getResultList().get(0)).intValue();
        return new Page<Sentence>() {
            @Override
            public int getTotalPages() {
                return totRows / pageable.getPageSize();
            }

            @Override
            public long getTotalElements() {
                return totRows;
            }

            @Override
            public <U> Page<U> map(Function<? super Sentence, ? extends U> function) {
                return null;
            }

            @Override
            public int getNumber() {
                return pageable.getPageNumber();
            }

            @Override
            public int getSize() {
                return sentences.size();
            }

            @Override
            public int getNumberOfElements() {
                return totRows;
            }

            @Override
            public List<Sentence> getContent() {
                return sentences;
            }

            @Override
            public boolean hasContent() {
                return sentences.isEmpty();
            }

            @Override
            public Sort getSort() {
                return pageable.getSort();
            }

            @Override
            public boolean isFirst() {
                return pageable.getPageNumber() == 1;
            }

            @Override
            public boolean isLast() {
                return pageable.getPageNumber() == totRows;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Sentence> iterator() {
                return sentences.iterator();
            }
        };
    }

    public SentenceDTO findSentencesByheader(String name, Integer domainId) {
        Query q = em.createNativeQuery("SELECT * FROM SENTENCE WHERE NAME = :name AND DOMAIN_ID = :domainId", Sentence.class)
            .setParameter("name", name)
            .setParameter("domainId", domainId);
        List<Sentence> sentences = q.getResultList();
        /* get phrase options in order by module_nr*/
        Map<String, List<PhraseOptionDTO>> phraseOptionsMap = new HashMap<>();
        Map<Long, Integer> moduleNoMap = new HashMap<>();
        for (Sentence s : sentences) {
            Query phraseOptionsQuery = em.createNativeQuery("SELECT po.* FROM PHRASE_OPTION po inner join SENTENCE_MODULE sm  on sm.PHRASE_OPTION_ID = po.PHRASE_OPTION_ID\n" +
                " where sm.SENTENCE_ID = :sentenceId\n " +
                " ORDER BY sm.MODULE_NO", PhraseOption.class)
                .setParameter("sentenceId", s.getId());
            List<PhraseOption> phraseOptions = phraseOptionsQuery.getResultList();
            /* for each sentence get module no */
            for (PhraseOption phraseOption : phraseOptions) {
                Query poQuery = em.createNativeQuery("SELECT sm.* FROM SENTENCE_MODULE sm " +
                    " where sm.SENTENCE_ID = :sentenceId " +
                    " and sm.phrase_option_id = :phraseOptionId ", SentenceModule.class)
                    .setParameter("sentenceId", s.getId())
                    .setParameter("phraseOptionId", phraseOption.getId());
                SentenceModule moduleNo = (SentenceModule) poQuery.getSingleResult();
                moduleNoMap.put(phraseOption.getId(), moduleNo.getModuleNo());
            }

            List<PhraseOptionDTO> phraseOptionsDTO = copeditPhraseOptionMapper.phraseOptionListToDTO(phraseOptions, moduleNoMap);
            phraseOptionsMap.put(s.getLanguage(), phraseOptionsDTO);
        }
        SentenceDTO s = copeditSentenceMapper.sentenceDTOMapper(sentences, phraseOptionsMap);
        return s;
    }

    @Transactional
    /***
     *Updates an existing sentence to database and return updated sentence
     * @param sentence sentence to update
     * @return updated sentence
     */
    public SentenceDTO updateSentence(SentenceDTO sentence) {
        List<Sentence> sentences = copeditSentenceMapper.sentenceDTOToSentence(sentence);
        List<SentenceModule> sentenceModuleDE = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesDE());
        List<SentenceModule> sentenceModuleIT = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesIT());
        List<SentenceModule> sentenceModuleFR = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesFR());
        List<SentenceModule> sentenceModuleEN = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesEN());

        sentences.forEach(x -> {
            x.datelast(ZonedDateTime.now());
            em.merge(x);
        });

        sentenceModuleDE.forEach(x ->
            em.merge(x));
        sentenceModuleIT.forEach(x ->
            em.merge(x));
        sentenceModuleFR.forEach(x ->
            em.merge(x));
        sentenceModuleEN.forEach(x ->
            em.merge(x));

        /* save sentence and then sentence_module */
        SentenceDTO savedSentence = findSentencesByheader(sentence.getSentencesDE().getName(), sentence.getDomain().getId().intValue());

        return savedSentence;
    }

    @Transactional
    /**
     * Saves a new sentence to database and returns the newly created sentence
     * @param sentence sentence to save
     * @return created sentence
     */
    public SentenceDTO createSentence(SentenceDTO sentence) {
        List<Sentence> sentences = copeditSentenceMapper.sentenceDTOToSentence(sentence);
        List<SentenceModule> sentenceModuleDE = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesDE());
        List<SentenceModule> sentenceModuleIT = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesIT());
        List<SentenceModule> sentenceModuleFR = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesFR());
        List<SentenceModule> sentenceModuleEN = copeditSentenceModuleMapper.sentenceLanguageDTOToSentenceModule(sentence.getSentencesEN());

        /* sets creation date to now to all sentences */
        sentences.forEach(x -> {
            x.datenew(ZonedDateTime.now()).datelast(ZonedDateTime.now());
            em.persist(x);
        });

        Long idSentenceDE = sentences.stream().filter(x -> x.getLanguage().equals(LANGUAGE_DE)).findFirst().map(Sentence::getId).orElse(null);
        Long idSentenceIT = sentences.stream().filter(x -> x.getLanguage().equals(LANGUAGE_IT)).findFirst().map(Sentence::getId).orElse(null);
        Long idSentenceFR = sentences.stream().filter(x -> x.getLanguage().equals(LANGUAGE_FR)).findFirst().map(Sentence::getId).orElse(null);
        Long idSentenceEN = sentences.stream().filter(x -> x.getLanguage().equals(LANGUAGE_EN)).findFirst().map(Sentence::getId).orElse(null);


        sentenceModuleDE.forEach(x -> {
            x.setSentenceId(idSentenceDE.intValue());
            em.persist(x);
        });
        sentenceModuleIT.forEach(x -> {
            x.setSentenceId(idSentenceIT.intValue());
            em.persist(x);
        });
        sentenceModuleFR.forEach(x -> {
            x.setSentenceId(idSentenceFR.intValue());
            em.persist(x);
        });
        sentenceModuleEN.forEach(x -> {
            x.setSentenceId(idSentenceEN.intValue());
            em.persist(x);
        });

        /* save sentence and then sentence_module */
        SentenceDTO savedSentence = findSentencesByheader(sentence.getSentencesDE().getName(), sentence.getDomain().getId().intValue());

        return savedSentence;
    }

    /**
     * Creates an empty new sentence ready for creation
     *
     * @param domainId domain to assign to sentence
     * @return new sentence
     */
    public SentenceDTO getNewSentence(Integer domainId) {

        DomainDTO domain = copeditDomainRepository.getOne(Long.valueOf(domainId));
        //DomainDTO domainDTO = copeditDomainMapper.domainToDomainDTO(domain);

        return new SentenceDTO()
            .deleted(false)
            .jokerSentence(false)
            .domain(domain)
            .version(new VersionDTO().version("1.0").baseVersion("1.0"))
            .sentencesDE(new SentenceLanguage()
                .phraseOptions(new ArrayList<>())
                .language(LANGUAGE_DE)
                .structure("")
            )
            .sentencesIT(new SentenceLanguage()
                .phraseOptions(new ArrayList<>())
                .language(LANGUAGE_IT)
                .structure("")
            )
            .sentencesEN(new SentenceLanguage()
                .phraseOptions(new ArrayList<>())
                .language(LANGUAGE_EN)
                .structure("")
            )
            .sentencesFR(new SentenceLanguage()
                .phraseOptions(new ArrayList<>())
                .language(LANGUAGE_FR)
                .structure("")
            );
    }

    @SqlResultSetMapping(
        name = "SentenceListMapper",
        classes = @ConstructorResult(
            targetClass = Sentence.class,
            columns = {
                @ColumnResult(name = "sentence_id", type = Long.class),
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "header", type = String.class),
                @ColumnResult(name = "deleted", type = Boolean.class),
                @ColumnResult(name = "joker_sentence", type = Boolean.class)
            }))
    @Entity
    class SentenceListMapper {
        @Id
        int id;
    }

    @SqlResultSetMapping(
        name = "SentenceMapper",
        classes = @ConstructorResult(
            targetClass = Sentence.class,
            columns = {
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "header", type = String.class),
                @ColumnResult(name = "deleted", type = Boolean.class),
                @ColumnResult(name = "joker_sentence", type = Boolean.class)
            }))
    @Entity
    class SentenceMapper {
        @Id
        int id;
    }

    @Transactional
    /**
     * Logically deletes a sentence setting the delete flag
     */
    public void deleteSentence(String name) {

        em.createNativeQuery(String.format("UPDATE SENTENCE SET deleted = true WHERE name = '%s'", name))
            .executeUpdate();

    }


    /**
     * Returns the sentences id containing the search string passed as parameter
     *
     * @param search search string to look for
     * @return the list of the sentences containing the search term
     */
    public Set<String> searchSentences(String search,String domainName) {

        Set<String> result = new HashSet<>();
        RestTemplate rt = new RestTemplate();
        ObjectMapper om = new ObjectMapper();

        try {
            /* get indexes JSON */
            String indexUrl=applicationProperties.getTextcatApi().gettextcatAPIurl() + applicationProperties.getTextcatApi().getIndexesUrl() + "&dn=" + domainName;
            String indexesStr = rt.getForEntity(indexUrl, String.class).getBody();
            JsonIndexes indexes = om.readValue(indexesStr, JsonIndexes.class);
            /* get sentences JSON */
            String sentencesUrl=applicationProperties.getTextcatApi().gettextcatAPIurl() + applicationProperties.getTextcatApi().getSentencesUrl() + "&dn=" + domainName;
            String sentencesStr = rt.getForEntity(sentencesUrl, String.class).getBody();
            JsonSentence sentences = om.readValue(sentencesStr, JsonSentence.class);

            Set<Integer> tmp = new HashSet<>();
            List<String> words = Arrays.asList(search.split(" "));
            /* for each word, search case insensitively in indexes array and put in tmp array*/
            for (String word : words) {
                if (word.length() == 0) continue;
                /* return only the items that match */
                List<String> sentencesId = indexes.getItems().stream().filter(x -> {

                    Pattern mypattern = Pattern.compile(".*" + word +".*", Pattern.CASE_INSENSITIVE);
                    Matcher mymatcher = mypattern.matcher(x.getWord());
                    return mymatcher.matches();
                })
                    .map(Item::getSentences)
                    .collect(Collectors.toList());
                /* extract id without duplicates */
                for (String s : sentencesId) {
                    Arrays.asList(s.split(";")).forEach(x -> tmp.add(Integer.parseInt(x)));
                }

            }
            /* get the corresponding dbid from sentences based on the id in temp array */
            result = sentences.getItems().stream().filter(x -> tmp.stream().anyMatch(y -> y.intValue() == x.getId().intValue()))
                .map(x -> x.getDbid())
                .map(x -> String.valueOf(x))
                .collect(Collectors.toSet());
            return result;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
