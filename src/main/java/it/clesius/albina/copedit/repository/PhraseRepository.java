package it.clesius.albina.copedit.repository;

import it.clesius.albina.copedit.domain.Phrase;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Phrase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhraseRepository extends JpaRepository<Phrase, Long> {

}
