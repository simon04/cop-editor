package it.clesius.albina.copedit.repository.custom;

import it.clesius.albina.copedit.domain.copedit.DomainDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Service
public class CopeditDomainRepository implements JpaRepository<DomainDTO, Long> {

    @Autowired
    EntityManager em;

    @Override
    public List<DomainDTO> findAll() {
        Query q = em.createNativeQuery("SELECT domain_id, name FROM Domain where domain_id > 0", "DomainMapper");
        List<DomainDTO> domains = q.getResultList();
        return domains;
    }

    @Override
    public List<DomainDTO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<DomainDTO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<DomainDTO> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DomainDTO domain) {

    }

    @Override
    public void deleteAll(Iterable<? extends DomainDTO> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DomainDTO> S save(S s) {
        return null;
    }

    @Override
    public <S extends DomainDTO> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DomainDTO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DomainDTO> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DomainDTO> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DomainDTO getOne(Long aLong) {
        Query q = em.createNativeQuery("SELECT domain_id, name FROM Domain where domain_id = :domain", "DomainMapper")
            .setParameter("domain", aLong);
        DomainDTO domain = (DomainDTO) q.getSingleResult();
        return domain;
    }

    @Override
    public <S extends DomainDTO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DomainDTO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DomainDTO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends DomainDTO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DomainDTO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DomainDTO> boolean exists(Example<S> example) {
        return false;
    }

    @SqlResultSetMapping(
        name = "DomainMapper",
        classes = @ConstructorResult(
            targetClass = DomainDTO.class,
            columns = {
                @ColumnResult(name = "domain_id", type = Long.class),
                @ColumnResult(name = "name")/*,
                @ColumnResult(name = "dateNew", type = ZonedDateTime.class),
                @ColumnResult(name = "dateLast", type = ZonedDateTime.class)*/
            }))
    @Entity
    class DomainMapper {
        @Id
        int id;
    }
}
