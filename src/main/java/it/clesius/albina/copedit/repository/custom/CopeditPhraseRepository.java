package it.clesius.albina.copedit.repository.custom;

import it.clesius.albina.copedit.config.Constants;
import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.PhraseOptionItem;
import it.clesius.albina.copedit.domain.copedit.*;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditCommonMapper;
import it.clesius.albina.copedit.domain.copedit.mappers.CopeditPhraseMapper;
import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Consumer;
import java.math.BigInteger;
import java.util.stream.Collectors;

import static it.clesius.albina.copedit.config.Constants.*;

@Service
public class CopeditPhraseRepository {

    @Autowired
    EntityManager em;
    @Autowired
    CopeditPhraseMapper copeditPhraseMapper;
    @Autowired
    CopeditDomainRepository copeditDomainRepository;
    @Autowired
    CopeditCommonMapper copeditCommonMapper;


    @Transactional
    public PhraseOptionEditDTO findPhraseByNameOLD(String name) {

        List<PhraseOption> phraseOptions = em.createNativeQuery("SELECT * FROM PHRASE_OPTION WHERE NAME = :name", PhraseOption.class)
            .setParameter("name", name)
            .getResultList();

        Map<String, List<Phrase>> phrasesMap = new HashMap<>();
        Map<String, List<PhraseOptionItem>> phrasesOptionsMap = new HashMap<>();
        Map<Long, Integer> phraseItemNoMap = new HashMap<>();

        /* for each phrase option, get list of phrases */
        for (PhraseOption phraseOption : phraseOptions) {

            List<Phrase> phrases = em.createNativeQuery("select ph.*\n" +
                "from PHRASE_OPTION\n" +
                "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
                "inner join PHRASE ph on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = ph.PHRASE_OPTION_ITEM_ID\n" +
                "where PHRASE_OPTION.NAME = :name AND LANGUAGE = :lang\n" +
                "ORDER BY PHRASE_OPTION_item.ITEM_NO", Phrase.class)
                .setParameter("name", name)
                .setParameter("lang", phraseOption.getLanguage())
                .getResultList();

            phrasesMap.put(phraseOption.getLanguage(), phrases);
            /* get the item_no from phrase_option_item */
            List<Object[]> itemNoList = em.createNativeQuery("select PHRASE.PHRASE_ID, PHRASE_OPTION_ITEM.ITEM_NO, PHRASE.ITEM_PART_NO\n" +
                "from PHRASE_OPTION\n" +
                "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
                "inner join PHRASE  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = PHRASE.PHRASE_OPTION_ITEM_ID\n" +
                "where phrase_option.NAME = :name AND LANGUAGE = :lang\n" +
                "ORDER BY LANGUAGE, item_no")
                .setParameter("name", name)
                .setParameter("lang", phraseOption.getLanguage())
                .getResultList();

            for (Object[] record : itemNoList) {

                Long idPhrase = ((BigInteger) record[0]).longValue();
                Integer itemNo = ((BigInteger) record[1]).intValue();
                Integer itemPartNo = ((Short) record[2]).intValue();
                phraseItemNoMap.put(idPhrase, itemNo);
            }

            List<PhraseOptionItem> optionItems = em.createNativeQuery("select PHRASE_OPTION_ITEM.*\n" +
                "from PHRASE_OPTION\n" +
                "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
                "inner join PHRASE  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = PHRASE.PHRASE_OPTION_ITEM_ID\n" +
                "where phrase_option.NAME = :name AND LANGUAGE = :lang\n" +
                "ORDER BY LANGUAGE, item_no", PhraseOptionItem.class)
                .setParameter("name", name)
                .setParameter("lang", phraseOption.getLanguage())
                .getResultList();

            phrasesOptionsMap.put(phraseOption.getLanguage(), optionItems);
        }

        return copeditPhraseMapper.phraseToPhraseOptionEditDTO(phraseOptions, phrasesMap, phraseItemNoMap, phrasesOptionsMap);
    }

    @Transactional
    public PhraseOptionEditDTO findPhraseByName(String name, Integer domainId) {

        List<PhraseOption> phraseOptions = em.createNativeQuery("SELECT * FROM PHRASE_OPTION WHERE NAME = :name AND DOMAIN_ID = :domainId", PhraseOption.class)
            .setParameter("name", name)
            .setParameter("domainId", domainId)
            .getResultList();

        PhraseOption phraseOptionDE = phraseOptions.stream().filter(x -> x.getLanguage().equals(Constants.LANGUAGE_DE)).findFirst().get();
        PhraseOption phraseOptionIT = phraseOptions.stream().filter(x -> x.getLanguage().equals(Constants.LANGUAGE_IT)).findFirst().get();
        PhraseOption phraseOptionFR = phraseOptions.stream().filter(x -> x.getLanguage().equals(Constants.LANGUAGE_FR)).findFirst().get();
        PhraseOption phraseOptionEN = phraseOptions.stream().filter(x -> x.getLanguage().equals(Constants.LANGUAGE_EN)).findFirst().get();

        PhraseOptionEditDTO res = new PhraseOptionEditDTO();
        VersionDTO version = new VersionDTO().baseVersion(phraseOptionDE.getBasisVersion()).version(phraseOptionDE.getVersion());
        res.version(version);
        DomainDTO domain = new DomainDTO().id(phraseOptionDE.getDomainId().longValue()).name(copeditCommonMapper.getDomainNameById(phraseOptions.get(0).getDomainId()));
        res.domain(domain);

        /* for each phrase option, get list of phrases */
        PhraseLanguageDTO phraseLanguageDE = phrasesByLang(phraseOptionDE);
        PhraseLanguageDTO phraseLanguageIT = phrasesByLang(phraseOptionIT);
        PhraseLanguageDTO phraseLanguageEN = phrasesByLang(phraseOptionEN);
        PhraseLanguageDTO phraseLanguageFR = phrasesByLang(phraseOptionFR);

        /*
        SELECT     sentence_module.sentence_id, sentence.name AS sentenceName
        FROM         sentence_module INNER JOIN
                      sentence ON sentence_module.sentence_id = sentence.sentence_id INNER JOIN
                          (SELECT     phrase_option_id
                            FROM          phrase_option AS phrase_option_1
                            WHERE      (name = 'aber_und') AND (domain_id = 1) AND (language = 'de')
                            UNION
                            SELECT     phrase_option_item.phrase_option_id
                            FROM         phrase INNER JOIN
                                                  phrase_option_item ON phrase.phrase_option_item_id = phrase_option_item.phrase_option_item_id INNER JOIN
                                                  phrase_option ON phrase_option_item.phrase_option_id = phrase_option.phrase_option_id
                            WHERE     (phrase.value LIKE N'%{aber_und}%') AND (phrase_option.domain_id = 1) AND (phrase_option.language = N'de')
                            GROUP BY phrase_option_item.phrase_option_id) AS po ON sentence_module.phrase_option_id = po.phrase_option_id


       List<Tuple> result = em.createNativeQuery("SELECT sentence_module.sentence_id, sentence.name AS sentenceName" +
            " FROM phrase_option INNER JOIN" +
            " sentence_module ON phrase_option.phrase_option_id = sentence_module.phrase_option_id INNER JOIN" +
            " sentence ON sentence_module.sentence_id = sentence.sentence_id" +
            " WHERE phrase_option.name  = :name AND phrase_option.domain_id = :domainId AND (phrase_option.language = 'de')", Tuple.class)
            .setParameter("name", name)
            .setParameter("domainId", domainId)
            .getResultList();




            get value of parent phrase

            SELECT     phrase.phrase_id, phrase.value, phrase.phrase_option_item_id, phrase_option_item_1.phrase_option_id, phrase_option_1.name
FROM         phrase_option INNER JOIN
                      phrase_option_item ON phrase_option.phrase_option_id = phrase_option_item.phrase_option_id INNER JOIN
                      phrase ON phrase_option_item.phrase_option_item_id = phrase.phrase_option_item_id INNER JOIN
                      phrase_option_item AS phrase_option_item_1 ON phrase.phrase_option_item_id = phrase_option_item_1.phrase_option_item_id INNER JOIN
                      phrase_option AS phrase_option_1 ON phrase_option_item_1.phrase_option_id = phrase_option_1.phrase_option_id
WHERE     (phrase.value LIKE N'%{am_Ostermontag}%') AND (phrase_option.domain_id = 1) AND (phrase_option.language = N'de')
GROUP BY phrase.value, phrase.phrase_id, phrase.phrase_option_item_id, phrase_option_item_1.phrase_option_id, phrase_option_1.name




  */

// Get list of all phraseOption
        List<Integer> phraseOptionList = new ArrayList<>();

        // 1
       String sqlCommand= "SELECT phrase_option_id FROM phrase_option " +
        " WHERE phrase_option.name  = :name AND phrase_option.domain_id = :domainId AND (phrase_option.language = 'de') " ;

        phraseOptionList=em.createNativeQuery(sqlCommand)
            .setParameter("name", name)
            .setParameter("domainId", domainId)
            .getResultList();

        sqlCommand = " SELECT phrase_option_item.phrase_option_id " +
            " FROM  phrase INNER JOIN " +
            " phrase_option_item ON phrase.phrase_option_item_id = phrase_option_item.phrase_option_item_id INNER JOIN " +
            " phrase_option ON phrase_option_item.phrase_option_id = phrase_option.phrase_option_id " +
            " WHERE (phrase.value LIKE :nameValue) AND (phrase_option.domain_id = :domainId ) AND (phrase_option.language = 'de') " +
            " GROUP BY phrase_option_item.phrase_option_id ";

        phraseOptionList.addAll(em.createNativeQuery(sqlCommand)
            .setParameter("nameValue", "%{" + name + "}%")
            .setParameter("domainId", domainId)
            .getResultList());

        // Iterating parent

        List<String> phraseParentNameList = new ArrayList<>();
        sqlCommand = "SELECT phrase_option.name " +
            " FROM phrase_option_item INNER JOIN " +
            " phrase ON phrase_option_item.phrase_option_item_id = phrase.phrase_option_item_id INNER JOIN " +
            " phrase_option ON phrase_option_item.phrase_option_id = phrase_option.phrase_option_id " +
            " WHERE (phrase.value LIKE :nameValue) AND (phrase_option.domain_id = :domainId ) AND (phrase_option.language = 'de') " ;

        phraseParentNameList = em.createNativeQuery(sqlCommand)
            .setParameter("nameValue", "%{" + name + "}%")
            .setParameter("domainId", domainId)
            .getResultList();

        Iterator<String> parentName = phraseParentNameList.iterator();
        while (parentName.hasNext()) {
            String poname = parentName.next();

            sqlCommand= "SELECT phrase_option_id FROM phrase_option " +
                " WHERE phrase_option.name  = :name AND phrase_option.domain_id = :domainId AND (phrase_option.language = 'de') " ;

            phraseOptionList.addAll(em.createNativeQuery(sqlCommand)
                .setParameter("name", poname)
                .setParameter("domainId", domainId)
                .getResultList());

            sqlCommand = " SELECT phrase_option_item.phrase_option_id " +
                " FROM  phrase INNER JOIN " +
                " phrase_option_item ON phrase.phrase_option_item_id = phrase_option_item.phrase_option_item_id INNER JOIN " +
                " phrase_option ON phrase_option_item.phrase_option_id = phrase_option.phrase_option_id " +
                " WHERE (phrase.value LIKE :nameValue) AND (phrase_option.domain_id = :domainId ) AND (phrase_option.language = 'de') " +
                " GROUP BY phrase_option_item.phrase_option_id ";

            phraseOptionList.addAll(em.createNativeQuery(sqlCommand)
                .setParameter("nameValue", "%{" + poname  + "}%")
                .setParameter("domainId", domainId)
                .getResultList());
        }

        phraseOptionList= (ArrayList) phraseOptionList.stream().distinct().collect(Collectors.toList());


            sqlCommand="SELECT sentence_module.sentence_id, sentence.name AS sentenceName" +
                        " FROM sentence_module INNER JOIN" +
            " sentence ON sentence_module.sentence_id = sentence.sentence_id " +
            " WHERE (sentence_module.phrase_option_id IN (:polist))";


        List<Tuple> result = em.createNativeQuery(sqlCommand, Tuple.class)
            .setParameter("polist", phraseOptionList)
            .getResultList();

        List<SentenceLanguage> sentenceList = new ArrayList<>();
        Iterator<Tuple> iter = result .iterator();
        while (iter.hasNext()) {
            Tuple record = iter.next();
            String sname= record.get("sentenceName", String.class);
            BigInteger sid= record.get("sentence_id", BigInteger.class);
            SentenceLanguage s = new SentenceLanguage();
            s.setId(sid.longValue());
            s.setName(sname);
            sentenceList.add(s);
        }
        res.setSentenceList(sentenceList);
        res.phrasesDE(phraseLanguageDE)
            .phrasesIT(phraseLanguageIT)
            .phrasesEN(phraseLanguageEN)
            .phrasesFR(phraseLanguageFR);

        return res;

    }

    public PhraseLanguageDTO phrasesByLang(PhraseOption po) {

        PhraseLanguageDTO res = new PhraseLanguageDTO();
        res.phrases(new ArrayList<>());
        res.phrasesItemPartNo2(new ArrayList<>());
        res.language(po.getLanguage())
            .header(po.getHeader())
            .name(po.getName())
            .id(po.getId())
            .remark(po.getRemark())
            .datelast(po.getDatelast())
            .datenew(po.getDatenew());

     /*   List<Phrase> phrases = em.createNativeQuery("select ph.*\n" +
            "from PHRASE_OPTION\n" +
            "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
            "inner join PHRASE ph on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = ph.PHRASE_OPTION_ITEM_ID\n" +
            "where PHRASE_OPTION.NAME = :name AND LANGUAGE = :lang AND PHRASE_OPTION.DOMAIN_ID = :domain\n" +
            "ORDER BY PHRASE_OPTION_item.ITEM_NO", Phrase.class)
            .setParameter("name", po.getName())
            .setParameter("lang", po.getLanguage())
            .setParameter("domain", po.getDomainId())
            .getResultList();

        List<PhraseOptionItem> phraseOptionItem = em.createNativeQuery("select distinct PHRASE_OPTION_ITEM.*\n" +
            "from PHRASE_OPTION\n" +
            "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
            "inner join PHRASE ph on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = ph.PHRASE_OPTION_ITEM_ID\n" +
            "where PHRASE_OPTION.NAME = :name AND LANGUAGE = :lang AND PHRASE_OPTION.DOMAIN_ID = :domain\n" +
            "ORDER BY PHRASE_OPTION_item.ITEM_NO", PhraseOptionItem.class)
            .setParameter("name", po.getName())
            .setParameter("lang", po.getLanguage())
            .setParameter("domain", po.getDomainId())
            .getResultList();

        List<PhraseOption> phraseOptions = em.createNativeQuery("select PHRASE_OPTION.*\n" +
            "from PHRASE_OPTION\n" +
            "inner join PHRASE_OPTION_ITEM  on PHRASE_OPTION_ITEM.PHRASE_OPTION_ID = PHRASE_OPTION.PHRASE_OPTION_ID\n" +
            "inner join PHRASE ph on PHRASE_OPTION_ITEM.PHRASE_OPTION_ITEM_ID = ph.PHRASE_OPTION_ITEM_ID\n" +
            "where PHRASE_OPTION.NAME = :name AND LANGUAGE = :lang  AND PHRASE_OPTION.DOMAIN_ID = :domain\n" +
            "ORDER BY PHRASE_OPTION_item.ITEM_NO", PhraseOption.class)
            .setParameter("name", po.getName())
            .setParameter("lang", po.getLanguage())
            .setParameter("domain", po.getDomainId())
            .getResultList();

       phraseOptionItem.forEach(x -> {
            PhraseOptionItemDTO item = new PhraseOptionItemDTO()
                .itemNo(x.getItemNo())
                .phraseOptionItemId(x.getPhraseOptionItemId())
                .phraseOptionId(x.getPhraseOptionId())
                .basisVersion(x.getBasisVersion())
                .version(x.getVersion())
                .deleted(x.getDeleted())
                .datelast(x.getDatelast())
                .datenew(x.getDatenew())
                .phrase(new PhraseDTO());

            *//* set phrase *//*
            Long phraseOpttionItemId = phraseOptionItem.stream().filter(y -> y.getPhraseOptionItemId().intValue() == y.getPhraseOptionItemId().intValue()
                && y.getItemNo().intValue() == x.getItemNo().intValue())
                .findFirst().map(y -> y.getPhraseOptionItemId()).get();

            Set<Long> phraseOptionItemIdList = phraseOptionItem.stream().filter(y -> y.getPhraseOptionItemId().intValue() == y.getPhraseOptionItemId().intValue()
                && y.getItemNo().intValue() == x.getItemNo().intValue())
                .map(y -> y.getPhraseOptionItemId())
                .collect(Collectors.toSet());

            phraseOptionItemIdList.forEach(a -> {
                List<Phrase>
                    phrasesList = phrases.stream().filter(y -> y.getPhraseOptionItemId().intValue() == a.intValue()).collect(Collectors.toList());
                for (Phrase p : phrasesList) {

                    PhraseDTO dto = new PhraseDTO().id(p.getId())
                        .value(p.getValue())
                        .datelast(p.getDatelast())
                        .datenew(p.getDatenew())
                        .spaceAfter(p.isSpaceAfter())
                        .spaceBefore(p.isSpaceBefore())
                        .removePunctuationBefore(p.isRemovePunctuationBefore())
                        .incorrect(p.isIncorrect())
                        .itemPartNo(p.getItemPartNo())
                        .itemNo(x.getItemNo());

                    if (p.getItemPartNo().intValue() == 1) {

                        res.getPhrases().add(new PhraseOptionItemDTO()
                            .itemNo(x.getItemNo())
                            .deleted(x.getDeleted())
                            .phraseOptionItemId(phraseOpttionItemId)
                            .phraseOptionId(x.getPhraseOptionId())
                            .basisVersion(x.getBasisVersion())
                            .version(x.getVersion())
                            .phrase(dto));
                    } else if (p.getItemPartNo().intValue() == 2) {
                        res.getPhrasesItemPartNo2().add(new PhraseOptionItemDTO()
                            .itemNo(x.getItemNo())
                            .deleted(x.getDeleted())
                            .phraseOptionId(x.getPhraseOptionId())
                            .phraseOptionItemId(phraseOpttionItemId)
                            .basisVersion(x.getBasisVersion())
                            .version(x.getVersion())
                            .phrase(dto));
                    }
                }
            });


        });*/

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT PHRASE.PHRASE_ID, ");
        sb.append("PHRASE.VALUE, ");
        sb.append("PHRASE.SPACE_BEFORE, ");
        sb.append("PHRASE.SPACE_AFTER, ");
        sb.append("PHRASE.REMOVE_PUNCTUATION_BEFORE, ");
        sb.append("PHRASE.ITEM_PART_NO, ");
        sb.append("PHRASE_OPTION_item.ITEM_NO, ");
        sb.append("PHRASE.INCORRECT, ");
        sb.append("PHRASE.DATENEW, ");
        sb.append("PHRASE.DATELAST, ");
        sb.append("PHRASE_OPTION.PHRASE_OPTION_ID, ");
        sb.append("PHRASE_OPTION.NAME, ");
        sb.append("PHRASE_OPTION.LANGUAGE, ");
        sb.append("PHRASE_OPTION.DELETED, ");
        sb.append("PHRASE_OPTION.VERSION, ");
        sb.append("PHRASE_OPTION.BASIS_VERSION, ");
        sb.append("PHRASE_OPTION_item.PHRASE_OPTION_ITEM_ID ");
        sb.append("FROM PHRASE_OPTION ");
        sb.append("INNER JOIN PHRASE_OPTION_item ON PHRASE_OPTION.PHRASE_OPTION_ID = PHRASE_OPTION_item.PHRASE_OPTION_ID ");
        sb.append("INNER JOIN PHRASE ON PHRASE_OPTION_item.PHRASE_OPTION_ITEM_ID = PHRASE.PHRASE_OPTION_ITEM_ID ");
        sb.append(String.format("WHERE PHRASE_OPTION.NAME = '%s' ", po.getName()));
        sb.append(String.format("AND PHRASE_OPTION.language = '%s' ", po.getLanguage()));
        sb.append(String.format("AND PHRASE_OPTION.DOMAIN_ID = '%d' ", po.getDomainId()));
        sb.append("ORDER BY PHRASE_OPTION_item.ITEM_NO");

        List<Tuple> queryResult = em.createNativeQuery(sb.toString(), Tuple.class).getResultList();
        queryResult.forEach(record -> {

            PhraseDTO dto = new PhraseDTO()
                .id(((BigInteger) record.get("PHRASE_ID")).longValue())
                .value((String) record.get("VALUE"))
                .datelast(ZonedDateTime.from(record.get("DATELAST", Timestamp.class).toInstant().atZone(ZoneId.systemDefault())))
                .datenew(ZonedDateTime.from(record.get("DATENEW", Timestamp.class).toInstant().atZone(ZoneId.systemDefault())))
                .spaceAfter((Boolean) record.get("SPACE_AFTER"))
                .spaceBefore((Boolean) record.get("SPACE_BEFORE"))
                .removePunctuationBefore((Boolean) record.get("REMOVE_PUNCTUATION_BEFORE"))
                .incorrect((Boolean) record.get("INCORRECT"))
                .itemPartNo(record.get("ITEM_PART_NO", Short.class).intValue())
                .itemNo(record.get("ITEM_NO", BigInteger.class).intValue());

            Integer itemPartNo = record.get("ITEM_PART_NO", Short.class).intValue();
            if (itemPartNo == 1) {

                res.getPhrases().add(new PhraseOptionItemDTO()
                    .itemNo(record.get("ITEM_NO", BigInteger.class).intValue())
                    .deleted(record.get("DELETED", Boolean.class))
                    .phraseOptionId(record.get("PHRASE_OPTION_ID", BigInteger.class).longValue())
                    .phraseOptionItemId(record.get("PHRASE_OPTION_ITEM_ID", BigInteger.class).longValue())
                    .basisVersion(record.get("VERSION", String.class))
                    .version(record.get("BASIS_VERSION", String.class))
                    .phrase(dto));
            } else if (itemPartNo == 2) {
                /* if itemPartNo == 2 is an exception, put it in the exceptions array*/
                res.getPhrasesItemPartNo2().add(new PhraseOptionItemDTO()
                    .itemNo(record.get("ITEM_NO", BigInteger.class).intValue())
                    .deleted(record.get("DELETED", Boolean.class))
                    .phraseOptionId(record.get("PHRASE_OPTION_ID", BigInteger.class).longValue())
                    .phraseOptionItemId(record.get("PHRASE_OPTION_ITEM_ID", BigInteger.class).longValue())
                    .basisVersion(record.get("VERSION", String.class))
                    .version(record.get("BASIS_VERSION", String.class))
                    .phrase(dto));
            }
        });

        return res;
    }

    public PhraseOptionEditDTO getNewPhrase(Integer domainId) {
        return new PhraseOptionEditDTO()
            .domain(copeditDomainRepository.getOne(domainId.longValue()))
            .version(new VersionDTO()
                .version("1.0")
                .baseVersion("1.0")
            )
            .phrasesDE(new PhraseLanguageDTO().language(LANGUAGE_DE).phrases(new ArrayList<>()).phrasesItemPartNo2(new ArrayList<>()))
            .phrasesIT(new PhraseLanguageDTO().language(LANGUAGE_IT).phrases(new ArrayList<>()).phrasesItemPartNo2(new ArrayList<>()))
            .phrasesEN(new PhraseLanguageDTO().language(LANGUAGE_EN).phrases(new ArrayList<>()).phrasesItemPartNo2(new ArrayList<>()))
            .phrasesFR(new PhraseLanguageDTO().language(LANGUAGE_FR).phrases(new ArrayList<>()).phrasesItemPartNo2(new ArrayList<>()))
            ;
    }

    /**
     * Updates an existing phrase
     *
     * @param phrase phrase to update
     * @return the updated phrase
     */
    @Transactional
    public PhraseOptionEditDTO updatePhrase(PhraseOptionEditDTO phrase) {

        /* consumer that sets the last update date to now() on PhraseOptionItem and Phrase */
        Consumer<Object> updateDatesConsumer = x -> {
            if (x instanceof PhraseOptionItem) {
                if (((PhraseOptionItem) x).getPhraseOptionItemId() == null) {
                    ((PhraseOptionItem) x).setDatenew(ZonedDateTime.now());
                }
                ((PhraseOptionItem) x).setDatelast(ZonedDateTime.now());
            } else if (x instanceof Phrase) {
                if (((Phrase) x).getId() == null) {
                    ((Phrase) x).setDatenew(ZonedDateTime.now());
                }
                ((Phrase) x).setDatelast(ZonedDateTime.now());
            }
        };

        List<PhraseOption> phraseOptionList = new ArrayList<>(
            Arrays.asList(
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesDE()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesIT()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesEN()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesFR())
            )
        );

        List<PhraseOptionItem> phraseOptionItemsDE = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesDE().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsIT = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesIT().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsEN = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesEN().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsFR = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesFR().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsFRItem2 = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesFR().getPhrasesItemPartNo2());
        List<PhraseOptionItem> phraseOptionItemsDEItem2 = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesDE().getPhrasesItemPartNo2());
        List<PhraseOptionItem> phraseOptionItemsITItem2 = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesIT().getPhrasesItemPartNo2());
        List<PhraseOptionItem> phraseOptionItemsENItem2 = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesEN().getPhrasesItemPartNo2());

        /* update lastDate */
        ZonedDateTime lastupdate = ZonedDateTime.now();
        phraseOptionList.forEach(x -> x.datelast(lastupdate));
        phraseOptionItemsDE.forEach(updateDatesConsumer);
        phraseOptionItemsIT.forEach(updateDatesConsumer);
        phraseOptionItemsEN.forEach(updateDatesConsumer);
        phraseOptionItemsFR.forEach(updateDatesConsumer);
        phraseOptionItemsFRItem2.forEach(updateDatesConsumer);
        phraseOptionItemsDEItem2.forEach(updateDatesConsumer);
        phraseOptionItemsITItem2.forEach(updateDatesConsumer);
        phraseOptionItemsENItem2.forEach(updateDatesConsumer);

        phraseOptionList.forEach(x -> {
            em.createNativeQuery("UPDATE PHRASE_OPTION set NAME = :name, HEADER = :header, REMARK = :remark, DATELAST = :datelast WHERE PHRASE_OPTION_ID = :id")
                .setParameter("name", x.getName())
                .setParameter("header", x.getHeader())
                .setParameter("remark", StringUtils.isEmpty(x.getRemark()) ? null : x.getRemark())
                .setParameter("datelast", lastupdate)
                .setParameter("id", x.getId())
                .executeUpdate();

        });


        /*List<Phrase> phraseDE = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesDE().getPhrases());
        phraseDE.forEach(updateDatesConsumer);
        List<Phrase> phraseIT = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesIT().getPhrases());
        phraseIT.forEach(updateDatesConsumer);
        List<Phrase> phraseEN = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesEN().getPhrases());
        phraseEN.forEach(updateDatesConsumer);
        List<Phrase> phraseFR = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesFR().getPhrases());
        phraseFR.forEach(updateDatesConsumer);*/

        /*List<Long> phraseIDList = phraseDE.stream().filter(p -> p.getId() != null).map(Phrase::getId).collect(Collectors.toList());
        phraseIDList.addAll(phraseIT.stream().filter(p -> p.getId() != null).map(Phrase::getId).collect(Collectors.toList()));
        phraseIDList.addAll(phraseEN.stream().filter(p -> p.getId() != null).map(Phrase::getId).collect(Collectors.toList()));
        phraseIDList.addAll(phraseFR.stream().filter(p -> p.getId() != null).map(Phrase::getId).collect(Collectors.toList()));*/

        List<Long> optionItemIdList = Arrays.asList(phrase.getPhrasesDE().getId(), phrase.getPhrasesEN().getId(),
            phrase.getPhrasesIT().getId(), phrase.getPhrasesFR().getId());

        /* delete phrases*/
        em.createNativeQuery("DELETE FROM PHRASE WHERE PHRASE_ID IN (SELECT phrase_id\n" +
            "FROM phrase_option_item\n" +
            "INNER JOIN phrase ON phrase.phrase_option_item_id = phrase_option_item.phrase_option_item_id\n" +
            "WHERE phrase_option_item.phrase_option_id IN :phraseIdList)")
            .setParameter("phraseIdList", optionItemIdList)
            .executeUpdate();

        /* delete phrase_option_item*/
        em.createNativeQuery("DELETE FROM PHRASE_OPTION_ITEM WHERE PHRASE_OPTION_ID IN :ids")
            .setParameter("ids", optionItemIdList)
            .executeUpdate();

        savePhrases(phrase.getPhrasesDE().getPhrases(), lastupdate, false);
        savePhrases(phrase.getPhrasesEN().getPhrases(), lastupdate, false);
        savePhrases(phrase.getPhrasesIT().getPhrases(), lastupdate, false);
        savePhrases(phrase.getPhrasesFR().getPhrases(), lastupdate, false);
        savePhrases(phrase.getPhrasesDE().getPhrasesItemPartNo2(), lastupdate, true);
        savePhrases(phrase.getPhrasesEN().getPhrasesItemPartNo2(), lastupdate, true);
        savePhrases(phrase.getPhrasesIT().getPhrasesItemPartNo2(), lastupdate, true);
        savePhrases(phrase.getPhrasesFR().getPhrasesItemPartNo2(), lastupdate, true);

        /* return saved phrase */
        PhraseOptionEditDTO result = findPhraseByName(phrase.getPhrasesDE().getName(), phrase.getDomain().getId().intValue());
        return result;
    }

    /**
     * Updates an existing phrase
     *
     * @param phrase phrase to update
     * @return the updated phrase
     */
    @Transactional
    public PhraseOptionEditDTO createPhrase(PhraseOptionEditDTO phrase) {

        /* consumer that sets the last update date to now() on PhraseOptionItem and Phrase */
        Consumer<Object> updateDatesConsumer = x -> {
            if (x instanceof PhraseOptionItem) {
                if (((PhraseOptionItem) x).getPhraseOptionItemId() == null) {
                    ((PhraseOptionItem) x).setDatenew(ZonedDateTime.now());
                }
                ((PhraseOptionItem) x).setDatelast(ZonedDateTime.now());
            } else if (x instanceof Phrase) {
                if (((Phrase) x).getId() == null) {
                    ((Phrase) x).setDatenew(ZonedDateTime.now());
                }
                ((Phrase) x).setDatelast(ZonedDateTime.now());
            }
        };

        List<PhraseOption> phraseOptionList = new ArrayList<>(
            Arrays.asList(
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesDE()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesIT()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesEN()),
                copeditPhraseMapper.phraseLanguageToPhraseOption(phrase.getPhrasesFR())
            )
        );

        List<PhraseOptionItem> phraseOptionItemsDE = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesDE().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsIT = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesIT().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsEN = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesEN().getPhrases());
        List<PhraseOptionItem> phraseOptionItemsFR = copeditPhraseMapper.phraseOptionItemsDTOToPhraseOptionItem(phrase.getPhrasesFR().getPhrases());

        /* update lastDate */
        ZonedDateTime lastupdate = ZonedDateTime.now();
        phraseOptionList.forEach(x -> x.datelast(lastupdate));
        phraseOptionItemsDE.forEach(updateDatesConsumer);
        phraseOptionItemsIT.forEach(updateDatesConsumer);
        phraseOptionItemsEN.forEach(updateDatesConsumer);
        phraseOptionItemsFR.forEach(updateDatesConsumer);

        phraseOptionList.forEach(x -> {

            /* set domain to phraseOptions*/
            x.setDomainId(phrase.getDomain().getId().intValue());


            StringBuilder sb = new StringBuilder("INSERT INTO phrase_option (phrase_option_id, name, language, header, version," +
                " basis_version, remark, deleted, domain_id, datenew, datelast) VALUES(");
            sb.append("nextval('seq_phrase_option_id'), ");
            sb.append(String.format("'%s', ", StringEscapeUtils.escapeSql(x.getName())));
            sb.append(String.format("'%s', ", x.getLanguage()));
            sb.append(String.format("'%s', ", x.getHeader()));
            sb.append(String.format("'%s', ", x.getVersion()));
            sb.append(String.format("'%s', ", x.getBasisVersion()));
            sb.append(StringUtils.isEmpty(x.getRemark()) ? "NULL, " : String.format("'%s', ", x.getRemark()));
            sb.append(String.format("%b, ", x.isDeleted()));
            sb.append(String.format("%d, ", x.getDomainId()));
            sb.append(String.format("'%s', ", lastupdate.format(Constants.pgDateTimeFormatter)));
            sb.append(String.format("'%s' ", lastupdate.format(Constants.pgDateTimeFormatter)));
            sb.append(") returning phrase_option_id");

            /*em.createNativeQuery("INSERT INTO phrase_option (phrase_option_id, name, language, header, version, " +
                "basis_version, remark, deleted, domain_id, datenew, datelast) VALUES(nextval('seq_phrase_option_id'), " +
                ":name, :language, , '', '', '', false, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);\n")
                .setParameter("name", x.getName())
                .setParameter("header", x.getHeader())
                .setParameter("remark", x.getRemark())
                .setParameter("language", x.getLanguage())
                .executeUpdate();*/
            BigInteger phraseOptionId = (BigInteger) em.createNativeQuery(sb.toString()).getSingleResult();
            switch (x.getLanguage()) {

                case Constants.LANGUAGE_DE:
                    phrase.getPhrasesDE().getPhrases().forEach(y -> y.setPhraseOptionId(phraseOptionId.longValue()));
                    break;
                case Constants.LANGUAGE_EN:
                    phrase.getPhrasesEN().getPhrases().forEach(y -> y.setPhraseOptionId(phraseOptionId.longValue()));
                    break;
                case Constants.LANGUAGE_IT:
                    phrase.getPhrasesIT().getPhrases().forEach(y -> y.setPhraseOptionId(phraseOptionId.longValue()));
                    break;
                case Constants.LANGUAGE_FR:
                    phrase.getPhrasesFR().getPhrases().forEach(y -> y.setPhraseOptionId(phraseOptionId.longValue()));
                    break;
            }

        });

        List<Phrase> phraseDE = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesDE().getPhrases());
        phraseDE.forEach(updateDatesConsumer);
        List<Phrase> phraseIT = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesIT().getPhrases());
        phraseIT.forEach(updateDatesConsumer);
        List<Phrase> phraseEN = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesEN().getPhrases());
        phraseEN.forEach(updateDatesConsumer);
        List<Phrase> phraseFR = copeditPhraseMapper.phraseDTOToPhraseList(phrase.getPhrasesFR().getPhrases());
        phraseFR.forEach(updateDatesConsumer);

        insertPhrases(phrase.getPhrasesDE().getPhrases(), lastupdate);
        insertPhrases(phrase.getPhrasesEN().getPhrases(), lastupdate);
        insertPhrases(phrase.getPhrasesIT().getPhrases(), lastupdate);
        insertPhrases(phrase.getPhrasesFR().getPhrases(), lastupdate);

        /* return saved phrase */
        PhraseOptionEditDTO result = findPhraseByName(phrase.getPhrasesDE().getName(), phrase.getDomain().getId().intValue());
        return result;
    }

    public Integer booleanToInt(Boolean bool) {
        if (bool) {
            return 1;
        }
        return 0;
    }

    /**
     * Saves the phrases of the given language
     *
     * @param phrases    phrases to save
     * @param lastupdate last updated date to set, also datenew if the phrase in new
     */
    public void savePhrases(List<PhraseOptionItemDTO> phrases, ZonedDateTime lastupdate, Boolean isException) {
        phrases.forEach(x -> {


            Long insertedPhraseOptionItemId = x.getPhraseOptionItemId() != null ? x.getPhraseOptionItemId() :
                ((BigInteger) em.createNativeQuery("SELECT nextval('SEQ_PHRASE_OPTION_ITEM_ID')").getSingleResult()).longValue();

            if (x.getDatenew() == null) {
                x.datenew(lastupdate);
            }
            x.datelast(lastupdate);

            if (!isException) {

                /* insert PHRASE_OPTION_ITEM */
                StringBuilder sb = new StringBuilder("INSERT INTO PHRASE_OPTION_ITEM (PHRASE_OPTION_ITEM_ID, ITEM_NO, DELETED, PHRASE_OPTION_ID, VERSION, " +
                    "BASIS_VERSION, DATENEW, DATELAST) ");
                sb.append("VALUES( ");
                sb.append(insertedPhraseOptionItemId);
                sb.append(", " + x.getPhrase().getItemNo());
                sb.append(", " + x.getDeleted());
                sb.append(", " + x.getPhraseOptionId());
                sb.append(", '" + x.getVersion() + "'");
                sb.append(", '" + x.getBasisVersion() + "'");
                sb.append(", '" + x.getDatenew().format(Constants.pgDateTimeFormatter) + "'");
                sb.append(", '" + x.getDatelast().format(Constants.pgDateTimeFormatter) + "'");
                sb.append(")");

                String query = sb.toString();
                em.createNativeQuery(query).executeUpdate();
            }

            if (x.getPhrase().getDatenew() == null) {
                x.getPhrase().datenew(lastupdate);
            }
            x.getPhrase().datelast(lastupdate);

            Long phraseId = x.getPhrase().getId() != null ? x.getPhrase().getId() :
                ((BigInteger) em.createNativeQuery("SELECT nextval('SEQ_PHRASE_ID')").getSingleResult()).longValue();

            /* insert PHRASE_OPTION_ITEM */
            StringBuilder sbPhrase = new StringBuilder("INSERT INTO PHRASE (PHRASE_ID, VALUE, SPACE_BEFORE, SPACE_AFTER, " +
                "REMOVE_PUNCTUATION_BEFORE, ITEM_PART_NO, INCORRECT, PHRASE_OPTION_ITEM_ID, DATENEW, DATELAST) ");
            sbPhrase.append("VALUES( ");
            sbPhrase.append(phraseId);
            sbPhrase.append(StringUtils.isEmpty(x.getPhrase().getValue()) ? ", NULL" : ", '" + StringEscapeUtils.escapeSql(x.getPhrase().getValue()) + "' ");
            sbPhrase.append(", " + x.getPhrase().getSpaceBefore());
            sbPhrase.append(", " + x.getPhrase().getSpaceAfter());
            sbPhrase.append(", " + x.getPhrase().getRemovePunctuationBefore());
            sbPhrase.append(", " + x.getPhrase().getItemPartNo());
            sbPhrase.append(", " + x.getPhrase().getIncorrect());
            sbPhrase.append(", " + insertedPhraseOptionItemId);
            sbPhrase.append(", '" + x.getPhrase().getDatenew().format(Constants.pgDateTimeFormatter) + "'");
            sbPhrase.append(", '" + x.getPhrase().getDatelast().format(Constants.pgDateTimeFormatter) + "'");
            sbPhrase.append(")");

            em.createNativeQuery(sbPhrase.toString())
                .executeUpdate();

        });
    }

    /**
     * Saves the phrases of the given language
     *
     * @param phrases    phrases to save
     * @param lastupdate last updated date to set, also datenew if the phrase in new
     */
    public void insertPhrases(List<PhraseOptionItemDTO> phrases, ZonedDateTime lastupdate) {
        phrases.forEach(x -> {


            Long insertedPhraseOptionItemId = x.getPhraseOptionItemId() != null ? x.getPhraseOptionItemId() :
                ((BigInteger) em.createNativeQuery("SELECT nextval('SEQ_PHRASE_OPTION_ITEM_ID')").getSingleResult()).longValue();

            if (x.getDatenew() == null) {
                x.datenew(lastupdate);
            }
            x.datelast(lastupdate);

            /* insert PHRASE_OPTION_ITEM */
            StringBuilder sb = new StringBuilder("INSERT INTO PHRASE_OPTION_ITEM (PHRASE_OPTION_ITEM_ID, ITEM_NO, DELETED, PHRASE_OPTION_ID, VERSION, " +
                "BASIS_VERSION, DATENEW, DATELAST) ");
            sb.append("VALUES( ");
            sb.append(insertedPhraseOptionItemId);
            sb.append(", " + x.getPhrase().getItemNo());
            sb.append(", " + x.getDeleted());
            sb.append(", " + x.getPhraseOptionId());
            sb.append(", '" + x.getVersion() + "'");
            sb.append(", '" + x.getBasisVersion() + "'");
            sb.append(", '" + x.getDatenew().format(Constants.pgDateTimeFormatter) + "'");
            sb.append(", '" + x.getDatelast().format(Constants.pgDateTimeFormatter) + "'");
            sb.append(") returning PHRASE_OPTION_ITEM_ID");

            String query = sb.toString();
            BigInteger phraseOptionItemId = (BigInteger) em.createNativeQuery(query).getSingleResult();
            x.phraseOptionItemId(phraseOptionItemId.longValue());

            if (x.getPhrase().getDatenew() == null) {
                x.getPhrase().datenew(lastupdate);
            }
            x.getPhrase().datelast(lastupdate);

            Long phraseId = x.getPhrase().getId() != null ? x.getPhrase().getId() :
                ((BigInteger) em.createNativeQuery("SELECT nextval('SEQ_PHRASE_ID')").getSingleResult()).longValue();

            /* insert PHRASE_OPTION_ITEM */
            StringBuilder sbPhrase = new StringBuilder("INSERT INTO PHRASE (PHRASE_ID, VALUE, SPACE_BEFORE, SPACE_AFTER, " +
                "REMOVE_PUNCTUATION_BEFORE, ITEM_PART_NO, INCORRECT, PHRASE_OPTION_ITEM_ID, DATENEW, DATELAST) ");
            sbPhrase.append("VALUES( ");
            sbPhrase.append(phraseId);
            sbPhrase.append(StringUtils.isEmpty(x.getPhrase().getValue()) ? ", NULL" : ", '" + StringEscapeUtils.escapeSql(x.getPhrase().getValue()) + "' ");
            sbPhrase.append(", " + x.getPhrase().getSpaceBefore());
            sbPhrase.append(", " + x.getPhrase().getSpaceAfter());
            sbPhrase.append(", " + x.getPhrase().getRemovePunctuationBefore());
            sbPhrase.append(", " + x.getPhrase().getItemPartNo());
            sbPhrase.append(", " + x.getPhrase().getIncorrect());
            sbPhrase.append(", " + insertedPhraseOptionItemId);
            sbPhrase.append(", '" + x.getPhrase().getDatenew().format(Constants.pgDateTimeFormatter) + "'");
            sbPhrase.append(", '" + x.getPhrase().getDatelast().format(Constants.pgDateTimeFormatter) + "'");
            sbPhrase.append(")");

            em.createNativeQuery(sbPhrase.toString())
                .executeUpdate();

        });
    }
}
