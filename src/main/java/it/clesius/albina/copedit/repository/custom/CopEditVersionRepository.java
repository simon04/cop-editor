package it.clesius.albina.copedit.repository.custom;

import it.clesius.albina.copedit.domain.copedit.VersionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Service
public class CopEditVersionRepository implements JpaRepository<VersionDTO, Long> {

    @Autowired
    EntityManager em;


    @Override
    public List<VersionDTO> findAll() {
        Query q = em.createNativeQuery("select DISTINCT s.VERSION, s. BASIS_VERSION from SENTENCE s", "VersionMapper");
        List<VersionDTO> versions = q.getResultList();
        return versions;
    }

    @Override
    public List<VersionDTO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<VersionDTO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<VersionDTO> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(VersionDTO version) {

    }

    @Override
    public void deleteAll(Iterable<? extends VersionDTO> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends VersionDTO> S save(S s) {
        return null;
    }

    @Override
    public <S extends VersionDTO> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<VersionDTO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends VersionDTO> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<VersionDTO> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public VersionDTO getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends VersionDTO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends VersionDTO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends VersionDTO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends VersionDTO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends VersionDTO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends VersionDTO> boolean exists(Example<S> example) {
        return false;
    }

    @SqlResultSetMapping(
        name = "VersionMapper",
        classes = @ConstructorResult(
            targetClass = VersionDTO.class,
            columns = {
                @ColumnResult(name = "version", type = String.class),
                @ColumnResult(name = "basis_version", type = String.class)
            }))
    @Entity
    class VersionMapper {
        @Id
        int id;
    }
}
