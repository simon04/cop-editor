package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.PhraseOption;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing PhraseOption.
 */
public interface PhraseOptionService {

    /**
     * Save a phraseOption.
     *
     * @param phraseOption the entity to save
     * @return the persisted entity
     */
    PhraseOption save(PhraseOption phraseOption);

    /**
     * Get all the phraseOptions.
     *
     * @return the list of entities
     */
    List<PhraseOption> findAll();


    /**
     * Get the "id" phraseOption.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PhraseOption> findOne(Long id);

    /**
     * Delete the "id" phraseOption.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
