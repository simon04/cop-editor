package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.SentenceLang;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SentenceLang.
 */
public interface SentenceLangService {

    /**
     * Save a sentenceLang.
     *
     * @param sentenceLang the entity to save
     * @return the persisted entity
     */
    SentenceLang save(SentenceLang sentenceLang);

    /**
     * Get all the sentenceLangs.
     *
     * @return the list of entities
     */
    List<SentenceLang> findAll();


    /**
     * Get the "id" sentenceLang.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SentenceLang> findOne(Long id);

    /**
     * Delete the "id" sentenceLang.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
