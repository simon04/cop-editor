package it.clesius.albina.copedit.service.copedit;

import it.clesius.albina.copedit.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Async
@EnableAsync
public class ImportFilesService {

    private final Logger log = LoggerFactory.getLogger(ImportFilesService.class);

    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    private SimpMessagingTemplate template;

    @Async
    public void launchNewThreadAsync(String domainName) {

        try {
            RestTemplate rt = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(applicationProperties.getTextcatApi().gettextcatAPIurl() + applicationProperties.getTextcatApi().getImportUrl())
                .queryParam("domain", domainName);
            String response = rt.getForObject(builder.toUriString(), String.class);
            log.info(String.format("Import outcome: %s", response));

            this.template.convertAndSend("/import/importStatus", response);
        } catch (Exception e) {
            e.printStackTrace();
            this.template.convertAndSend("/import/importStatus", "FATAL");
        }

    }
}
