package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.DomainService;
import it.clesius.albina.copedit.domain.Domain;
import it.clesius.albina.copedit.repository.DomainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Domain.
 */
@Service
@Transactional
public class DomainServiceImpl implements DomainService {

    private final Logger log = LoggerFactory.getLogger(DomainServiceImpl.class);

    private final DomainRepository domainRepository;

    public DomainServiceImpl(DomainRepository domainRepository) {
        this.domainRepository = domainRepository;
    }

    /**
     * Save a domain.
     *
     * @param domain the entity to save
     * @return the persisted entity
     */
    @Override
    public Domain save(Domain domain) {
        log.debug("Request to save Domain : {}", domain);        return domainRepository.save(domain);
    }

    /**
     * Get all the domains.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Domain> findAll() {
        log.debug("Request to get all Domains");
        return domainRepository.findAll();
    }


    /**
     * Get one domain by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Domain> findOne(Long id) {
        log.debug("Request to get Domain : {}", id);
        return domainRepository.findById(id);
    }

    /**
     * Delete the domain by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Domain : {}", id);
        domainRepository.deleteById(id);
    }
}
