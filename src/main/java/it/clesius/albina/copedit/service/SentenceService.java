package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.Sentence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Sentence.
 */
public interface SentenceService {

    /**
     * Save a sentence.
     *
     * @param sentence the entity to save
     * @return the persisted entity
     */
    Sentence save(Sentence sentence);

    /**
     * Get all the sentences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Sentence> findAll(Pageable pageable);


    /**
     * Get the "id" sentence.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Sentence> findOne(Long id);

    /**
     * Delete the "id" sentence.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
