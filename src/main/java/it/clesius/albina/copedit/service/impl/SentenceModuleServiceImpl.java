package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.SentenceModuleService;
import it.clesius.albina.copedit.domain.SentenceModule;
import it.clesius.albina.copedit.repository.SentenceModuleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing SentenceModule.
 */
@Service
@Transactional
public class SentenceModuleServiceImpl implements SentenceModuleService {

    private final Logger log = LoggerFactory.getLogger(SentenceModuleServiceImpl.class);

    private final SentenceModuleRepository sentenceModuleRepository;

    public SentenceModuleServiceImpl(SentenceModuleRepository sentenceModuleRepository) {
        this.sentenceModuleRepository = sentenceModuleRepository;
    }

    /**
     * Save a sentenceModule.
     *
     * @param sentenceModule the entity to save
     * @return the persisted entity
     */
    @Override
    public SentenceModule save(SentenceModule sentenceModule) {
        log.debug("Request to save SentenceModule : {}", sentenceModule);        return sentenceModuleRepository.save(sentenceModule);
    }

    /**
     * Get all the sentenceModules.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SentenceModule> findAll() {
        log.debug("Request to get all SentenceModules");
        return sentenceModuleRepository.findAll();
    }


    /**
     * Get one sentenceModule by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SentenceModule> findOne(Long id) {
        log.debug("Request to get SentenceModule : {}", id);
        return sentenceModuleRepository.findById(id);
    }

    /**
     * Delete the sentenceModule by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SentenceModule : {}", id);
        sentenceModuleRepository.deleteById(id);
    }
}
