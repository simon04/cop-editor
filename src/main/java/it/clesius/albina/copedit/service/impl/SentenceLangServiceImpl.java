package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.SentenceLangService;
import it.clesius.albina.copedit.domain.SentenceLang;
import it.clesius.albina.copedit.repository.SentenceLangRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing SentenceLang.
 */
@Service
@Transactional
public class SentenceLangServiceImpl implements SentenceLangService {

    private final Logger log = LoggerFactory.getLogger(SentenceLangServiceImpl.class);

    private final SentenceLangRepository sentenceLangRepository;

    public SentenceLangServiceImpl(SentenceLangRepository sentenceLangRepository) {
        this.sentenceLangRepository = sentenceLangRepository;
    }

    /**
     * Save a sentenceLang.
     *
     * @param sentenceLang the entity to save
     * @return the persisted entity
     */
    @Override
    public SentenceLang save(SentenceLang sentenceLang) {
        log.debug("Request to save SentenceLang : {}", sentenceLang);        return sentenceLangRepository.save(sentenceLang);
    }

    /**
     * Get all the sentenceLangs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SentenceLang> findAll() {
        log.debug("Request to get all SentenceLangs");
        return sentenceLangRepository.findAll();
    }


    /**
     * Get one sentenceLang by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SentenceLang> findOne(Long id) {
        log.debug("Request to get SentenceLang : {}", id);
        return sentenceLangRepository.findById(id);
    }

    /**
     * Delete the sentenceLang by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SentenceLang : {}", id);
        sentenceLangRepository.deleteById(id);
    }
}
