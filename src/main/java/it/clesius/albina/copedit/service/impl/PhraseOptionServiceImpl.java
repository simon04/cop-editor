package it.clesius.albina.copedit.service.impl;

import it.clesius.albina.copedit.service.PhraseOptionService;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.repository.PhraseOptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing PhraseOption.
 */
@Service
@Transactional
public class PhraseOptionServiceImpl implements PhraseOptionService {

    private final Logger log = LoggerFactory.getLogger(PhraseOptionServiceImpl.class);

    private final PhraseOptionRepository phraseOptionRepository;

    public PhraseOptionServiceImpl(PhraseOptionRepository phraseOptionRepository) {
        this.phraseOptionRepository = phraseOptionRepository;
    }

    /**
     * Save a phraseOption.
     *
     * @param phraseOption the entity to save
     * @return the persisted entity
     */
    @Override
    public PhraseOption save(PhraseOption phraseOption) {
        log.debug("Request to save PhraseOption : {}", phraseOption);        return phraseOptionRepository.save(phraseOption);
    }

    /**
     * Get all the phraseOptions.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PhraseOption> findAll() {
        log.debug("Request to get all PhraseOptions");
        return phraseOptionRepository.findAll();
    }


    /**
     * Get one phraseOption by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhraseOption> findOne(Long id) {
        log.debug("Request to get PhraseOption : {}", id);
        return phraseOptionRepository.findById(id);
    }

    /**
     * Delete the phraseOption by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhraseOption : {}", id);
        phraseOptionRepository.deleteById(id);
    }
}
