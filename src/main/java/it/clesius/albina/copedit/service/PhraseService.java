package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.Phrase;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Phrase.
 */
public interface PhraseService {

    /**
     * Save a phrase.
     *
     * @param phrase the entity to save
     * @return the persisted entity
     */
    Phrase save(Phrase phrase);

    /**
     * Get all the phrases.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Phrase> findAll(Pageable pageable);


    /**
     * Get the "id" phrase.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Phrase> findOne(Long id);

    /**
     * Delete the "id" phrase.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
