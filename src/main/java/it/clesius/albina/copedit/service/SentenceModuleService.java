package it.clesius.albina.copedit.service;

import it.clesius.albina.copedit.domain.SentenceModule;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SentenceModule.
 */
public interface SentenceModuleService {

    /**
     * Save a sentenceModule.
     *
     * @param sentenceModule the entity to save
     * @return the persisted entity
     */
    SentenceModule save(SentenceModule sentenceModule);

    /**
     * Get all the sentenceModules.
     *
     * @return the list of entities
     */
    List<SentenceModule> findAll();


    /**
     * Get the "id" sentenceModule.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SentenceModule> findOne(Long id);

    /**
     * Delete the "id" sentenceModule.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
