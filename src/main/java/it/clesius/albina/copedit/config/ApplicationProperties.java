package it.clesius.albina.copedit.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to App.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final ApplicationProperties.ImportProperties importProperties = new ApplicationProperties.ImportProperties();

    private final ApplicationProperties.TextcatApi textcatApi = new ApplicationProperties.TextcatApi();

    private final ApplicationProperties.ExportProperties exportProperties = new ApplicationProperties.ExportProperties();


    public ExportProperties getExportProperties() {
        return exportProperties;
    }

    public TextcatApi getTextcatApi() {
        return textcatApi;
    }

    public ImportProperties getImportProperties() {
        return importProperties;
    }

    public static class ImportProperties {

        String importFolder;

        public String getImportFolder() {
            return importFolder;
        }

        public void setImportFolder(String importFolder) {
            this.importFolder = importFolder;
        }
    }

    public static class TextcatApi {
        String textcatAPIurl;
        String sentencesUrl;
        String indexesUrl;
        String importUrl;
        String recodeUrl;
        String reloadUrl;

        public void setReloadUrl(String reloadUrl) {
            this.reloadUrl = reloadUrl;
        }

        public String getReloadUrl() {
            return reloadUrl;
        }

        public String gettextcatAPIurl() {
            return textcatAPIurl;
        }

        public void settextcatAPIurl(String textcatAPIurl) {
            this.textcatAPIurl= textcatAPIurl;
        }

        public String getRecodeUrl() {
            return recodeUrl;
       }
        public void setRecodeUrl(String recodeUrl) {
            this.recodeUrl = recodeUrl;
        }

        public String getIndexesUrl() {
            return indexesUrl;
        }

        public String getImportUrl() {
            return importUrl;
        }

        public void setImportUrl(String importUrl) {
            this.importUrl = importUrl;
        }

        public String getSentencesUrl() {
            return sentencesUrl;
        }

        public void setSentencesUrl(String sentencesUrl) {
            this.sentencesUrl = sentencesUrl;
        }

        public void setIndexesUrl(String indexesUrl) {
            this.indexesUrl = indexesUrl;
        }
    }

    public static class ExportProperties {

        String tempFolder;

        public String getTempFolder() {
            return tempFolder;
        }

        public void setTempFolder(String tempFolder) {
            this.tempFolder = tempFolder;
        }
    }

}
