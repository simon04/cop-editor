package it.clesius.albina.copedit.config;

import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

@Controller
@Service
public class WebSocketController {

    @SendTo("/import/importStatus")
    public String greeting(String message) throws Exception {
        return HtmlUtils.htmlEscape(message);
    }
}
