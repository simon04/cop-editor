package it.clesius.albina.copedit.config;

import java.time.format.DateTimeFormatter;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "de";

    /* language constants */
    public static final String LANGUAGE_DE = "de";
    public static final String LANGUAGE_IT = "it";
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_FR = "fr";

    public static final DateTimeFormatter pgDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private Constants() {
    }
}
