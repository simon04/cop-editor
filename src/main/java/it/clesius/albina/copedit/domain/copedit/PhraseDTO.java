package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;

public class PhraseDTO {

    Long id;
    String value;
    Boolean spaceBefore;
    Boolean spaceAfter;
    Boolean removePunctuationBefore;
    Integer itemPartNo;
    Integer itemNo;
    Boolean incorrect;
    ZonedDateTime datenew;
    ZonedDateTime datelast;

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getSpaceBefore() {
        return spaceBefore;
    }

    public void setSpaceBefore(Boolean spaceBefore) {
        this.spaceBefore = spaceBefore;
    }

    public Boolean getSpaceAfter() {
        return spaceAfter;
    }

    public void setSpaceAfter(Boolean spaceAfter) {
        this.spaceAfter = spaceAfter;
    }

    public Boolean getRemovePunctuationBefore() {
        return removePunctuationBefore;
    }

    public void setRemovePunctuationBefore(Boolean removePunctuationBefore) {
        this.removePunctuationBefore = removePunctuationBefore;
    }

    public Integer getItemPartNo() {
        return itemPartNo;
    }

    public void setItemPartNo(Integer itemPartNo) {
        this.itemPartNo = itemPartNo;
    }

    public Boolean getIncorrect() {
        return incorrect;
    }

    public void setIncorrect(Boolean incorrect) {
        this.incorrect = incorrect;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }

    public PhraseDTO id(Long id) {
        this.id = id;
        return this;
    }

    public PhraseDTO value(String value) {
        this.value = value;
        return this;
    }

    public PhraseDTO spaceBefore(Boolean spaceBefore) {
        this.spaceBefore = spaceBefore;
        return this;
    }

    public PhraseDTO spaceAfter(Boolean spaceAfter) {
        this.spaceAfter = spaceAfter;
        return this;
    }

    public PhraseDTO removePunctuationBefore(Boolean removePunctuationBefore) {
        this.removePunctuationBefore = removePunctuationBefore;
        return this;
    }

    public PhraseDTO itemPartNo(Integer itemPartNo) {
        this.itemPartNo = itemPartNo;
        return this;
    }

    public PhraseDTO incorrect(Boolean incorrect) {
        this.incorrect = incorrect;
        return this;
    }

    public PhraseDTO datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public PhraseDTO datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public PhraseDTO itemNo(Integer itemNo) {
        this.itemNo = itemNo;
        return this;
    }
}
