package it.clesius.albina.copedit.domain.enumeration;

/**
 * The LanguageEnum enumeration.
 */
public enum LanguageEnum {
    DE, EN, IT, FR
}
