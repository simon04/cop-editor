package it.clesius.albina.copedit.domain.copedit;

public class SentenceDTO {

    DomainDTO domain;
    VersionDTO version;
    Boolean deleted;
    Boolean jokerSentence;
    SentenceLanguage sentencesDE;
    SentenceLanguage sentencesIT;
    SentenceLanguage sentencesFR;
    SentenceLanguage sentencesEN;

    public DomainDTO getDomain() {
        return domain;
    }

    public void setDomain(DomainDTO domain) {
        this.domain = domain;
    }

    public VersionDTO getVersion() {
        return version;
    }

    public void setVersion(VersionDTO version) {
        this.version = version;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getJokerSentence() {
        return jokerSentence;
    }

    public void setJokerSentence(Boolean jokerSentence) {
        this.jokerSentence = jokerSentence;
    }

    public SentenceLanguage getSentencesDE() {
        return sentencesDE;
    }

    public void setSentencesDE(SentenceLanguage sentencesDE) {
        this.sentencesDE = sentencesDE;
    }

    public SentenceLanguage getSentencesIT() {
        return sentencesIT;
    }

    public void setSentencesIT(SentenceLanguage sentencesIT) {
        this.sentencesIT = sentencesIT;
    }

    public SentenceLanguage getSentencesFR() {
        return sentencesFR;
    }

    public void setSentencesFR(SentenceLanguage sentencesFR) {
        this.sentencesFR = sentencesFR;
    }

    public SentenceLanguage getSentencesEN() {
        return sentencesEN;
    }

    public void setSentencesEN(SentenceLanguage sentencesEN) {
        this.sentencesEN = sentencesEN;
    }


    public SentenceDTO domain(DomainDTO domain) {
        this.domain = domain;
        return this;
    }

    public SentenceDTO version(VersionDTO version) {
        this.version = version;
        return this;
    }

    public SentenceDTO deleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    public SentenceDTO jokerSentence(Boolean jokerSentence) {
        this.jokerSentence = jokerSentence;
        return this;
    }

    public SentenceDTO sentencesDE(SentenceLanguage sentencesDE) {
        this.sentencesDE = sentencesDE;
        return this;
    }

    public SentenceDTO sentencesIT(SentenceLanguage sentencesIT) {
        this.sentencesIT = sentencesIT;
        return this;
    }

    public SentenceDTO sentencesFR(SentenceLanguage sentencesFR) {
        this.sentencesFR = sentencesFR;
        return this;
    }

    public SentenceDTO sentencesEN(SentenceLanguage sentencesEN) {
        this.sentencesEN = sentencesEN;
        return this;
    }
}
