package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.SentenceModule;
import it.clesius.albina.copedit.domain.copedit.SentenceDTO;
import it.clesius.albina.copedit.domain.copedit.SentenceLanguage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CopeditSentenceModuleMapper {

    public List<SentenceModule> sentenceDTOToSentenceModule(SentenceDTO dto) {

        List<SentenceModule> modules = new ArrayList<>();
        modules.addAll(sentenceLanguageDTOToSentenceModule(dto.getSentencesDE()));
        modules.addAll(sentenceLanguageDTOToSentenceModule(dto.getSentencesIT()));
        modules.addAll(sentenceLanguageDTOToSentenceModule(dto.getSentencesEN()));
        modules.addAll(sentenceLanguageDTOToSentenceModule(dto.getSentencesFR()));
        return modules;
    }

    public List<SentenceModule> sentenceLanguageDTOToSentenceModule(SentenceLanguage sl) {
        List<SentenceModule> sentenceModules = new ArrayList<>();
        Long sentenceId = sl.getId();
        sl.getPhraseOptions().forEach(po -> {
            SentenceModule sm = new SentenceModule()
                .sentenceId(Optional.ofNullable(sentenceId).map(Long::intValue).orElse(null))
                .phraseOptionId(po.getId().intValue())
                .moduleNo(po.getModuleNo())
                .datenew(po.getDateNew())
                .datelast(po.getDateLast());
            sentenceModules.add(sm);
        });
        return sentenceModules;
    }

}
