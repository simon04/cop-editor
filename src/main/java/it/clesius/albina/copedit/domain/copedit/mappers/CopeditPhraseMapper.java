package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.PhraseOptionItem;
import it.clesius.albina.copedit.domain.copedit.*;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static it.clesius.albina.copedit.config.Constants.*;

@Service
public class CopeditPhraseMapper {

    @Autowired
    CopeditCommonMapper copeditCommonMapper;

    /**
     * Converts phrase to PhraseOptionEditDTO DTO
     *
     * @param phraseOptions list of phraseOptions
     * @return the mapped object
     */
    public PhraseOptionEditDTO phraseToPhraseOptionEditDTO(List<PhraseOption> phraseOptions,
                                                           Map<String, List<Phrase>> phrasesMap,
                                                           Map<Long, Integer> phraseItemNoMap,
                                                           Map<String, List<PhraseOptionItem>> phrasesOptionsMap) {

        PhraseOptionEditDTO bean = new PhraseOptionEditDTO()
            .domain(new DomainDTO()
                .id(phraseOptions.get(0).getDomainId().longValue())
                .name(copeditCommonMapper.getDomainNameById(phraseOptions.get(0).getDomainId()))
            )
            .version(new VersionDTO()
                .version(phraseOptions.get(0).getVersion())
                .baseVersion(phraseOptions.get(0).getBasisVersion())
            );

        /* for each language set list of phrases */
        for (PhraseOption phraseOption : phraseOptions) {

            PhraseLanguageDTO languageSentence = new PhraseLanguageDTO()
                .language(phraseOption.getLanguage())
                .header(phraseOption.getHeader())
                .name(phraseOption.getName())
                .id(phraseOption.getId())
                .remark(phraseOption.getRemark())
                .datelast(phraseOption.getDatelast())
                .datenew(phraseOption.getDatenew());

            /* add language*/
            switch (phraseOption.getLanguage()) {
                case LANGUAGE_DE:
                    bean.setPhrasesDE(languageSentence);
                    bean.getPhrasesDE().setPhrases(phraseToPhraseOptionItemDTO(phrasesMap.get(LANGUAGE_DE), phrasesOptionsMap.get(LANGUAGE_DE)));
                    break;
                case LANGUAGE_IT:
                    bean.setPhrasesIT(languageSentence);
                    bean.getPhrasesIT().setPhrases(phraseToPhraseOptionItemDTO(phrasesMap.get(LANGUAGE_IT), phrasesOptionsMap.get(LANGUAGE_IT)));
                    break;
                case LANGUAGE_EN:
                    bean.setPhrasesEN(languageSentence);
                    bean.getPhrasesEN().setPhrases(phraseToPhraseOptionItemDTO(phrasesMap.get(LANGUAGE_EN), phrasesOptionsMap.get(LANGUAGE_EN)));
                    break;
                case LANGUAGE_FR:
                    bean.setPhrasesFR(languageSentence);
                    bean.getPhrasesFR().setPhrases(phraseToPhraseOptionItemDTO(phrasesMap.get(LANGUAGE_FR), phrasesOptionsMap.get(LANGUAGE_FR)));
                    break;
            }
        }
        return bean;
    }

    /**
     * Maps an instance of Phrase class to corresponding PhraseDTO. The itemNo is read from the map containing the value
     * contained in phrase_option_item table
     *
     * @param phraseList      list of phrases to map
     * @param phraseItemNoMap map of phraseId (key) and itemNo (value, comes from phrase_option_item table)
     * @return a list of mapped PhraseDTO
     */
    public List<PhraseDTO> phraseToPhraseDTO(List<Phrase> phraseList, Map<Long, Integer> phraseItemNoMap) {

        List<PhraseDTO> res = new ArrayList<>();
        for (Phrase ph : phraseList) {
            PhraseDTO phrase = new PhraseDTO()
                .id(ph.getId())
                .value(ph.getValue())
                .spaceBefore(ph.isSpaceBefore())
                .spaceAfter(ph.isSpaceAfter())
                .removePunctuationBefore(ph.isRemovePunctuationBefore())
                .itemPartNo(phraseItemNoMap.get(ph.getId()))
                .incorrect(ph.isIncorrect())
                .datenew(ph.getDatenew())
                .datelast(ph.getDatelast());
            res.add(phrase);
        }
        return res;
    }

    public List<PhraseOptionItemDTO> phraseToPhraseOptionItemDTO(List<Phrase> phrasesMap, List<PhraseOptionItem> phrasesOptionsMap) {

        List<PhraseOptionItemDTO> res = new ArrayList<>();
        phrasesOptionsMap.forEach(x -> {

            Phrase phrase = phrasesMap.stream().filter(ph ->
                ph.getPhraseOptionItemId().intValue() == x.getPhraseOptionItemId().intValue()
            ).findFirst().orElse(null);

            PhraseDTO phraseDto = new PhraseDTO()
                .id(phrase.getId())
                .datelast(phrase.getDatelast())
                .datenew(phrase.getDatenew())
                .incorrect(phrase.isIncorrect())
                .removePunctuationBefore(phrase.isRemovePunctuationBefore())
                .spaceAfter(phrase.isSpaceAfter())
                .spaceBefore(phrase.isSpaceBefore())
                .itemPartNo(x.getItemNo())
                .value(phrase.getValue());

            PhraseOptionItemDTO el = new PhraseOptionItemDTO()
                .basisVersion(x.getBasisVersion())
                .version(x.getVersion())
                .datelast(x.getDatelast())
                .datenew(x.getDatenew())
                .deleted(x.getDeleted())
                .itemNo(x.getItemNo())
                .phraseOptionId(x.getPhraseOptionId())
                .phraseOptionItemId(x.getPhraseOptionItemId())
                .phrase(phraseDto);
            res.add(el);
        });

        return res;
    }

    public List<PhraseOptionItem> phraseOptionItemsDTOToPhraseOptionItem(List<PhraseOptionItemDTO> dtos) {
        List<PhraseOptionItem> res = new ArrayList<>();
        dtos.stream().forEach(x -> {
            PhraseOptionItem poi = new PhraseOptionItem()
                .basisVersion(ObjectUtils.firstNonNull(x.getBasisVersion(), "1.0"))
                .version(ObjectUtils.firstNonNull(x.getVersion(), "1.0"))
                .datelast(ObjectUtils.firstNonNull(x.getDatelast(), ZonedDateTime.now()))
                .datenew(ObjectUtils.firstNonNull(x.getDatenew(), ZonedDateTime.now()))
                .deleted(x.getDeleted())
                .itemNo(x.getPhrase().getItemPartNo())
                .phraseOptionId(x.getPhraseOptionId())
                .phraseOptionItemId(x.getPhraseOptionItemId());
            res.add(poi);
        });

        return res;
    }

    public PhraseOption phraseLanguageToPhraseOption(PhraseLanguageDTO dto) {

        PhraseOption po = new PhraseOption()
            .name(dto.getName())
            .language(dto.getLanguage())
            .deleted(false)
            .remark(dto.getRemark())
            .datenew(dto.getDatenew())
            .datelast(dto.getDatelast())
            .header(dto.getHeader())
            .basisVersion("1.0")
            .version("1.0");
        po.setId(dto.getId());
        return po;
    }

    public List<Phrase> phraseDTOToPhraseList(List<PhraseOptionItemDTO> dtoList) {

        List<Phrase> res = new ArrayList<>();
        dtoList.forEach(x -> {

            PhraseDTO phrase = x.getPhrase();

            Phrase ph = new Phrase()
                .datelast(x.getDatelast())
                .datenew(x.getDatenew()).value(phrase.getValue())
                .incorrect(phrase.getIncorrect())
                .removePunctuationBefore(phrase.getRemovePunctuationBefore())
                .phraseOptionItemId(Optional.ofNullable(x.getPhraseOptionItemId()).map(Long::intValue).orElse(null))
                .spaceAfter(phrase.getSpaceAfter())
                .spaceBefore(phrase.getSpaceBefore())
                .itemNo(phrase.getItemPartNo());
            ph.setId(phrase.getId());
            res.add(ph);
        });
        return res;
    }

}
