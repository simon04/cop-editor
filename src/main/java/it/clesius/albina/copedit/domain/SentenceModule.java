package it.clesius.albina.copedit.domain;

import it.clesius.albina.copedit.domain.keys.SentenceModulePK;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A SentenceModule.
 */
@Entity
@Table(name = "sentenceModule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@IdClass(SentenceModulePK.class)
public class SentenceModule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "sentence_id")
    private Integer sentenceId;

    @Id
    @Column(name = "phrase_option_id")
    private Integer phraseOptionId;

    @Column(name = "module_no")
    private Integer moduleNo;

    @Column(name = "datenew")
    private ZonedDateTime datenew;

    @Column(name = "datelast")
    private ZonedDateTime datelast;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Integer getSentenceId() {
        return sentenceId;
    }

    public SentenceModule sentenceId(Integer sentenceId) {
        this.sentenceId = sentenceId;
        return this;
    }

    public void setSentenceId(Integer sentenceId) {
        this.sentenceId = sentenceId;
    }

    public Integer getPhraseOptionId() {
        return phraseOptionId;
    }

    public SentenceModule phraseOptionId(Integer phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
        return this;
    }

    public void setPhraseOptionId(Integer phraseOptionId) {
        this.phraseOptionId = phraseOptionId;
    }

    public Integer getModuleNo() {
        return moduleNo;
    }

    public SentenceModule moduleNo(Integer moduleNo) {
        this.moduleNo = moduleNo;
        return this;
    }

    public void setModuleNo(Integer moduleNo) {
        this.moduleNo = moduleNo;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public SentenceModule datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public SentenceModule datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public String toString() {
        return "SentenceModule{" +
            ", sentenceId=" + getSentenceId() +
            ", phraseOptionId=" + getPhraseOptionId() +
            ", moduleNo=" + getModuleNo() +
            ", datenew='" + getDatenew() + "'" +
            ", datelast='" + getDatelast() + "'" +
            "}";
    }
}
