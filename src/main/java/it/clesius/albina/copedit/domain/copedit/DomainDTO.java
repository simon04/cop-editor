package it.clesius.albina.copedit.domain.copedit;

public class DomainDTO {

    public DomainDTO() {
    }

    public DomainDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DomainDTO id(Long id) {
        this.id = id;
        return this;
    }

    public DomainDTO name(String name) {
        this.name = name;
        return this;
    }
}
