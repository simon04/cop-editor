package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class CopeditPhraseOptionMapper {

    public PhraseOptionDTO phraseOptionToDTO(PhraseOption po) {

        PhraseOptionDTO dto = new PhraseOptionDTO();
        dto.setId(po.getId());
        dto.name(po.getName());
        dto.language(po.getLanguage());
        dto.header(po.getHeader());
        dto.version(po.getVersion());
        dto.basisVersion(po.getBasisVersion());
        dto.remark(po.getRemark());
        dto.deletd(po.isDeleted());
        dto.domainId(po.getDomainId());
        dto.dateNew(po.getDatenew());
        dto.dateLast(po.getDatelast());
        return dto;
    }

    /*public PhraseOptionComboDTO phraseOptionToComboDTO(PhraseOption po) {

        PhraseOptionComboDTO dto = new PhraseOptionComboDTO();
        dto.label(po.getName())
            .value(phraseOptionToDTO(po));
        return dto;
    }*/

    public List<PhraseOptionDTO> phraseOptionListToDTO(List<PhraseOption> po, Map<Long, Integer> moduleNoMap) {

        List<PhraseOptionDTO> list = new ArrayList<>();
        po.forEach(x -> {
            PhraseOptionDTO dto = phraseOptionToDTO(x);
            dto.setModuleNo(moduleNoMap.get(x.getId()));
            list.add(dto);
        });
        return list;
    }

    /*public List<PhraseOptionComboDTO> phraseOptionListToComboDTO(List<PhraseOption> po, Map<Long, Integer> moduleNoMap) {

        List<PhraseOptionComboDTO> list = new ArrayList<>();
        po.forEach(x -> {
            PhraseOptionComboDTO dto = phraseOptionToComboDTO(x);
            dto.getValue().setModuleNo(moduleNoMap.get(x.getId()));
            list.add(dto);
        });
        return list;
    }*/

    public List<PhraseOptionDTO> phraseOptionListToDTO(List<PhraseOption> po) {

        List<PhraseOptionDTO> list = new ArrayList<>();
        po.forEach(x -> {
            PhraseOptionDTO dto = phraseOptionToDTO(x);
            list.add(dto);
        });
        return list;
    }

    public List<PhraseOption> phraseOptionsDTOToPhraseOptions(List<PhraseOptionDTO> dtos) {
        List<PhraseOption> res = new ArrayList<>();
        dtos.forEach(x -> res.add(phraseOptionDTOToPhraseOption(x)));
        return res;
    }

    public PhraseOption phraseOptionDTOToPhraseOption(PhraseOptionDTO dto) {

        PhraseOption po = new PhraseOption();
        po.setId(dto.getId());
        po.name(dto.getName())
            .language(dto.getLanguage())
            .header(dto.getHeader())
            .version(dto.getVersion())
            .basisVersion(dto.getBasisVersion())
            .remark(dto.getRemark())
            .deleted(dto.getDeleted())
            .domainId(dto.getDomainId())
            .datenew(dto.getDateNew())
            .datelast(dto.getDateLast());
        return po;
    }
}
