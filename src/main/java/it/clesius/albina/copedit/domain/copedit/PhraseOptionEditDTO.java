package it.clesius.albina.copedit.domain.copedit;

import java.util.List;

public class PhraseOptionEditDTO {

    DomainDTO domain;
    VersionDTO version;
    PhraseLanguageDTO phrasesDE;
    PhraseLanguageDTO phrasesIT;
    PhraseLanguageDTO phrasesEN;
    PhraseLanguageDTO phrasesFR;
    List<SentenceLanguage> sentenceList;

    public void setSentenceList(List<SentenceLanguage>  sentenceList) {
        this.sentenceList = sentenceList;
    }

    public List<SentenceLanguage>  getSentenceList() {
        return sentenceList;
    }

    public DomainDTO getDomain() {
        return domain;
    }

    public void setDomain(DomainDTO domain) {
        this.domain = domain;
    }

    public VersionDTO getVersion() {
        return version;
    }

    public void setVersion(VersionDTO version) {
        this.version = version;
    }

    public PhraseLanguageDTO getPhrasesDE() {
        return phrasesDE;
    }

    public void setPhrasesDE(PhraseLanguageDTO phrasesDE) {
        this.phrasesDE = phrasesDE;
    }

    public PhraseLanguageDTO getPhrasesIT() {
        return phrasesIT;
    }

    public void setPhrasesIT(PhraseLanguageDTO phrasesIT) {
        this.phrasesIT = phrasesIT;
    }

    public PhraseLanguageDTO getPhrasesEN() {
        return phrasesEN;
    }

    public void setPhrasesEN(PhraseLanguageDTO phrasesEN) {
        this.phrasesEN = phrasesEN;
    }

    public PhraseLanguageDTO getPhrasesFR() {
        return phrasesFR;
    }

    public void setPhrasesFR(PhraseLanguageDTO phrasesFR) {
        this.phrasesFR = phrasesFR;
    }

    public PhraseOptionEditDTO domain(DomainDTO domain) {
        this.domain = domain;
        return this;
    }

    public PhraseOptionEditDTO version(VersionDTO version) {
        this.version = version;
        return this;
    }

    public PhraseOptionEditDTO phrasesDE(PhraseLanguageDTO phrasesDE) {
        this.phrasesDE = phrasesDE;
        return this;
    }

    public PhraseOptionEditDTO phrasesIT(PhraseLanguageDTO phrasesIT) {
        this.phrasesIT = phrasesIT;
        return this;
    }

    public PhraseOptionEditDTO phrasesEN(PhraseLanguageDTO phrasesEN) {
        this.phrasesEN = phrasesEN;
        return this;
    }

    public PhraseOptionEditDTO phrasesFR(PhraseLanguageDTO phrasesFR) {
        this.phrasesFR = phrasesFR;
        return this;
    }
}
