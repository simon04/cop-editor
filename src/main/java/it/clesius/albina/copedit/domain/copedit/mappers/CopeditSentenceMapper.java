package it.clesius.albina.copedit.domain.copedit.mappers;

import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.domain.copedit.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CopeditSentenceMapper {

    @Autowired
    CopeditCommonMapper copeditCommonMapper;

    public SentenceDTO sentenceDTOMapper(List<Sentence> sentence, Map<String, List<PhraseOptionDTO>> phraseOptionsMap) {
        SentenceDTO bean = new SentenceDTO();
        bean.setDomain(new DomainDTO()
                .id(sentence.get(0).getDomainId().longValue())
                .name(copeditCommonMapper.getDomainNameById(sentence.get(0).getDomainId()))
            //.datenew(sentence.get(0).getDatenew())
            //.datelast(sentence.get(0).getDatelast())
        );
        bean.deleted(sentence.get(0).isDeleted());
        bean.jokerSentence(sentence.get(0).isJokerSentence());
        bean.setVersion(new VersionDTO()
            .version(sentence.get(0).getVersion())
            .baseVersion(sentence.get(0).getBasisVersion())
        );
        for (Sentence s : sentence) {

            SentenceLanguage languageSentence = new SentenceLanguage();
            languageSentence.setName(s.getName());
            languageSentence.setHeader(s.getHeader());
            languageSentence.setLanguage(s.getLanguage());
            languageSentence.setStructure(s.getStructure());
            languageSentence.setRemark(s.getRemark());
            languageSentence.id(s.getId());
            languageSentence.updatedDate(s.getDatelast());
            languageSentence.createdDate(s.getDatenew());

            /* add language*/
            switch (s.getLanguage()) {
                case "de":
                    bean.setSentencesDE(languageSentence);
                    bean.getSentencesDE().phraseOptions(phraseOptionsMap.get("de"));
                    break;
                case "it":
                    bean.setSentencesIT(languageSentence);
                    bean.getSentencesIT().phraseOptions(phraseOptionsMap.get("it"));
                    break;
                case "en":
                    bean.setSentencesEN(languageSentence);
                    bean.getSentencesEN().phraseOptions(phraseOptionsMap.get("en"));
                    break;
                case "fr":
                    bean.setSentencesFR(languageSentence);
                    bean.getSentencesFR().phraseOptions(phraseOptionsMap.get("fr"));
                    break;
            }
        }
        reorderPhraseOptions(bean.getSentencesDE());
        reorderPhraseOptions(bean.getSentencesIT());
        reorderPhraseOptions(bean.getSentencesFR());
        reorderPhraseOptions(bean.getSentencesEN());

        return bean;
    }

    public List<Sentence> sentenceDTOToSentence(SentenceDTO dto) {

        List<Sentence> sentences = new ArrayList<>();
        Sentence de = sentenceLanguageToSentence(dto.getSentencesDE(), dto);
        Sentence it = sentenceLanguageToSentence(dto.getSentencesIT(), dto);
        Sentence en = sentenceLanguageToSentence(dto.getSentencesEN(), dto);
        Sentence fr = sentenceLanguageToSentence(dto.getSentencesFR(), dto);
        sentences.addAll(Arrays.asList(de, it, en, fr));
        return sentences;
    }

    public Sentence sentenceLanguageToSentence(SentenceLanguage sl, SentenceDTO dto) {
        Sentence s = new Sentence();
        s.setVersion(dto.getVersion().getVersion());
        s.setBasisVersion(dto.getVersion().getBaseVersion());
        s.setDomainId(dto.getDomain().getId().intValue());
        s.setDeleted(dto.getDeleted());
        s.setJokerSentence(Optional.ofNullable(dto.getJokerSentence()).orElse(false));
        s.header(sl.getHeader());
        s.name(sl.getName());
        s.structure(sl.getStructure());
        s.language(sl.getLanguage());
        s.setId(sl.getId());
        s.datenew(sl.getCreatedDate());
        s.datelast(sl.getUpdatedDate());
        s.remark(sl.getRemark());
        return s;
    }

    /**
     * Reorder the list of the phraseOptions based on the sentence structure
     *
     * @param sl sentence to reorder
     */
    public void reorderPhraseOptions(SentenceLanguage sl) {

        List<PhraseOptionDTO> reorderedPhraseOptions = new ArrayList<>();
        List<String> splittedElements = Arrays.asList(sl.getStructure().split(","));
        splittedElements.forEach(element -> {

            List<String> el = Arrays.asList(element.split("\\."));
            Integer index = Integer.parseInt(el.get(0));
            PhraseOptionDTO po = sl.getPhraseOptions().get(index - 1);

            PhraseOptionDTO clone = (PhraseOptionDTO) po.clone();
            Integer itemNo = Integer.parseInt(el.get(1));
            if (itemNo == 2) {
                clone.setItemNo(2);
            } else {
                clone.setItemNo(1);
            }
            reorderedPhraseOptions.add(clone);

        });
        sl.setPhraseOptions(reorderedPhraseOptions);

    }

}
