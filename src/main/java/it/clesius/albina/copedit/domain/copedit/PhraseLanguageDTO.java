package it.clesius.albina.copedit.domain.copedit;

import java.time.ZonedDateTime;
import java.util.List;

public class PhraseLanguageDTO {

    Long id;
    String language;
    String name;
    String header;
    String remark;
    ZonedDateTime datelast;
    ZonedDateTime datenew;
    List<PhraseOptionItemDTO> phrases;
    List<PhraseOptionItemDTO> phrasesItemPartNo2;

    public List<PhraseOptionItemDTO> getPhrasesItemPartNo2() {
        return phrasesItemPartNo2;
    }

    public void setPhrasesItemPartNo2(List<PhraseOptionItemDTO> phrasesItemPartNo2) {
        this.phrasesItemPartNo2 = phrasesItemPartNo2;
    }

    public Long getId() {
        return id;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public ZonedDateTime getDatelast() {
        return datelast;
    }

    public void setDatelast(ZonedDateTime datelast) {
        this.datelast = datelast;
    }

    public ZonedDateTime getDatenew() {
        return datenew;
    }

    public void setDatenew(ZonedDateTime datenew) {
        this.datenew = datenew;
    }

    public List<PhraseOptionItemDTO> getPhrases() {
        return phrases;
    }

    public void setPhrases(List<PhraseOptionItemDTO> phrases) {
        this.phrases = phrases;
    }

    public PhraseLanguageDTO id(Long id) {
        this.id = id;
        return this;
    }

    public PhraseLanguageDTO language(String language) {
        this.language = language;
        return this;
    }

    public PhraseLanguageDTO name(String name) {
        this.name = name;
        return this;
    }

    public PhraseLanguageDTO header(String header) {
        this.header = header;
        return this;
    }

    public PhraseLanguageDTO datelast(ZonedDateTime datelast) {
        this.datelast = datelast;
        return this;
    }

    public PhraseLanguageDTO datenew(ZonedDateTime datenew) {
        this.datenew = datenew;
        return this;
    }

    public PhraseLanguageDTO phrases(List<PhraseOptionItemDTO> phrases) {
        this.phrases = phrases;
        return this;
    }

    public PhraseLanguageDTO remark(String remark) {
        this.remark = remark;
        return this;
    }

    public PhraseLanguageDTO phrasesItemPartNo2(List<PhraseOptionItemDTO> phrasesItemPartNo2) {
        this.phrasesItemPartNo2 = phrasesItemPartNo2;
        return this;
    }
}
