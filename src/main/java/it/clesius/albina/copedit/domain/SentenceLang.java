package it.clesius.albina.copedit.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import it.clesius.albina.copedit.domain.enumeration.LanguageEnum;

/**
 * A SentenceLang.
 */
@Entity
@Table(name = "sentence_lang")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SentenceLang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "jhi_header")
    private String header;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private LanguageEnum language;

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;

    @Column(name = "jhi_structure")
    private String structure;

    @Column(name = "remark")
    private String remark;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SentenceLang name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public SentenceLang header(String header) {
        this.header = header;
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public LanguageEnum getLanguage() {
        return language;
    }

    public SentenceLang language(LanguageEnum language) {
        this.language = language;
        return this;
    }

    public void setLanguage(LanguageEnum language) {
        this.language = language;
    }

    public ZonedDateTime getUpdatedDate() {
        return updatedDate;
    }

    public SentenceLang updatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(ZonedDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStructure() {
        return structure;
    }

    public SentenceLang structure(String structure) {
        this.structure = structure;
        return this;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public String getRemark() {
        return remark;
    }

    public SentenceLang remark(String remark) {
        this.remark = remark;
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SentenceLang sentenceLang = (SentenceLang) o;
        if (sentenceLang.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sentenceLang.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SentenceLang{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", header='" + getHeader() + "'" +
            ", language='" + getLanguage() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", structure='" + getStructure() + "'" +
            ", remark='" + getRemark() + "'" +
            "}";
    }
}
