package it.clesius.albina.copedit.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.service.SentenceService;
import it.clesius.albina.copedit.web.rest.errors.BadRequestAlertException;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import it.clesius.albina.copedit.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Sentence.
 */
@RestController
@RequestMapping("/api")
public class SentenceResource {

    private final Logger log = LoggerFactory.getLogger(SentenceResource.class);

    private static final String ENTITY_NAME = "sentence";

    private final SentenceService sentenceService;

    public SentenceResource(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    /**
     * POST  /sentences : Create a new sentence.
     *
     * @param sentence the sentence to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sentence, or with status 400 (Bad Request) if the sentence has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sentences")
    @Timed
    public ResponseEntity<Sentence> createSentence(@RequestBody Sentence sentence) throws URISyntaxException {
        log.debug("REST request to save Sentence : {}", sentence);
        if (sentence.getId() != null) {
            throw new BadRequestAlertException("A new sentence cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sentence result = sentenceService.save(sentence);
        return ResponseEntity.created(new URI("/api/sentences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sentences : Updates an existing sentence.
     *
     * @param sentence the sentence to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sentence,
     * or with status 400 (Bad Request) if the sentence is not valid,
     * or with status 500 (Internal Server Error) if the sentence couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sentences")
    @Timed
    public ResponseEntity<Sentence> updateSentence(@RequestBody Sentence sentence) throws URISyntaxException {
        log.debug("REST request to update Sentence : {}", sentence);
        if (sentence.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Sentence result = sentenceService.save(sentence);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sentence.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sentences : get all the sentences.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sentences in body
     */
    @GetMapping("/sentences")
    @Timed
    public ResponseEntity<List<Sentence>> getAllSentences(Pageable pageable) {
        log.debug("REST request to get a page of Sentences");
        Page<Sentence> page = sentenceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sentences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sentences/:id : get the "id" sentence.
     *
     * @param id the id of the sentence to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sentence, or with status 404 (Not Found)
     */
    @GetMapping("/sentences/{id}")
    @Timed
    public ResponseEntity<Sentence> getSentence(@PathVariable Long id) {
        log.debug("REST request to get Sentence : {}", id);
        Optional<Sentence> sentence = sentenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sentence);
    }

    /**
     * DELETE  /sentences/:id : delete the "id" sentence.
     *
     * @param id the id of the sentence to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sentences/{id}")
    @Timed
    public ResponseEntity<Void> deleteSentence(@PathVariable Long id) {
        log.debug("REST request to delete Sentence : {}", id);
        sentenceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
