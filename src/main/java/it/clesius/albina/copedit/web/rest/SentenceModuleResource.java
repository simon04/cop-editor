package it.clesius.albina.copedit.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import it.clesius.albina.copedit.domain.SentenceModule;
import it.clesius.albina.copedit.service.SentenceModuleService;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SentenceModule.
 */
@RestController
@RequestMapping("/api")
public class SentenceModuleResource {

    private final Logger log = LoggerFactory.getLogger(SentenceModuleResource.class);

    private static final String ENTITY_NAME = "sentenceModule";

    private final SentenceModuleService sentenceModuleService;

    public SentenceModuleResource(SentenceModuleService sentenceModuleService) {
        this.sentenceModuleService = sentenceModuleService;
    }

    /**
     * POST  /sentence-modules : Create a new sentenceModule.
     *
     * @param sentenceModule the sentenceModule to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sentenceModule, or with status 400 (Bad Request) if the sentenceModule has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sentence-modules")
    @Timed
    public ResponseEntity<SentenceModule> createSentenceModule(@RequestBody SentenceModule sentenceModule) throws URISyntaxException {
        return null;
    }

    /**
     * PUT  /sentence-modules : Updates an existing sentenceModule.
     *
     * @param sentenceModule the sentenceModule to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sentenceModule,
     * or with status 400 (Bad Request) if the sentenceModule is not valid,
     * or with status 500 (Internal Server Error) if the sentenceModule couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sentence-modules")
    @Timed
    public ResponseEntity<SentenceModule> updateSentenceModule(@RequestBody SentenceModule sentenceModule) throws URISyntaxException {
        return null;
    }

    /**
     * GET  /sentence-modules : get all the sentenceModules.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sentenceModules in body
     */
    @GetMapping("/sentence-modules")
    @Timed
    public List<SentenceModule> getAllSentenceModules() {
        log.debug("REST request to get all SentenceModules");
        return sentenceModuleService.findAll();
    }

    /**
     * GET  /sentence-modules/:id : get the "id" sentenceModule.
     *
     * @param id the id of the sentenceModule to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sentenceModule, or with status 404 (Not Found)
     */
    @GetMapping("/sentence-modules/{id}")
    @Timed
    public ResponseEntity<SentenceModule> getSentenceModule(@PathVariable Long id) {
        log.debug("REST request to get SentenceModule : {}", id);
        Optional<SentenceModule> sentenceModule = sentenceModuleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sentenceModule);
    }

    /**
     * DELETE  /sentence-modules/:id : delete the "id" sentenceModule.
     *
     * @param id the id of the sentenceModule to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sentence-modules/{id}")
    @Timed
    public ResponseEntity<Void> deleteSentenceModule(@PathVariable Long id) {
        log.debug("REST request to delete SentenceModule : {}", id);
        sentenceModuleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
