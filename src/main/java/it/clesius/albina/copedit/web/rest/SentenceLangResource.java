package it.clesius.albina.copedit.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.SentenceLang;
import it.clesius.albina.copedit.service.SentenceLangService;
import it.clesius.albina.copedit.web.rest.errors.BadRequestAlertException;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SentenceLang.
 */
@RestController
@RequestMapping("/api")
public class SentenceLangResource {

    private final Logger log = LoggerFactory.getLogger(SentenceLangResource.class);

    private static final String ENTITY_NAME = "sentenceLang";

    private final SentenceLangService sentenceLangService;

    public SentenceLangResource(SentenceLangService sentenceLangService) {
        this.sentenceLangService = sentenceLangService;
    }

    /**
     * POST  /sentence-langs : Create a new sentenceLang.
     *
     * @param sentenceLang the sentenceLang to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sentenceLang, or with status 400 (Bad Request) if the sentenceLang has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sentence-langs")
    @Timed
    public ResponseEntity<SentenceLang> createSentenceLang(@RequestBody SentenceLang sentenceLang) throws URISyntaxException {
        log.debug("REST request to save SentenceLang : {}", sentenceLang);
        if (sentenceLang.getId() != null) {
            throw new BadRequestAlertException("A new sentenceLang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SentenceLang result = sentenceLangService.save(sentenceLang);
        return ResponseEntity.created(new URI("/api/sentence-langs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sentence-langs : Updates an existing sentenceLang.
     *
     * @param sentenceLang the sentenceLang to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sentenceLang,
     * or with status 400 (Bad Request) if the sentenceLang is not valid,
     * or with status 500 (Internal Server Error) if the sentenceLang couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sentence-langs")
    @Timed
    public ResponseEntity<SentenceLang> updateSentenceLang(@RequestBody SentenceLang sentenceLang) throws URISyntaxException {
        log.debug("REST request to update SentenceLang : {}", sentenceLang);
        if (sentenceLang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SentenceLang result = sentenceLangService.save(sentenceLang);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sentenceLang.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sentence-langs : get all the sentenceLangs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sentenceLangs in body
     */
    @GetMapping("/sentence-langs")
    @Timed
    public List<SentenceLang> getAllSentenceLangs() {
        log.debug("REST request to get all SentenceLangs");
        return sentenceLangService.findAll();
    }

    /**
     * GET  /sentence-langs/:id : get the "id" sentenceLang.
     *
     * @param id the id of the sentenceLang to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sentenceLang, or with status 404 (Not Found)
     */
    @GetMapping("/sentence-langs/{id}")
    @Timed
    public ResponseEntity<SentenceLang> getSentenceLang(@PathVariable Long id) {
        log.debug("REST request to get SentenceLang : {}", id);
        Optional<SentenceLang> sentenceLang = sentenceLangService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sentenceLang);
    }

    /**
     * DELETE  /sentence-langs/:id : delete the "id" sentenceLang.
     *
     * @param id the id of the sentenceLang to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sentence-langs/{id}")
    @Timed
    public ResponseEntity<Void> deleteSentenceLang(@PathVariable Long id) {
        log.debug("REST request to delete SentenceLang : {}", id);
        sentenceLangService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
