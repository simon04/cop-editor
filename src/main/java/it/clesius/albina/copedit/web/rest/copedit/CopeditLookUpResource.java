package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.copedit.DomainDTO;
import it.clesius.albina.copedit.domain.copedit.VersionDTO;
import it.clesius.albina.copedit.repository.custom.CopEditVersionRepository;
import it.clesius.albina.copedit.repository.custom.CopeditDomainRepository;
import it.clesius.albina.copedit.web.rest.VersionResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/copedit/lookup")
public class CopeditLookUpResource {


    private final Logger log = LoggerFactory.getLogger(VersionResource.class);


    @Autowired
    CopEditVersionRepository copEditVersionRepository;
    @Autowired
    CopeditDomainRepository copeditDomainRepository;

    /**
     * GET  /versions : get all the versions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of versions in body
     */
    @GetMapping("/versions")
    @Timed
    public List<VersionDTO> getAllVersions() {
        log.debug("REST request to get all Versions");
        return copEditVersionRepository.findAll();
    }

    /**
     * GET  /versions : get all the versions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of versions in body
     */
    @GetMapping("/domains")
    @Timed
    public List<DomainDTO> getAllDomains() {
        log.debug("REST request to get all Versions");
        return copeditDomainRepository.findAll();
    }
}
