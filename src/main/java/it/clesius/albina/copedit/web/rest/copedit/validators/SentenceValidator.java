package it.clesius.albina.copedit.web.rest.copedit.validators;

import it.clesius.albina.copedit.domain.copedit.SentenceDTO;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SentenceValidator {

    @Autowired
    EntityManager entityManager;

    /**
     * Checks whether a sentence with same header already exists or not
     *
     * @param sentence
     * @return true if the sentence already exists, else false
     */
    public Boolean sentenceAlreadyExists(SentenceDTO sentence) {

        List<String> sentencesID = new ArrayList<>();
        /* if already existing sentence, get id of every sentence for duplicate check */
        if (sentence.getSentencesDE().getId() != null) {
            sentencesID = Arrays.asList(
                sentence.getSentencesDE().getId().toString(),
                sentence.getSentencesIT().getId().toString(),
                sentence.getSentencesEN().getId().toString(),
                sentence.getSentencesFR().getId().toString());
        }

        String name = sentence.getSentencesDE().getName();
        String query = String.format("SELECT * FROM SENTENCE WHERE name = '%s'", name);
        if (sentencesID.size() > 0) {
            query += String.format(" AND DOMAIN_ID = %d AND SENTENCE_ID NOT IN (%s)", sentence.getDomain().getId(), String.join(",", sentencesID));
        }

        List results = entityManager.createNativeQuery(query).getResultList();
        return !results.isEmpty();
    }

    /**
     * Check whether all required fields in sentence are present or not
     *
     * @param sentence sentence to check
     * @return true if sentence required fields are present, else false
     */
    public Boolean sentenceIsComplete(SentenceDTO sentence) {

        /* name must be set */
        Boolean allNames = BooleanUtils.and(new Boolean[]{
            !StringUtils.isEmpty(sentence.getSentencesDE().getName()),
            !StringUtils.isEmpty(sentence.getSentencesIT().getName()),
            !StringUtils.isEmpty(sentence.getSentencesEN().getName()),
            !StringUtils.isEmpty(sentence.getSentencesFR().getName())}
        );

        /* all headers are required */
        Boolean allHeaders = BooleanUtils.and(new Boolean[]{
            !StringUtils.isEmpty(sentence.getSentencesDE().getHeader()),
            !StringUtils.isEmpty(sentence.getSentencesIT().getHeader()),
            !StringUtils.isEmpty(sentence.getSentencesEN().getHeader()),
            !StringUtils.isEmpty(sentence.getSentencesFR().getHeader())}
        );

        /* at least one phraseOptions per language is required */
        Boolean phraseOptions = BooleanUtils.and(new Boolean[]{
                sentence.getSentencesDE().getPhraseOptions().size() > 0,
                sentence.getSentencesIT().getPhraseOptions().size() > 0,
                sentence.getSentencesEN().getPhraseOptions().size() > 0,
                sentence.getSentencesFR().getPhraseOptions().size() > 0
            }
        );

        return allNames && allHeaders && phraseOptions;
    }

}
