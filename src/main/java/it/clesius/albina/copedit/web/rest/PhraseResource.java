package it.clesius.albina.copedit.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.service.PhraseService;
import it.clesius.albina.copedit.web.rest.errors.BadRequestAlertException;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import it.clesius.albina.copedit.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Phrase.
 */
@RestController
@RequestMapping("/api")
public class PhraseResource {

    private final Logger log = LoggerFactory.getLogger(PhraseResource.class);

    private static final String ENTITY_NAME = "phrase";

    private final PhraseService phraseService;

    public PhraseResource(PhraseService phraseService) {
        this.phraseService = phraseService;
    }

    /**
     * POST  /phrases : Create a new phrase.
     *
     * @param phrase the phrase to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phrase, or with status 400 (Bad Request) if the phrase has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phrases")
    @Timed
    public ResponseEntity<Phrase> createPhrase(@RequestBody Phrase phrase) throws URISyntaxException {
        log.debug("REST request to save Phrase : {}", phrase);
        if (phrase.getId() != null) {
            throw new BadRequestAlertException("A new phrase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Phrase result = phraseService.save(phrase);
        return ResponseEntity.created(new URI("/api/phrases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /phrases : Updates an existing phrase.
     *
     * @param phrase the phrase to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phrase,
     * or with status 400 (Bad Request) if the phrase is not valid,
     * or with status 500 (Internal Server Error) if the phrase couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phrases")
    @Timed
    public ResponseEntity<Phrase> updatePhrase(@RequestBody Phrase phrase) throws URISyntaxException {
        log.debug("REST request to update Phrase : {}", phrase);
        if (phrase.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Phrase result = phraseService.save(phrase);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, phrase.getId().toString()))
            .body(result);
    }

    /**
     * GET  /phrases : get all the phrases.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of phrases in body
     */
    @GetMapping("/phrases")
    @Timed
    public ResponseEntity<List<Phrase>> getAllPhrases(Pageable pageable) {
        log.debug("REST request to get a page of Phrases");
        Page<Phrase> page = phraseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/phrases");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /phrases/:id : get the "id" phrase.
     *
     * @param id the id of the phrase to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phrase, or with status 404 (Not Found)
     */
    @GetMapping("/phrases/{id}")
    @Timed
    public ResponseEntity<Phrase> getPhrase(@PathVariable Long id) {
        log.debug("REST request to get Phrase : {}", id);
        Optional<Phrase> phrase = phraseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phrase);
    }

    /**
     * DELETE  /phrases/:id : delete the "id" phrase.
     *
     * @param id the id of the phrase to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phrases/{id}")
    @Timed
    public ResponseEntity<Void> deletePhrase(@PathVariable Long id) {
        log.debug("REST request to delete Phrase : {}", id);
        phraseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
