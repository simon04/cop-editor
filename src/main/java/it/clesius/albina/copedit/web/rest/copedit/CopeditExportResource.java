package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.service.copedit.CopeditExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/copedit")
public class CopeditExportResource {

    @Autowired
    CopeditExportService copeditExportService;
    @Autowired
    ApplicationProperties applicationProperties;


    private final String ATTACHMENT_FILENAME = "attachment; filename=\"%s\"";

    @GetMapping(value = "/export", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Timed
    public ResponseEntity<byte[]> exportDomain(@RequestParam Integer domainId) throws IOException {

        try {
            byte[] zipFile = copeditExportService.exportDomain(domainId);
            String fileName = copeditExportService.getExportZipFileName(domainId);
            return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format(ATTACHMENT_FILENAME, fileName))
                .contentType(MediaType.parseMediaType("application/zip"))
                .body(zipFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }
}
