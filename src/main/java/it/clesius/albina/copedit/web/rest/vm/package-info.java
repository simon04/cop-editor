/**
 * View Models used by Spring MVC REST controllers.
 */
package it.clesius.albina.copedit.web.rest.vm;
