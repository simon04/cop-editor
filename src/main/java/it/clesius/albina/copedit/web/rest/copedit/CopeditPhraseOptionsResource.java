package it.clesius.albina.copedit.web.rest.copedit;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.domain.PhraseOption;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionDTO;
import it.clesius.albina.copedit.domain.copedit.PhraseOptionListItemDTO;
import it.clesius.albina.copedit.repository.custom.CopeditPhraseOptionRepository;
import it.clesius.albina.copedit.service.PhraseService;
import it.clesius.albina.copedit.web.rest.PhraseResource;
import it.clesius.albina.copedit.web.rest.util.HeaderUtil;
import it.clesius.albina.copedit.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Service
@RequestMapping("/api/copedit")
public class CopeditPhraseOptionsResource {

    private final Logger log = LoggerFactory.getLogger(PhraseResource.class);

    private static final String ENTITY_NAME = "phrase";
    @Autowired
    CopeditPhraseOptionRepository copeditPhraseOptionRepository;

    private final PhraseService phraseService;

    public CopeditPhraseOptionsResource(PhraseService phraseService) {
        this.phraseService = phraseService;
    }


    /**
     * POST  /phrases : Create a new phrase.
     *
     * @param phrase the phrase to create
     * @return the ResponseEntity with status 201 (Created) and with body the new phrase, or with status 400 (Bad Request) if the phrase has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/phraseOptions")
    @Timed
    public ResponseEntity<Phrase> createPhrase(@RequestBody Phrase phrase) throws URISyntaxException {
        return null;
    }

    /**
     * PUT  /phrases : Updates an existing phrase.
     *
     * @param phrase the phrase to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated phrase,
     * or with status 400 (Bad Request) if the phrase is not valid,
     * or with status 500 (Internal Server Error) if the phrase couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/phraseOptions")
    @Timed
    public ResponseEntity<Phrase> updatePhrase(@RequestBody Phrase phrase) throws URISyntaxException {
        return null;
    }

    /**
     * GET  /phrases : get all the phrases.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of phrases in body
     */
    @GetMapping("/phraseOptionsList")
    @Timed
    public ResponseEntity<List<PhraseOption>> getAllPhrases() {
        log.debug("REST request to get a page of Phrases");
        List<PhraseOption> page = copeditPhraseOptionRepository.findAll();
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @GetMapping("/phraseOptions")
    @Timed
    public ResponseEntity<List<PhraseOptionListItemDTO>> getAllPhrases(Pageable pageable,
                                                                       @RequestParam(value = "deleted", required = false) Boolean deleted,
                                                                       @RequestParam(value = "search", required = false) String search,
                                                                       @RequestParam(value = "domain", required = false) Integer domainId,
                                                                       @RequestParam(value = "version", required = false) String versionId) {
        log.debug("REST request to get a page of Phrases");
        Page<PhraseOptionListItemDTO> page = copeditPhraseOptionRepository.findAll(pageable, deleted, search, domainId, versionId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/copedit/phrases");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /phrases/:id : get the "id" phrase.
     *
     * @param id the id of the phrase to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the phrase, or with status 404 (Not Found)
     */
    @GetMapping("/phraseOptions/{id}")
    @Timed
    public ResponseEntity<Phrase> getPhrase(@PathVariable Long id) {
       /* log.debug("REST request to get Phrase : {}", id);
        Optional<Phrase> phrase = phraseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(phrase);*/
        return null;
    }

    /**
     * DELETE  /phrases/:id : delete the "id" phrase.
     *
     * @param id the id of the phrase to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/phraseOptions/{id}")
    @Timed
    public ResponseEntity<Void> deletePhrase(@PathVariable Long id) {
        log.debug("REST request to delete Phrase : {}", id);
        copeditPhraseOptionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/phrasesOptionsLang")
    @Timed
    public ResponseEntity<List<PhraseOptionDTO>> getPhraseOptionsByLang(@RequestParam(value = "lang", required = false) String lang,
                                                                        @RequestParam(value = "domain", required = false, defaultValue = "1") Integer domain) {
        List<PhraseOptionDTO> res = copeditPhraseOptionRepository.findAllByLanguage(lang, domain);
        return ResponseUtil.wrapOrNotFound(Optional.of(res));
    }
}
