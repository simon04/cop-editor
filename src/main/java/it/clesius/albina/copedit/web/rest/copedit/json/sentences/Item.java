
package it.clesius.albina.copedit.web.rest.copedit.json.sentences;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "dbid",
    "rgn",
    "name",
    "header",
    "options",
    "joker"
})
public class Item {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("dbid")
    private Integer dbid;
    @JsonProperty("rgn")
    private Object rgn;
    @JsonProperty("name")
    private String name;
    @JsonProperty("header")
    private String header;
    @JsonProperty("options")
    private String options;
    @JsonProperty("joker")
    private Boolean joker;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("dbid")
    public Integer getDbid() {
        return dbid;
    }

    @JsonProperty("dbid")
    public void setDbid(Integer dbid) {
        this.dbid = dbid;
    }

    @JsonProperty("rgn")
    public Object getRgn() {
        return rgn;
    }

    @JsonProperty("rgn")
    public void setRgn(Object rgn) {
        this.rgn = rgn;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    @JsonProperty("options")
    public String getOptions() {
        return options;
    }

    @JsonProperty("options")
    public void setOptions(String options) {
        this.options = options;
    }

    @JsonProperty("joker")
    public Boolean getJoker() {
        return joker;
    }

    @JsonProperty("joker")
    public void setJoker(Boolean joker) {
        this.joker = joker;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
