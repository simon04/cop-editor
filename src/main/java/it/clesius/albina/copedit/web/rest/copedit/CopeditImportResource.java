package it.clesius.albina.copedit.web.rest.copedit;

import it.clesius.albina.copedit.config.ApplicationProperties;
import it.clesius.albina.copedit.service.copedit.ImportFilesService;
import it.clesius.albina.copedit.web.rest.errors.FieldErrorVM;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/copedit")
public class CopeditImportResource {

    private final Logger log = LoggerFactory.getLogger(CopeditImportResource.class);

    @Autowired
    ApplicationProperties applicationProperties;
    @Autowired
    ImportFilesService importFilesService;

    /**
     * POST  /import : Import a zip file.
     *
     * @param files files array to import
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<?> createPhrase(@RequestPart(value = "files[]", required = true) MultipartFile[] files,
                                          @RequestParam(value = "domain", required = true) String domainName) {

        List<MultipartFile> fileList = Arrays.asList(files);

        if (files.length != 4) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "The number of files to upload must be EXACTLY 4"));
        }

        /* check file names */
        Boolean isDePresent = fileList.stream().filter(x -> x.getOriginalFilename().equals("de.zip")).count() == 1L;
        Boolean isFrPresent = fileList.stream().filter(x -> x.getOriginalFilename().equals("fr.zip")).count() == 1L;
        Boolean isEnPresent = fileList.stream().filter(x -> x.getOriginalFilename().equals("en.zip")).count() == 1L;
        Boolean isItPresent = fileList.stream().filter(x -> x.getOriginalFilename().equals("it.zip")).count() == 1L;

        if (!BooleanUtils.and(new Boolean[]{isDePresent, isFrPresent, isEnPresent, isItPresent})) {
            return ResponseEntity.badRequest().body(new FieldErrorVM("sentence", "", "The files must be named EXACTLY de.zip, it.zip, fr.zip and en.zip"));
        }

        LocalDateTime now = LocalDateTime.now();
        /* backup existing files in folder, overwrite with new files */
        fileList.forEach(file -> {
            try {
                File languageFile = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + file.getOriginalFilename());
                if (languageFile.exists()) {

                    NumberFormat f = new DecimalFormat("00");
                    String backupDirName = String.format("backup_%d%s%s%s%s%s", now.getYear(), f.format(now.getDayOfMonth()), f.format(now.getMonthValue()), f.format(now.getHour()), f.format(now.getMinute()), f.format(now.getSecond()));
                    File backupDirectory = new File(applicationProperties.getImportProperties().getImportFolder() + "/" + backupDirName);
                    // backup file
                    FileUtils.moveFileToDirectory(languageFile, backupDirectory, true);
                }
                // overwrite/create
                FileUtils.writeByteArrayToFile(languageFile, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        importFilesService.launchNewThreadAsync(domainName);
        return ResponseEntity.ok(new FieldErrorVM("", "", "OK"));
    }
}
