package it.clesius.albina.copedit.web.rest;

import it.clesius.albina.copedit.AppApp;

import it.clesius.albina.copedit.domain.SentenceLang;
import it.clesius.albina.copedit.repository.SentenceLangRepository;
import it.clesius.albina.copedit.service.SentenceLangService;
import it.clesius.albina.copedit.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static it.clesius.albina.copedit.web.rest.TestUtil.sameInstant;
import static it.clesius.albina.copedit.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.clesius.albina.copedit.domain.enumeration.LanguageEnum;
/**
 * Test class for the SentenceLangResource REST controller.
 *
 * @see SentenceLangResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApp.class)
public class SentenceLangResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final LanguageEnum DEFAULT_LANGUAGE = LanguageEnum.DE;
    private static final LanguageEnum UPDATED_LANGUAGE = LanguageEnum.EN;

    private static final ZonedDateTime DEFAULT_UPDATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STRUCTURE = "AAAAAAAAAA";
    private static final String UPDATED_STRUCTURE = "BBBBBBBBBB";

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    @Autowired
    private SentenceLangRepository sentenceLangRepository;

    

    @Autowired
    private SentenceLangService sentenceLangService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSentenceLangMockMvc;

    private SentenceLang sentenceLang;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SentenceLangResource sentenceLangResource = new SentenceLangResource(sentenceLangService);
        this.restSentenceLangMockMvc = MockMvcBuilders.standaloneSetup(sentenceLangResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SentenceLang createEntity(EntityManager em) {
        SentenceLang sentenceLang = new SentenceLang()
            .name(DEFAULT_NAME)
            .header(DEFAULT_HEADER)
            .language(DEFAULT_LANGUAGE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .structure(DEFAULT_STRUCTURE)
            .remark(DEFAULT_REMARK);
        return sentenceLang;
    }

    @Before
    public void initTest() {
        sentenceLang = createEntity(em);
    }

    @Test
    @Transactional
    public void createSentenceLang() throws Exception {
        int databaseSizeBeforeCreate = sentenceLangRepository.findAll().size();

        // Create the SentenceLang
        restSentenceLangMockMvc.perform(post("/api/sentence-langs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceLang)))
            .andExpect(status().isCreated());

        // Validate the SentenceLang in the database
        List<SentenceLang> sentenceLangList = sentenceLangRepository.findAll();
        assertThat(sentenceLangList).hasSize(databaseSizeBeforeCreate + 1);
        SentenceLang testSentenceLang = sentenceLangList.get(sentenceLangList.size() - 1);
        assertThat(testSentenceLang.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSentenceLang.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testSentenceLang.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testSentenceLang.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testSentenceLang.getStructure()).isEqualTo(DEFAULT_STRUCTURE);
        assertThat(testSentenceLang.getRemark()).isEqualTo(DEFAULT_REMARK);
    }

    @Test
    @Transactional
    public void createSentenceLangWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sentenceLangRepository.findAll().size();

        // Create the SentenceLang with an existing ID
        sentenceLang.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSentenceLangMockMvc.perform(post("/api/sentence-langs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceLang)))
            .andExpect(status().isBadRequest());

        // Validate the SentenceLang in the database
        List<SentenceLang> sentenceLangList = sentenceLangRepository.findAll();
        assertThat(sentenceLangList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSentenceLangs() throws Exception {
        // Initialize the database
        sentenceLangRepository.saveAndFlush(sentenceLang);

        // Get all the sentenceLangList
        restSentenceLangMockMvc.perform(get("/api/sentence-langs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sentenceLang.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(sameInstant(DEFAULT_UPDATED_DATE))))
            .andExpect(jsonPath("$.[*].structure").value(hasItem(DEFAULT_STRUCTURE.toString())))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK.toString())));
    }
    

    @Test
    @Transactional
    public void getSentenceLang() throws Exception {
        // Initialize the database
        sentenceLangRepository.saveAndFlush(sentenceLang);

        // Get the sentenceLang
        restSentenceLangMockMvc.perform(get("/api/sentence-langs/{id}", sentenceLang.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sentenceLang.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(sameInstant(DEFAULT_UPDATED_DATE)))
            .andExpect(jsonPath("$.structure").value(DEFAULT_STRUCTURE.toString()))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSentenceLang() throws Exception {
        // Get the sentenceLang
        restSentenceLangMockMvc.perform(get("/api/sentence-langs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSentenceLang() throws Exception {
        // Initialize the database
        sentenceLangService.save(sentenceLang);

        int databaseSizeBeforeUpdate = sentenceLangRepository.findAll().size();

        // Update the sentenceLang
        SentenceLang updatedSentenceLang = sentenceLangRepository.findById(sentenceLang.getId()).get();
        // Disconnect from session so that the updates on updatedSentenceLang are not directly saved in db
        em.detach(updatedSentenceLang);
        updatedSentenceLang
            .name(UPDATED_NAME)
            .header(UPDATED_HEADER)
            .language(UPDATED_LANGUAGE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .structure(UPDATED_STRUCTURE)
            .remark(UPDATED_REMARK);

        restSentenceLangMockMvc.perform(put("/api/sentence-langs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSentenceLang)))
            .andExpect(status().isOk());

        // Validate the SentenceLang in the database
        List<SentenceLang> sentenceLangList = sentenceLangRepository.findAll();
        assertThat(sentenceLangList).hasSize(databaseSizeBeforeUpdate);
        SentenceLang testSentenceLang = sentenceLangList.get(sentenceLangList.size() - 1);
        assertThat(testSentenceLang.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSentenceLang.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testSentenceLang.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testSentenceLang.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testSentenceLang.getStructure()).isEqualTo(UPDATED_STRUCTURE);
        assertThat(testSentenceLang.getRemark()).isEqualTo(UPDATED_REMARK);
    }

    @Test
    @Transactional
    public void updateNonExistingSentenceLang() throws Exception {
        int databaseSizeBeforeUpdate = sentenceLangRepository.findAll().size();

        // Create the SentenceLang

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSentenceLangMockMvc.perform(put("/api/sentence-langs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceLang)))
            .andExpect(status().isBadRequest());

        // Validate the SentenceLang in the database
        List<SentenceLang> sentenceLangList = sentenceLangRepository.findAll();
        assertThat(sentenceLangList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSentenceLang() throws Exception {
        // Initialize the database
        sentenceLangService.save(sentenceLang);

        int databaseSizeBeforeDelete = sentenceLangRepository.findAll().size();

        // Get the sentenceLang
        restSentenceLangMockMvc.perform(delete("/api/sentence-langs/{id}", sentenceLang.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SentenceLang> sentenceLangList = sentenceLangRepository.findAll();
        assertThat(sentenceLangList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SentenceLang.class);
        SentenceLang sentenceLang1 = new SentenceLang();
        sentenceLang1.setId(1L);
        SentenceLang sentenceLang2 = new SentenceLang();
        sentenceLang2.setId(sentenceLang1.getId());
        assertThat(sentenceLang1).isEqualTo(sentenceLang2);
        sentenceLang2.setId(2L);
        assertThat(sentenceLang1).isNotEqualTo(sentenceLang2);
        sentenceLang1.setId(null);
        assertThat(sentenceLang1).isNotEqualTo(sentenceLang2);
    }
}
