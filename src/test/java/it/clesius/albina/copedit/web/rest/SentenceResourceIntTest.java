package it.clesius.albina.copedit.web.rest;

import it.clesius.albina.copedit.AppApp;

import it.clesius.albina.copedit.domain.Sentence;
import it.clesius.albina.copedit.repository.SentenceRepository;
import it.clesius.albina.copedit.service.SentenceService;
import it.clesius.albina.copedit.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static it.clesius.albina.copedit.web.rest.TestUtil.sameInstant;
import static it.clesius.albina.copedit.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SentenceResource REST controller.
 *
 * @see SentenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApp.class)
public class SentenceResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_STRUCTURE = "AAAAAAAAAA";
    private static final String UPDATED_STRUCTURE = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_BASIS_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_BASIS_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_REMARK = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DELETED = false;
    private static final Boolean UPDATED_DELETED = true;

    private static final Boolean DEFAULT_JOKER_SENTENCE = false;
    private static final Boolean UPDATED_JOKER_SENTENCE = true;

    private static final Integer DEFAULT_DOMAIN_ID = 1;
    private static final Integer UPDATED_DOMAIN_ID = 2;

    private static final ZonedDateTime DEFAULT_DATENEW = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATENEW = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DATELAST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATELAST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SentenceRepository sentenceRepository;

    

    @Autowired
    private SentenceService sentenceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSentenceMockMvc;

    private Sentence sentence;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SentenceResource sentenceResource = new SentenceResource(sentenceService);
        this.restSentenceMockMvc = MockMvcBuilders.standaloneSetup(sentenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sentence createEntity(EntityManager em) {
        Sentence sentence = new Sentence()
            .name(DEFAULT_NAME)
            .header(DEFAULT_HEADER)
            .language(DEFAULT_LANGUAGE)
            .structure(DEFAULT_STRUCTURE)
            .version(DEFAULT_VERSION)
            .basisVersion(DEFAULT_BASIS_VERSION)
            .remark(DEFAULT_REMARK)
            .deleted(DEFAULT_DELETED)
            .jokerSentence(DEFAULT_JOKER_SENTENCE)
            .domainId(DEFAULT_DOMAIN_ID)
            .datenew(DEFAULT_DATENEW)
            .datelast(DEFAULT_DATELAST);
        return sentence;
    }

    @Before
    public void initTest() {
        sentence = createEntity(em);
    }

    @Test
    @Transactional
    public void createSentence() throws Exception {
        int databaseSizeBeforeCreate = sentenceRepository.findAll().size();

        // Create the Sentence
        restSentenceMockMvc.perform(post("/api/sentences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentence)))
            .andExpect(status().isCreated());

        // Validate the Sentence in the database
        List<Sentence> sentenceList = sentenceRepository.findAll();
        assertThat(sentenceList).hasSize(databaseSizeBeforeCreate + 1);
        Sentence testSentence = sentenceList.get(sentenceList.size() - 1);
        assertThat(testSentence.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSentence.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testSentence.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testSentence.getStructure()).isEqualTo(DEFAULT_STRUCTURE);
        assertThat(testSentence.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testSentence.getBasisVersion()).isEqualTo(DEFAULT_BASIS_VERSION);
        assertThat(testSentence.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testSentence.isDeleted()).isEqualTo(DEFAULT_DELETED);
        assertThat(testSentence.isJokerSentence()).isEqualTo(DEFAULT_JOKER_SENTENCE);
        assertThat(testSentence.getDomainId()).isEqualTo(DEFAULT_DOMAIN_ID);
        assertThat(testSentence.getDatenew()).isEqualTo(DEFAULT_DATENEW);
        assertThat(testSentence.getDatelast()).isEqualTo(DEFAULT_DATELAST);
    }

    @Test
    @Transactional
    public void createSentenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sentenceRepository.findAll().size();

        // Create the Sentence with an existing ID
        sentence.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSentenceMockMvc.perform(post("/api/sentences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentence)))
            .andExpect(status().isBadRequest());

        // Validate the Sentence in the database
        List<Sentence> sentenceList = sentenceRepository.findAll();
        assertThat(sentenceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSentences() throws Exception {
        // Initialize the database
        sentenceRepository.saveAndFlush(sentence);

        // Get all the sentenceList
        restSentenceMockMvc.perform(get("/api/sentences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sentence.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].structure").value(hasItem(DEFAULT_STRUCTURE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].basisVersion").value(hasItem(DEFAULT_BASIS_VERSION.toString())))
            .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK.toString())))
            .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].jokerSentence").value(hasItem(DEFAULT_JOKER_SENTENCE.booleanValue())))
            .andExpect(jsonPath("$.[*].domainId").value(hasItem(DEFAULT_DOMAIN_ID)))
            .andExpect(jsonPath("$.[*].datenew").value(hasItem(sameInstant(DEFAULT_DATENEW))))
            .andExpect(jsonPath("$.[*].datelast").value(hasItem(sameInstant(DEFAULT_DATELAST))));
    }
    

    @Test
    @Transactional
    public void getSentence() throws Exception {
        // Initialize the database
        sentenceRepository.saveAndFlush(sentence);

        // Get the sentence
        restSentenceMockMvc.perform(get("/api/sentences/{id}", sentence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sentence.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.structure").value(DEFAULT_STRUCTURE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.basisVersion").value(DEFAULT_BASIS_VERSION.toString()))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK.toString()))
            .andExpect(jsonPath("$.deleted").value(DEFAULT_DELETED.booleanValue()))
            .andExpect(jsonPath("$.jokerSentence").value(DEFAULT_JOKER_SENTENCE.booleanValue()))
            .andExpect(jsonPath("$.domainId").value(DEFAULT_DOMAIN_ID))
            .andExpect(jsonPath("$.datenew").value(sameInstant(DEFAULT_DATENEW)))
            .andExpect(jsonPath("$.datelast").value(sameInstant(DEFAULT_DATELAST)));
    }
    @Test
    @Transactional
    public void getNonExistingSentence() throws Exception {
        // Get the sentence
        restSentenceMockMvc.perform(get("/api/sentences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSentence() throws Exception {
        // Initialize the database
        sentenceService.save(sentence);

        int databaseSizeBeforeUpdate = sentenceRepository.findAll().size();

        // Update the sentence
        Sentence updatedSentence = sentenceRepository.findById(sentence.getId()).get();
        // Disconnect from session so that the updates on updatedSentence are not directly saved in db
        em.detach(updatedSentence);
        updatedSentence
            .name(UPDATED_NAME)
            .header(UPDATED_HEADER)
            .language(UPDATED_LANGUAGE)
            .structure(UPDATED_STRUCTURE)
            .version(UPDATED_VERSION)
            .basisVersion(UPDATED_BASIS_VERSION)
            .remark(UPDATED_REMARK)
            .deleted(UPDATED_DELETED)
            .jokerSentence(UPDATED_JOKER_SENTENCE)
            .domainId(UPDATED_DOMAIN_ID)
            .datenew(UPDATED_DATENEW)
            .datelast(UPDATED_DATELAST);

        restSentenceMockMvc.perform(put("/api/sentences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSentence)))
            .andExpect(status().isOk());

        // Validate the Sentence in the database
        List<Sentence> sentenceList = sentenceRepository.findAll();
        assertThat(sentenceList).hasSize(databaseSizeBeforeUpdate);
        Sentence testSentence = sentenceList.get(sentenceList.size() - 1);
        assertThat(testSentence.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSentence.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testSentence.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testSentence.getStructure()).isEqualTo(UPDATED_STRUCTURE);
        assertThat(testSentence.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testSentence.getBasisVersion()).isEqualTo(UPDATED_BASIS_VERSION);
        assertThat(testSentence.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testSentence.isDeleted()).isEqualTo(UPDATED_DELETED);
        assertThat(testSentence.isJokerSentence()).isEqualTo(UPDATED_JOKER_SENTENCE);
        assertThat(testSentence.getDomainId()).isEqualTo(UPDATED_DOMAIN_ID);
        assertThat(testSentence.getDatenew()).isEqualTo(UPDATED_DATENEW);
        assertThat(testSentence.getDatelast()).isEqualTo(UPDATED_DATELAST);
    }

    @Test
    @Transactional
    public void updateNonExistingSentence() throws Exception {
        int databaseSizeBeforeUpdate = sentenceRepository.findAll().size();

        // Create the Sentence

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSentenceMockMvc.perform(put("/api/sentences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentence)))
            .andExpect(status().isBadRequest());

        // Validate the Sentence in the database
        List<Sentence> sentenceList = sentenceRepository.findAll();
        assertThat(sentenceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSentence() throws Exception {
        // Initialize the database
        sentenceService.save(sentence);

        int databaseSizeBeforeDelete = sentenceRepository.findAll().size();

        // Get the sentence
        restSentenceMockMvc.perform(delete("/api/sentences/{id}", sentence.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sentence> sentenceList = sentenceRepository.findAll();
        assertThat(sentenceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sentence.class);
        Sentence sentence1 = new Sentence();
        sentence1.setId(1L);
        Sentence sentence2 = new Sentence();
        sentence2.setId(sentence1.getId());
        assertThat(sentence1).isEqualTo(sentence2);
        sentence2.setId(2L);
        assertThat(sentence1).isNotEqualTo(sentence2);
        sentence1.setId(null);
        assertThat(sentence1).isNotEqualTo(sentence2);
    }
}
