package it.clesius.albina.copedit.web.rest;

import it.clesius.albina.copedit.AppApp;
import it.clesius.albina.copedit.domain.Phrase;
import it.clesius.albina.copedit.repository.PhraseRepository;
import it.clesius.albina.copedit.service.PhraseService;
import it.clesius.albina.copedit.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static it.clesius.albina.copedit.web.rest.TestUtil.createFormattingConversionService;
import static it.clesius.albina.copedit.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PhraseResource REST controller.
 *
 * @see PhraseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApp.class)
public class PhraseResourceIntTest {

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_SPACE_BEFORE = false;
    private static final Boolean UPDATED_SPACE_BEFORE = true;

    private static final Boolean DEFAULT_SPACE_AFTER = false;
    private static final Boolean UPDATED_SPACE_AFTER = true;

    private static final Boolean DEFAULT_REMOVE_PUNCTUATION_BEFORE = false;
    private static final Boolean UPDATED_REMOVE_PUNCTUATION_BEFORE = true;

    private static final Integer DEFAULT_ITEM_NO = 1;
    private static final Integer UPDATED_ITEM_NO = 2;

    private static final Boolean DEFAULT_INCORRECT = false;
    private static final Boolean UPDATED_INCORRECT = true;

    private static final Integer DEFAULT_PHRASE_OPTION_ITEM_ID = 1;
    private static final Integer UPDATED_PHRASE_OPTION_ITEM_ID = 2;

    private static final ZonedDateTime DEFAULT_DATELAST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATELAST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DATENEW = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATENEW = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PhraseRepository phraseRepository;


    @Autowired
    private PhraseService phraseService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhraseMockMvc;

    private Phrase phrase;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhraseResource phraseResource = new PhraseResource(phraseService);
        this.restPhraseMockMvc = MockMvcBuilders.standaloneSetup(phraseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phrase createEntity(EntityManager em) {
        Phrase phrase = new Phrase()
            .value(DEFAULT_VALUE)
            .spaceBefore(DEFAULT_SPACE_BEFORE)
            .spaceAfter(DEFAULT_SPACE_AFTER)
            .removePunctuationBefore(DEFAULT_REMOVE_PUNCTUATION_BEFORE)
            .itemNo(DEFAULT_ITEM_NO)
            .incorrect(DEFAULT_INCORRECT)
            .phraseOptionItemId(DEFAULT_PHRASE_OPTION_ITEM_ID)
            .datelast(DEFAULT_DATELAST)
            .datenew(DEFAULT_DATENEW);
        return phrase;
    }

    @Before
    public void initTest() {
        phrase = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhrase() throws Exception {
        int databaseSizeBeforeCreate = phraseRepository.findAll().size();

        // Create the Phrase
        restPhraseMockMvc.perform(post("/api/phrases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phrase)))
            .andExpect(status().isCreated());

        // Validate the Phrase in the database
        List<Phrase> phraseList = phraseRepository.findAll();
        assertThat(phraseList).hasSize(databaseSizeBeforeCreate + 1);
        Phrase testPhrase = phraseList.get(phraseList.size() - 1);
        assertThat(testPhrase.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testPhrase.isSpaceBefore()).isEqualTo(DEFAULT_SPACE_BEFORE);
        assertThat(testPhrase.isSpaceAfter()).isEqualTo(DEFAULT_SPACE_AFTER);
        assertThat(testPhrase.isRemovePunctuationBefore()).isEqualTo(DEFAULT_REMOVE_PUNCTUATION_BEFORE);
        assertThat(testPhrase.getItemPartNo()).isEqualTo(DEFAULT_ITEM_NO);
        assertThat(testPhrase.isIncorrect()).isEqualTo(DEFAULT_INCORRECT);
        assertThat(testPhrase.getPhraseOptionItemId()).isEqualTo(DEFAULT_PHRASE_OPTION_ITEM_ID);
        assertThat(testPhrase.getDatelast()).isEqualTo(DEFAULT_DATELAST);
        assertThat(testPhrase.getDatenew()).isEqualTo(DEFAULT_DATENEW);
    }

    @Test
    @Transactional
    public void createPhraseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = phraseRepository.findAll().size();

        // Create the Phrase with an existing ID
        phrase.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhraseMockMvc.perform(post("/api/phrases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phrase)))
            .andExpect(status().isBadRequest());

        // Validate the Phrase in the database
        List<Phrase> phraseList = phraseRepository.findAll();
        assertThat(phraseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhrases() throws Exception {
        // Initialize the database
        phraseRepository.saveAndFlush(phrase);

        // Get all the phraseList
        restPhraseMockMvc.perform(get("/api/phrases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(phrase.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].spaceBefore").value(hasItem(DEFAULT_SPACE_BEFORE.booleanValue())))
            .andExpect(jsonPath("$.[*].spaceAfter").value(hasItem(DEFAULT_SPACE_AFTER.booleanValue())))
            .andExpect(jsonPath("$.[*].removePunctuationBefore").value(hasItem(DEFAULT_REMOVE_PUNCTUATION_BEFORE.booleanValue())))
            .andExpect(jsonPath("$.[*].itemNo").value(hasItem(DEFAULT_ITEM_NO)))
            .andExpect(jsonPath("$.[*].incorrect").value(hasItem(DEFAULT_INCORRECT.booleanValue())))
            .andExpect(jsonPath("$.[*].phraseOptionItemId").value(hasItem(DEFAULT_PHRASE_OPTION_ITEM_ID)))
            .andExpect(jsonPath("$.[*].datelast").value(hasItem(sameInstant(DEFAULT_DATELAST))))
            .andExpect(jsonPath("$.[*].datenew").value(hasItem(sameInstant(DEFAULT_DATENEW))));
    }


    @Test
    @Transactional
    public void getPhrase() throws Exception {
        // Initialize the database
        phraseRepository.saveAndFlush(phrase);

        // Get the phrase
        restPhraseMockMvc.perform(get("/api/phrases/{id}", phrase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(phrase.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.spaceBefore").value(DEFAULT_SPACE_BEFORE.booleanValue()))
            .andExpect(jsonPath("$.spaceAfter").value(DEFAULT_SPACE_AFTER.booleanValue()))
            .andExpect(jsonPath("$.removePunctuationBefore").value(DEFAULT_REMOVE_PUNCTUATION_BEFORE.booleanValue()))
            .andExpect(jsonPath("$.itemNo").value(DEFAULT_ITEM_NO))
            .andExpect(jsonPath("$.incorrect").value(DEFAULT_INCORRECT.booleanValue()))
            .andExpect(jsonPath("$.phraseOptionItemId").value(DEFAULT_PHRASE_OPTION_ITEM_ID))
            .andExpect(jsonPath("$.datelast").value(sameInstant(DEFAULT_DATELAST)))
            .andExpect(jsonPath("$.datenew").value(sameInstant(DEFAULT_DATENEW)));
    }

    @Test
    @Transactional
    public void getNonExistingPhrase() throws Exception {
        // Get the phrase
        restPhraseMockMvc.perform(get("/api/phrases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhrase() throws Exception {
        // Initialize the database
        phraseService.save(phrase);

        int databaseSizeBeforeUpdate = phraseRepository.findAll().size();

        // Update the phrase
        Phrase updatedPhrase = phraseRepository.findById(phrase.getId()).get();
        // Disconnect from session so that the updates on updatedPhrase are not directly saved in db
        em.detach(updatedPhrase);
        updatedPhrase
            .value(UPDATED_VALUE)
            .spaceBefore(UPDATED_SPACE_BEFORE)
            .spaceAfter(UPDATED_SPACE_AFTER)
            .removePunctuationBefore(UPDATED_REMOVE_PUNCTUATION_BEFORE)
            .itemNo(UPDATED_ITEM_NO)
            .incorrect(UPDATED_INCORRECT)
            .phraseOptionItemId(UPDATED_PHRASE_OPTION_ITEM_ID)
            .datelast(UPDATED_DATELAST)
            .datenew(UPDATED_DATENEW);

        restPhraseMockMvc.perform(put("/api/phrases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhrase)))
            .andExpect(status().isOk());

        // Validate the Phrase in the database
        List<Phrase> phraseList = phraseRepository.findAll();
        assertThat(phraseList).hasSize(databaseSizeBeforeUpdate);
        Phrase testPhrase = phraseList.get(phraseList.size() - 1);
        assertThat(testPhrase.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testPhrase.isSpaceBefore()).isEqualTo(UPDATED_SPACE_BEFORE);
        assertThat(testPhrase.isSpaceAfter()).isEqualTo(UPDATED_SPACE_AFTER);
        assertThat(testPhrase.isRemovePunctuationBefore()).isEqualTo(UPDATED_REMOVE_PUNCTUATION_BEFORE);
        assertThat(testPhrase.getItemPartNo()).isEqualTo(UPDATED_ITEM_NO);
        assertThat(testPhrase.isIncorrect()).isEqualTo(UPDATED_INCORRECT);
        assertThat(testPhrase.getPhraseOptionItemId()).isEqualTo(UPDATED_PHRASE_OPTION_ITEM_ID);
        assertThat(testPhrase.getDatelast()).isEqualTo(UPDATED_DATELAST);
        assertThat(testPhrase.getDatenew()).isEqualTo(UPDATED_DATENEW);
    }

    @Test
    @Transactional
    public void updateNonExistingPhrase() throws Exception {
        int databaseSizeBeforeUpdate = phraseRepository.findAll().size();

        // Create the Phrase

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPhraseMockMvc.perform(put("/api/phrases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phrase)))
            .andExpect(status().isBadRequest());

        // Validate the Phrase in the database
        List<Phrase> phraseList = phraseRepository.findAll();
        assertThat(phraseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhrase() throws Exception {
        // Initialize the database
        phraseService.save(phrase);

        int databaseSizeBeforeDelete = phraseRepository.findAll().size();

        // Get the phrase
        restPhraseMockMvc.perform(delete("/api/phrases/{id}", phrase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Phrase> phraseList = phraseRepository.findAll();
        assertThat(phraseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Phrase.class);
        Phrase phrase1 = new Phrase();
        phrase1.setId(1L);
        Phrase phrase2 = new Phrase();
        phrase2.setId(phrase1.getId());
        assertThat(phrase1).isEqualTo(phrase2);
        phrase2.setId(2L);
        assertThat(phrase1).isNotEqualTo(phrase2);
        phrase1.setId(null);
        assertThat(phrase1).isNotEqualTo(phrase2);
    }
}
