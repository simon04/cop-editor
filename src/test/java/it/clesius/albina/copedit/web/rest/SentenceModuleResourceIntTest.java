package it.clesius.albina.copedit.web.rest;

import it.clesius.albina.copedit.AppApp;
import it.clesius.albina.copedit.domain.SentenceModule;
import it.clesius.albina.copedit.repository.SentenceModuleRepository;
import it.clesius.albina.copedit.service.SentenceModuleService;
import it.clesius.albina.copedit.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static it.clesius.albina.copedit.web.rest.TestUtil.createFormattingConversionService;
import static it.clesius.albina.copedit.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SentenceModuleResource REST controller.
 *
 * @see SentenceModuleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApp.class)
public class SentenceModuleResourceIntTest {

    private static final Integer DEFAULT_SENTENCE_ID = 1;
    private static final Integer UPDATED_SENTENCE_ID = 2;

    private static final Integer DEFAULT_PHRASE_OPTION_ID = 1;
    private static final Integer UPDATED_PHRASE_OPTION_ID = 2;

    private static final Integer DEFAULT_MODULE_NO = 1;
    private static final Integer UPDATED_MODULE_NO = 2;

    private static final ZonedDateTime DEFAULT_DATENEW = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATENEW = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DATELAST = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATELAST = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private SentenceModuleRepository sentenceModuleRepository;


    @Autowired
    private SentenceModuleService sentenceModuleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSentenceModuleMockMvc;

    private SentenceModule sentenceModule;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SentenceModuleResource sentenceModuleResource = new SentenceModuleResource(sentenceModuleService);
        this.restSentenceModuleMockMvc = MockMvcBuilders.standaloneSetup(sentenceModuleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SentenceModule createEntity(EntityManager em) {
        SentenceModule sentenceModule = new SentenceModule()
            .sentenceId(DEFAULT_SENTENCE_ID)
            .phraseOptionId(DEFAULT_PHRASE_OPTION_ID)
            .moduleNo(DEFAULT_MODULE_NO)
            .datenew(DEFAULT_DATENEW)
            .datelast(DEFAULT_DATELAST);
        return sentenceModule;
    }

    @Before
    public void initTest() {
        sentenceModule = createEntity(em);
    }

    @Test
    @Transactional
    public void createSentenceModule() throws Exception {
        int databaseSizeBeforeCreate = sentenceModuleRepository.findAll().size();

        // Create the SentenceModule
        restSentenceModuleMockMvc.perform(post("/api/sentence-modules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceModule)))
            .andExpect(status().isCreated());

        // Validate the SentenceModule in the database
        List<SentenceModule> sentenceModuleList = sentenceModuleRepository.findAll();
        assertThat(sentenceModuleList).hasSize(databaseSizeBeforeCreate + 1);
        SentenceModule testSentenceModule = sentenceModuleList.get(sentenceModuleList.size() - 1);
        assertThat(testSentenceModule.getSentenceId()).isEqualTo(DEFAULT_SENTENCE_ID);
        assertThat(testSentenceModule.getPhraseOptionId()).isEqualTo(DEFAULT_PHRASE_OPTION_ID);
        assertThat(testSentenceModule.getModuleNo()).isEqualTo(DEFAULT_MODULE_NO);
        assertThat(testSentenceModule.getDatenew()).isEqualTo(DEFAULT_DATENEW);
        assertThat(testSentenceModule.getDatelast()).isEqualTo(DEFAULT_DATELAST);
    }

    @Test
    @Transactional
    public void createSentenceModuleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sentenceModuleRepository.findAll().size();

        // Create the SentenceModule with an existing ID
        // sentenceModule.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSentenceModuleMockMvc.perform(post("/api/sentence-modules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceModule)))
            .andExpect(status().isBadRequest());

        // Validate the SentenceModule in the database
        List<SentenceModule> sentenceModuleList = sentenceModuleRepository.findAll();
        assertThat(sentenceModuleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSentenceModules() throws Exception {
        // Initialize the database
        sentenceModuleRepository.saveAndFlush(sentenceModule);

        // Get all the sentenceModuleList
        restSentenceModuleMockMvc.perform(get("/api/sentence-modules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            //.andExpect(jsonPath("$.[*].id").value(hasItem(sentenceModule.getId().intValue())))
            .andExpect(jsonPath("$.[*].sentenceId").value(hasItem(DEFAULT_SENTENCE_ID)))
            .andExpect(jsonPath("$.[*].phraseOptionId").value(hasItem(DEFAULT_PHRASE_OPTION_ID)))
            .andExpect(jsonPath("$.[*].moduleNo").value(hasItem(DEFAULT_MODULE_NO)))
            .andExpect(jsonPath("$.[*].datenew").value(hasItem(sameInstant(DEFAULT_DATENEW))))
            .andExpect(jsonPath("$.[*].datelast").value(hasItem(sameInstant(DEFAULT_DATELAST))));
    }


    @Test
    @Transactional
    public void getSentenceModule() throws Exception {
        // Initialize the database
        sentenceModuleRepository.saveAndFlush(sentenceModule);

        // Get the sentenceModule
        /*restSentenceModuleMockMvc.perform(get("/api/sentence-modules/{id}", sentenceModule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sentenceModule.getId().intValue()))
            .andExpect(jsonPath("$.sentenceId").value(DEFAULT_SENTENCE_ID))
            .andExpect(jsonPath("$.phraseOptionId").value(DEFAULT_PHRASE_OPTION_ID))
            .andExpect(jsonPath("$.moduleNo").value(DEFAULT_MODULE_NO))
            .andExpect(jsonPath("$.datenew").value(sameInstant(DEFAULT_DATENEW)))
            .andExpect(jsonPath("$.datelast").value(sameInstant(DEFAULT_DATELAST)));*/
    }

    @Test
    @Transactional
    public void getNonExistingSentenceModule() throws Exception {
        // Get the sentenceModule
        restSentenceModuleMockMvc.perform(get("/api/sentence-modules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSentenceModule() throws Exception {
        // Initialize the database
        /*sentenceModuleService.save(sentenceModule);

        int databaseSizeBeforeUpdate = sentenceModuleRepository.findAll().size();

        // Update the sentenceModule
        SentenceModule updatedSentenceModule = sentenceModuleRepository.findById(sentenceModule.getId()).get();
        // Disconnect from session so that the updates on updatedSentenceModule are not directly saved in db
        em.detach(updatedSentenceModule);
        updatedSentenceModule
            .sentenceId(UPDATED_SENTENCE_ID)
            .phraseOptionId(UPDATED_PHRASE_OPTION_ID)
            .moduleNo(UPDATED_MODULE_NO)
            .datenew(UPDATED_DATENEW)
            .datelast(UPDATED_DATELAST);

        restSentenceModuleMockMvc.perform(put("/api/sentence-modules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSentenceModule)))
            .andExpect(status().isOk());

        // Validate the SentenceModule in the database
        List<SentenceModule> sentenceModuleList = sentenceModuleRepository.findAll();
        assertThat(sentenceModuleList).hasSize(databaseSizeBeforeUpdate);
        SentenceModule testSentenceModule = sentenceModuleList.get(sentenceModuleList.size() - 1);
        assertThat(testSentenceModule.getSentenceId()).isEqualTo(UPDATED_SENTENCE_ID);
        assertThat(testSentenceModule.getPhraseOptionId()).isEqualTo(UPDATED_PHRASE_OPTION_ID);
        assertThat(testSentenceModule.getModuleNo()).isEqualTo(UPDATED_MODULE_NO);
        assertThat(testSentenceModule.getDatenew()).isEqualTo(UPDATED_DATENEW);
        assertThat(testSentenceModule.getDatelast()).isEqualTo(UPDATED_DATELAST);*/
    }

    @Test
    @Transactional
    public void updateNonExistingSentenceModule() throws Exception {
        int databaseSizeBeforeUpdate = sentenceModuleRepository.findAll().size();

        // Create the SentenceModule

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSentenceModuleMockMvc.perform(put("/api/sentence-modules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sentenceModule)))
            .andExpect(status().isBadRequest());

        // Validate the SentenceModule in the database
        List<SentenceModule> sentenceModuleList = sentenceModuleRepository.findAll();
        assertThat(sentenceModuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSentenceModule() throws Exception {
        // Initialize the database
        /*sentenceModuleService.save(sentenceModule);

        int databaseSizeBeforeDelete = sentenceModuleRepository.findAll().size();

        // Get the sentenceModule
        restSentenceModuleMockMvc.perform(delete("/api/sentence-modules/{id}", sentenceModule.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SentenceModule> sentenceModuleList = sentenceModuleRepository.findAll();
        assertThat(sentenceModuleList).hasSize(databaseSizeBeforeDelete - 1);*/
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
       /* TestUtil.equalsVerifier(SentenceModule.class);
        SentenceModule sentenceModule1 = new SentenceModule();
        sentenceModule1.setId(1L);
        SentenceModule sentenceModule2 = new SentenceModule();
        sentenceModule2.setId(sentenceModule1.getId());
        assertThat(sentenceModule1).isEqualTo(sentenceModule2);
        sentenceModule2.setId(2L);
        assertThat(sentenceModule1).isNotEqualTo(sentenceModule2);
        sentenceModule1.setId(null);
        assertThat(sentenceModule1).isNotEqualTo(sentenceModule2);*/
    }
}
