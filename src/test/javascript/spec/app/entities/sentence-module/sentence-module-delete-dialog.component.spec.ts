/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppTestModule } from '../../../test.module';
import { SentenceModuleDeleteDialogComponent } from 'app/entities/sentence-module/sentence-module-delete-dialog.component';
import { SentenceModuleService } from 'app/entities/sentence-module/sentence-module.service';

describe('Component Tests', () => {
    describe('SentenceModule Management Delete Component', () => {
        let comp: SentenceModuleDeleteDialogComponent;
        let fixture: ComponentFixture<SentenceModuleDeleteDialogComponent>;
        let service: SentenceModuleService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceModuleDeleteDialogComponent]
            })
                .overrideTemplate(SentenceModuleDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SentenceModuleDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceModuleService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
