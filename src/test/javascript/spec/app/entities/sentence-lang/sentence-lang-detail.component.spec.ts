/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { SentenceLangDetailComponent } from 'app/entities/sentence-lang/sentence-lang-detail.component';
import { SentenceLang } from 'app/shared/model/sentence-lang.model';

describe('Component Tests', () => {
    describe('SentenceLang Management Detail Component', () => {
        let comp: SentenceLangDetailComponent;
        let fixture: ComponentFixture<SentenceLangDetailComponent>;
        const route = ({ data: of({ sentenceLang: new SentenceLang(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceLangDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SentenceLangDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SentenceLangDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sentenceLang).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
