/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { SentenceLangComponent } from 'app/entities/sentence-lang/sentence-lang.component';
import { SentenceLangService } from 'app/entities/sentence-lang/sentence-lang.service';
import { SentenceLang } from 'app/shared/model/sentence-lang.model';

describe('Component Tests', () => {
    describe('SentenceLang Management Component', () => {
        let comp: SentenceLangComponent;
        let fixture: ComponentFixture<SentenceLangComponent>;
        let service: SentenceLangService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [SentenceLangComponent],
                providers: []
            })
                .overrideTemplate(SentenceLangComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SentenceLangComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SentenceLangService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SentenceLang(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sentenceLangs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
