/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppTestModule } from '../../../test.module';
import { PhraseOptionDetailComponent } from 'app/entities/phrase-option/phrase-option-detail.component';
import { PhraseOption } from 'app/shared/model/phrase-option.model';

describe('Component Tests', () => {
    describe('PhraseOption Management Detail Component', () => {
        let comp: PhraseOptionDetailComponent;
        let fixture: ComponentFixture<PhraseOptionDetailComponent>;
        const route = ({ data: of({ phraseOption: new PhraseOption(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [PhraseOptionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PhraseOptionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PhraseOptionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.phraseOption).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
