/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppTestModule } from '../../../test.module';
import { PhraseOptionComponent } from 'app/entities/phrase-option/phrase-option.component';
import { PhraseOptionService } from 'app/entities/phrase-option/phrase-option.service';
import { PhraseOption } from 'app/shared/model/phrase-option.model';

describe('Component Tests', () => {
    describe('PhraseOption Management Component', () => {
        let comp: PhraseOptionComponent;
        let fixture: ComponentFixture<PhraseOptionComponent>;
        let service: PhraseOptionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppTestModule],
                declarations: [PhraseOptionComponent],
                providers: []
            })
                .overrideTemplate(PhraseOptionComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PhraseOptionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PhraseOptionService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new PhraseOption(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.phraseOptions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
